package com.realstudiosonline.sid.graphqlserver;

import android.util.Log;

import androidx.annotation.NonNull;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.ApolloMutationCall;
import com.apollographql.apollo.api.FileUpload;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.rx3.Rx3Apollo;
import com.realstudiosonline.sid.UpdateTaskStatusMutation;
import com.realstudiosonline.sid.models.StudentIDCard;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import okhttp3.OkHttpClient;


public class Server {
    private static ApolloClient apolloClient;

 /*   public static Observable<Response<Login.Data>> fetchRockets() {
        ApolloQueryCall<RocketsQuery.Data> call = getApolloClient()
                .query(new RocketsQuery());

        return Rx3Apollo.from(call)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter((dataResponse -> dataResponse.getData() != null));
    }*/

 /*mutation{
  sid{
    addProject(
      input:{
       // name:"GHANA EDUCATION SERVICE",
       // initials:"sid",
      //  year:"2021",
        closure:true,
        cardTitle:"GHANA EDUCATION SERVICE SHS STUDENT ID CARD"
        startDate:"2020-12-20",
        endDate:"2021-05-20",
        cardSize:"4x4",
        cardBgImg:"https://picsum.photos/500/300?grayscale",


        fields:{
          firstName:true,
          lastName:true,
          middleName:true,
          issueDate:true,
          idNumber:true,
          qrCode:true,
        }
      },
      BgImg:Upload
    ){
      id
      name
      cardTitle
      startDate
      endDate
      cardSize
      cardBgImg
      initials
      year
      closure
    }
  }

}*/

   /* public static Observable<Response<LoginMutation.Data>> loginUSer(String emailPhone, String password) {
        LoginMutation login = new LoginMutation(emailPhone,password);
        ApolloMutationCall<LoginMutation.Data> call = getApolloClient()
                .mutate(login);

        return Rx3Apollo.from(call)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter((dataResponse -> dataResponse.getData() != null));
    }*/

    public static Observable<Response<UpdateTaskStatusMutation.Data>> updateTaskStatus(@NonNull String projectOfficerId,
                                                                                       @NonNull String schoolCode,
                                                                                       @NonNull String status){

        UpdateTaskStatusMutation mutation = new UpdateTaskStatusMutation(projectOfficerId, schoolCode,status);

        ApolloMutationCall<UpdateTaskStatusMutation.Data> call = getApolloClient().mutate(mutation);
        return  Rx3Apollo.from(call).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).filter((dataResponse -> dataResponse.getData()!=null));


    }



    /*public static Observable<Response<SetUpSchoolMutation.Data>> setUpSchool(@NonNull String schoolCode,
                                                                             @NonNull String projectId,
                                                                             @NonNull String contactPerson,
                                                                             @NonNull String contactNumber,
                                                                             @NonNull String status){
        SetUpSchoolMutation mutation = new SetUpSchoolMutation(schoolCode, projectId,contactPerson, contactNumber,status);

        ApolloMutationCall<SetUpSchoolMutation.Data> call = getApolloClient().mutate(mutation);
        return Rx3Apollo.from(call).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).filter((dataResponse -> dataResponse.getData()!=null));
    }
*/


   /* public static Observable<Response<AddProjectMutation.Data>> addProject(String name,
                                                                           String initials,
                                                                           String year,
                                                                           boolean closure,
                                                                           String cardTitle,
                                                                           String startDate,
                                                                           String endDate,
                                                                           String cardSize,
                                                                           String  cardBgImg,
                                                                           String BgImg){
        //  new FileUploader("image/jpeg", BgImg)
        AddProjectMutation mutation = new AddProjectMutation(name,
                initials,year, closure,cardTitle, startDate,endDate,cardSize,cardBgImg,
                new FileUpload("image/jpeg", BgImg));
        ApolloMutationCall<AddProjectMutation.Data> call = getApolloClient().mutate(mutation);

        return Rx3Apollo.from(call)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).filter((dataResponse -> dataResponse.getData() != null));
    }
*/

   /* public static Observable<Response<GetProjectsQuery.Data>> getProjects(){

        GetProjectsQuery getProjectsQuery = new GetProjectsQuery();

       ApolloQueryCall<GetProjectsQuery.Data> queryCall =  getApolloClient().query(getProjectsQuery);

        return Rx3Apollo.from(queryCall)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter((dataResponse -> dataResponse.getData() != null));
    }*/
/*    public static  Observable<Response<AddStudentMutation.Data>> addStudent(
            @NonNull StudentIDCard studentIDCard,
            @NonNull String imei,
            @NonNull LoginMutation.ProjectOfficer projectOfficer){


        Log.d("addStudent: ","studentIDCard: "+studentIDCard.toString()+"\n\n\n\n\n");
        Log.d("addStudent: ","IMEI:"+imei+"\n\n\n\n\n");
        Log.d("addStudent: ","projectOfficer.projectId():"+projectOfficer.projectId()+"\n\n\n\n\n");

        AddStudentMutation studentMutation = new AddStudentMutation(
                "completed",
                imei,
                projectOfficer.projectId(),//project ID
                projectOfficer.id(),
                studentIDCard.getClassID(),//class ID
                studentIDCard.getSchoolCode(),
                studentIDCard.getStudentID(),//school Code
                studentIDCard.getFullName(),//first Name
                studentIDCard.getFullName(),// last Name
                studentIDCard.getDob(),//Date of birth
                studentIDCard.getGender(),// gender
                studentIDCard.getGuardianContact(),//guardian Phone number
                new FileUpload("image/jpeg", studentIDCard.getRightThumbURl()),//Bio Right Thumb
                new FileUpload("image/jpeg", studentIDCard.getLeftThumbURL()),//Bio Left Thumb
                new FileUpload("image/jpeg", studentIDCard.getRightForeFingerURl()),//Bio Right Fore Finger
                new FileUpload("image/jpeg", studentIDCard.getLeftForeFingerURL()),//Bio Left Fore Finger
                new FileUpload("image/jpeg", studentIDCard.getRawProfileImage()),//Profile Image
                new FileUpload("image/jpeg", studentIDCard.getGeneratedCardURI())//Card
                );

        ApolloMutationCall<AddStudentMutation.Data> call = getApolloClient()
                .mutate(studentMutation);

        return Rx3Apollo.from(call)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter((dataResponse -> dataResponse.getData() != null));

    }*/

 /*   public static Observable<Response<LaunchPadsQuery.Data>> fetchLaunchPads() {
        ApolloQueryCall<LaunchPadsQuery.Data> call = getApolloClient()
                .query(new LaunchPadsQuery());

        return Rx3Apollo.from(call)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter((dataResponse -> dataResponse.getData() != null));
    }*/

    private static ApolloClient getApolloClient() {
        if (apolloClient == null) {
           /* Dispatcher dispatcher = new Dispatcher();
            dispatcher.setMaxRequests(1);
            Interceptor interceptor = chain -> {
                //SystemClock.sleep(1000);

                return chain.proceed(chain.request());
            };*/
           /* ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(1);


            ApolloInterceptor apolloInterceptor=  new ApolloInterceptor() {
                @Override
                public void interceptAsync(@NotNull InterceptorRequest request,
                                           @NotNull ApolloInterceptorChain chain, @NotNull Executor dispatcher,
                                           @NotNull CallBack callBack) {
                    SystemClock.sleep(1000);
                    chain.proceedAsync(request,dispatcher,callBack);

                }

                @Override
                public void dispose() {

                }
            };*/
            //Build the Apollo Client
          //  OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            //httpClient.addInterceptor(interceptor);
         //   httpClient.connectTimeout(180, TimeUnit.SECONDS);
          //  httpClient.readTimeout(180, TimeUnit.SECONDS);
          //  HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
         //   logging.setLevel(HttpLoggingInterceptor.Level.BODY);
         //    httpClient.addInterceptor(logging);  // <-- this is the important line!
           // httpClient.addNetworkInterceptor(interceptor);
          //  httpClient.dispatcher(dispatcher);

            String serverUrl = "https://staging.api.desafrica.com/v1";
            OkHttpClient okHttpClient = new OkHttpClient
                    .Builder().build();

            apolloClient = ApolloClient.builder()
                    .serverUrl(serverUrl)
                    .okHttpClient(okHttpClient)
                    .build();
        }

        return apolloClient;
    }
}