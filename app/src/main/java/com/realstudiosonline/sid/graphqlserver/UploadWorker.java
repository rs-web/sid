package com.realstudiosonline.sid.graphqlserver;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.WorkerParameters;
import androidx.work.rxjava3.RxWorker;
import io.reactivex.rxjava3.core.Single;

public class UploadWorker extends RxWorker {


    /**
     * @param appContext   The application {@link Context}
     * @param workerParams Parameters to setup the internal state of this worker
     */
    public UploadWorker(@NonNull Context appContext, @NonNull WorkerParameters workerParams) {
        super(appContext, workerParams);
    }

    Result result = Result.failure();

/*    val remoteService = RemoteService()

    override fun createWork(): Single<Result> {
        return remoteService.getMySingleResponse()
                .doOnSuccess { *//* process result somehow *//* }
                .map { Result.success() }
                .onErrorReturn { Result.failure() }
    }*/
    @NonNull
    @Override
    public Single<Result> createWork() {


       /* return Observable.range(0, 100)
                .flatMap { download("https://www.example.com") }
            .toList()
                .map { Result.success() };*/


       /* Server.addStudent(
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "").observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<Response<AddStudentMutation.Data>>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull Response<AddStudentMutation.Data> dataResponse) {
                response = dataResponse;

                Data outputData = new Data.Builder()
                        .putString("UPLOAD_RESPONSE", dataResponse.getData().sid().addStudent().id())
                        .build();

                result = Result.success(outputData);
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                result = Result.failure();
            }

            @Override
            public void onComplete() {

            }
        });*/
        return Single.just(result);
    }
}
