package com.realstudiosonline.sid.models;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class PressReport {



    public final String reportType;

    // ... type definitions
    // Describes when the annotation will be discarded
    @Retention(RetentionPolicy.SOURCE)
    // Enumerate valid values for this interface
    @StringDef({
            START,NONE,LOADING,PRINTED,SUBMITTED
    })
    // Create an interface for validating String types
    public @interface FilterReportTypeDef {}
    // Declare the constants
    public static final String START = "Start";
    public static final String NONE = "None";
    public static final String LOADING = "Loading";
    public static final String PRINTED = "Printed";
    public static final String SUBMITTED = "Submitted";
    // Mark the argument as restricted to these enumerated types
    public PressReport(@FilterReportTypeDef String reportType) {
        this.reportType = reportType;
    }
}
