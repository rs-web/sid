package com.realstudiosonline.sid.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

@Entity(tableName = "student")
public class StudentRoom {

    @PrimaryKey
    private int id;
    @TypeConverters(StudentInfo.class)
    private StudentInfo info;

    public StudentRoom() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



}
