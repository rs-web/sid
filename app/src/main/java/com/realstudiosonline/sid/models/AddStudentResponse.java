package com.realstudiosonline.sid.models;

public class AddStudentResponse {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "AddStudentResponse{" +
                "message='" + message + '\'' +
                '}';
    }

    /* private String id;
    private String cardId;
    private String firstName;
    private String lastName;
    private String dob;
    private String gender;
    private String guardianPhone;
    private String profileImg;
    private String leftForeFingure;
    private String rightForeFingure;
    private String leftThumbFingure;
    private String rightThumbFingure;
    private String cardImg;
    private String createdAt;
    private String updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGuardianPhone() {
        return guardianPhone;
    }

    public void setGuardianPhone(String guardianPhone) {
        this.guardianPhone = guardianPhone;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public String getLeftForeFingure() {
        return leftForeFingure;
    }

    public void setLeftForeFingure(String leftForeFingure) {
        this.leftForeFingure = leftForeFingure;
    }

    public String getRightForeFingure() {
        return rightForeFingure;
    }

    public void setRightForeFingure(String rightForeFingure) {
        this.rightForeFingure = rightForeFingure;
    }

    public String getLeftThumbFingure() {
        return leftThumbFingure;
    }

    public void setLeftThumbFingure(String leftThumbFingure) {
        this.leftThumbFingure = leftThumbFingure;
    }

    public String getRightThumbFingure() {
        return rightThumbFingure;
    }

    public void setRightThumbFingure(String rightThumbFingure) {
        this.rightThumbFingure = rightThumbFingure;
    }

    public String getCardImg() {
        return cardImg;
    }

    public void setCardImg(String cardImg) {
        this.cardImg = cardImg;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }*/
}
