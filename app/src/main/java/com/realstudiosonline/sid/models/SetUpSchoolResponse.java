package com.realstudiosonline.sid.models;

public class SetUpSchoolResponse {

    /*
    * {"data":{"id":350,"schoolCode":"0020102",
    * "contactPerson":"Nick","contactNumber":"0245422636",
    * "status":"started","card_count":0,
    * "isSchoolSetupAlready":false}}*/
    private int id;
    private String schoolCode;
    private String contactPerson;
    private String contactNumber;
    private String status;
    private String cardCount;
    private int card_count;
    private boolean isSchoolSetupAlready;

    public int getCard_count() {
        return card_count;
    }

    public void setCard_count(int card_count) {
        this.card_count = card_count;
    }

    public boolean isSchoolSetupAlready() {
        return isSchoolSetupAlready;
    }

    public void setSchoolSetupAlready(boolean schoolSetupAlready) {
        isSchoolSetupAlready = schoolSetupAlready;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSchoolCode() {
        return schoolCode;
    }

    public void setSchoolCode(String schoolCode) {
        this.schoolCode = schoolCode;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCardCount() {
        return cardCount;
    }

    public void setCardCount(String cardCount) {
        this.cardCount = cardCount;
    }

    @Override
    public String toString() {
        return "SetUpSchoolResponse{" +
                "id=" + id +
                ", schoolCode='" + schoolCode + '\'' +
                ", contactPerson='" + contactPerson + '\'' +
                ", contactNumber='" + contactNumber + '\'' +
                ", status='" + status + '\'' +
                ", cardCount='" + cardCount + '\'' +
                ", card_count=" + card_count +
                ", isSchoolSetupAlready=" + isSchoolSetupAlready +
                '}';
    }
}
