package com.realstudiosonline.sid.models;

public class GenerateCardSelected {
    private int id;

    public GenerateCardSelected(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
