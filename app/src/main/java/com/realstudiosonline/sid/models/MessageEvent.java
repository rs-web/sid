package com.realstudiosonline.sid.models;

public class MessageEvent {
    private boolean eventCalled;
    private String message;
    private int id;

    public MessageEvent(boolean eventCalled, String message, int id) {
        this.eventCalled = eventCalled;
        this.message = message;
        this.id = id;
    }

    public boolean isEventCalled() {
        return eventCalled;
    }

    public void setEventCalled(boolean eventCalled) {
        this.eventCalled = eventCalled;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "MessageEvent{" +
                "eventCalled=" + eventCalled +
                ", message='" + message + '\'' +
                ", id=" + id +
                '}';
    }
}
