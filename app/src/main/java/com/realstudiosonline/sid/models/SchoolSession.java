package com.realstudiosonline.sid.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "school_session")
public class SchoolSession {

    @ColumnInfo(name = "school_code")
    @NonNull
    @PrimaryKey
    private String school_code;

    @ColumnInfo(name = "school_name")
    private String school_name;

    @ColumnInfo(name = "session_date")
    private String session_date;

    @ColumnInfo(name = "project_officer")
    private String project_officer;

    @ColumnInfo(name = "current_session")
    private String current_session;

    @ColumnInfo(name = "updated_at")
    private String updated_at;



    public SchoolSession() {
    }

    @NonNull
    public String getSchool_code() {
        return school_code;
    }

    public void setSchool_code(@NonNull String school_code) {
        this.school_code = school_code;
    }

    public String getSession_date() {
        return session_date;
    }

    public void setSession_date(String session_date) {
        this.session_date = session_date;
    }

    public String getProject_officer() {
        return project_officer;
    }

    public void setProject_officer(String project_officer) {
        this.project_officer = project_officer;
    }

    public String getCurrent_session() {
        return current_session;
    }

    public void setCurrent_session(String current_session) {
        this.current_session = current_session;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    @Override
    public String toString() {
        return "SchoolSession{" +
                "school_code='" + school_code + '\'' +
                ", school_name='" + school_name + '\'' +
                ", session_date='" + session_date + '\'' +
                ", project_officer='" + project_officer + '\'' +
                ", current_session='" + current_session + '\'' +
                ", updated_at='" + updated_at + '\'' +
                '}';
    }
}
