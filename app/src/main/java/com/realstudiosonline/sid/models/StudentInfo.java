package com.realstudiosonline.sid.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "student_info")
public class StudentInfo {

    @PrimaryKey
    private int id;
    private String school_code;
    private String school;
    private String region;
    private String index_number_jhs;
    private String student_name;
    private String year_group;
    private String program;
    private String date_of_birth;
    private String residential_status;
    private String student_mobile_number;

    private String edited_name;

    public String getEdited_name() {
        return edited_name;
    }

    public void setEdited_name(String edited_name) {
        this.edited_name = edited_name;
    }

    @SerializedName("Guardian name")
    private String guardian_name;
    @SerializedName("Guardian Mobile")
    private String guardian_mobile;

    private String shs_number;

    private String other_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSchool_code() {
        return school_code;
    }

    public void setSchool_code(String school_code) {
        this.school_code = school_code;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getIndex_number_jhs() {
        return index_number_jhs;
    }

    public void setIndex_number_jhs(String index_number_jhs) {
        this.index_number_jhs = index_number_jhs;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getYear_group() {
        return year_group;
    }

    public void setYear_group(String year_group) {
        this.year_group = year_group;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getResidential_status() {
        return residential_status;
    }

    public void setResidential_status(String residential_status) {
        this.residential_status = residential_status;
    }

    public String getStudent_mobile_number() {
        return student_mobile_number;
    }

    public void setStudent_mobile_number(String student_mobile_number) {
        this.student_mobile_number = student_mobile_number;
    }

    public String getGuardian_name() {
        return guardian_name;
    }

    public void setGuardian_name(String guardian_name) {
        this.guardian_name = guardian_name;
    }

    public String getGuardian_mobile() {
        return guardian_mobile;
    }

    public void setGuardian_mobile(String guardian_mobile) {
        this.guardian_mobile = guardian_mobile;
    }

    public String getShs_number() {
        return shs_number;
    }

    public void setShs_number(String shs_number) {
        this.shs_number = shs_number;
    }

    public String getOther_id() {
        return other_id;
    }

    public void setOther_id(String other_id) {
        this.other_id = other_id;
    }

    @Override
    public String toString() {
        return student_name;
    }
}
