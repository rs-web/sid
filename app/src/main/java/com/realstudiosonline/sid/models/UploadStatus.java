package com.realstudiosonline.sid.models;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class UploadStatus {

    public final String uploadStatusType;
    // ... type definitions
    // Describes when the annotation will be discarded
    @Retention(RetentionPolicy.SOURCE)
    // Enumerate valid values for this interface
    @StringDef({ SENT,READY,FAILED,SUBMITTING})
    // Create an interface for validating String types
    public @interface FilterSessionTypeDef {}
    // Declare the constants
    public static final String SENT = "Sent";
    public static final String READY = "Ready";
    public static final String FAILED = "Failed";
    public static final String SUBMITTING = "Submitting";

    // Mark the argument as restricted to these enumerated types
    public UploadStatus(@FilterSessionTypeDef String uploadStatusType) {
        this.uploadStatusType = uploadStatusType;
    }
}
