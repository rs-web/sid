package com.realstudiosonline.sid.models;

public class Student {
    private int id;
    private StudentInfo info;

    public Student() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public StudentInfo getInfo() {
        return info;
    }

    public void setInfo(StudentInfo info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", info=" + info +
                '}';
    }

    /* "id": 1,
            "info": {
                "school_code": "0030403",
                "school": "Abakrampa Senior High/Tech",
                "region": "CENTRAL REGION",
                "index_number_jhs": "031101000118",
                "student_name": "ABAIDOO ESTHER",
                "year_group": "2018",
                "program": "General Arts",
                "date_of_birth": "",
                "residential_status": "BOARDING",
                "student_mobile_number": "558159911",
                "Guardian name": "",
                "Guardian Mobile": "",
                "shs_number": "1800304030137",
                "other_id": "MS20001A18"
            },*/
}
