package com.realstudiosonline.sid.models;

public class StudentDone {
    private boolean isDone;

    public StudentDone(boolean isDone) {
        this.isDone = isDone;
    }
}
