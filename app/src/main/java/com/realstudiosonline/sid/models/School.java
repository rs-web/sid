package com.realstudiosonline.sid.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "school")
public class School implements Serializable {

    /*
    *  "school_id": 30,
            "school_name": "Accra Wesley Girls High School",
            "school_code": "0010148",
            "gender": "girls",
            "location": "Accra Metro",
            "district": "",
            "region": "",
            "form_one_count": 0,
            "form_two_count": 621,
            "form_three_count": 512
            *           "contact_person": "",
            "contact_person_number": ""
            * */
    @NonNull
    @PrimaryKey
    private int school_id;
    private String school_name;
    private String school_code;
    private String gender;
    private String location;
    private String district;
    private String region;
    private int form_two_count;
    private int form_one_count;
    private int form_three_count;
    private String contact_person;
    private String contact_person_number;
    private String class_id;
    private String sid_status;
    private String lat;
    private String lng;
    private String crestPath;


    public School() {
    }

    public int getSchool_id() {
        return school_id;
    }

    public void setSchool_id(int school_id) {
        this.school_id = school_id;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getSchool_code() {
        return school_code;
    }

    public void setSchool_code(String school_code) {
        this.school_code = school_code;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int getForm_two_count() {
        return form_two_count;
    }

    public void setForm_two_count(int form_two_count) {
        this.form_two_count = form_two_count;
    }

    public int getForm_one_count() {
        return form_one_count;
    }

    public void setForm_one_count(int form_one_count) {
        this.form_one_count = form_one_count;
    }

    public int getForm_three_count() {
        return form_three_count;
    }

    public void setForm_three_count(int form_three_count) {
        this.form_three_count = form_three_count;
    }

    public String getContact_person() {
        return contact_person;
    }

    public void setContact_person(String contact_person) {
        this.contact_person = contact_person;
    }

    public String getContact_person_number() {
        return contact_person_number;
    }

    public void setContact_person_number(String contact_person_number) {
        this.contact_person_number = contact_person_number;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getSid_status() {
        return sid_status;
    }

    public void setSid_status(String sid_status) {
        this.sid_status = sid_status;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return school_name;
    }

    public String getCrestPath() {
        return crestPath;
    }

    public void setCrestPath(String crestPath) {
        this.crestPath = crestPath;
    }
}
