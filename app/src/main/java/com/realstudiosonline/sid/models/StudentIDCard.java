package com.realstudiosonline.sid.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Arrays;
import java.util.Comparator;

@Entity(tableName = "student_id_card")
public class StudentIDCard implements Comparator<StudentIDCard> {


    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    private String id;

    @ColumnInfo(name = "school_id")
    private String schoolID;

    @ColumnInfo(name = "school_code")
    private String schoolCode;

    @ColumnInfo(name = "shs_number")
    private String shs_number;

    @ColumnInfo(name = "index_number_jhs")
    private String index_number_jhs;

    @ColumnInfo(name = "school_name")
    private String schoolName;

    @ColumnInfo(name = "full_name")
    private String fullName;

    @ColumnInfo(name = "other_name")
    private String otherNames;

    @ColumnInfo(name = "dob")
    private String dob;

    @ColumnInfo(name = "gender")
    private String gender;

    @ColumnInfo(name = "finger_print")
    private String fingerPrint;

    @ColumnInfo(name = "card")
    private String generatedCardURI;

    @ColumnInfo(name = "raw_profile_image")
    private String rawProfileImage;

    @ColumnInfo(name = "guardian_contact")
    private String guardianContact;

    @ColumnInfo(name = "expiration_date")
    private String expirationDate;

    @ColumnInfo(name = "issue_date")
    private String issueDate;


    @ColumnInfo(name = "student_id")
    private String studentID;

    @ColumnInfo(name = "class_id")
    private String classID;

    @ColumnInfo(name = "right_thumb",typeAffinity = ColumnInfo.BLOB)
    private byte[] rightThumb;


    @ColumnInfo(name = "right_fore_finger",typeAffinity = ColumnInfo.BLOB)
    private byte[] rightForeFinger;


    @ColumnInfo(name = "leftThumb",typeAffinity = ColumnInfo.BLOB)
    private byte[] leftThumb;


    @ColumnInfo(name = "leftForeFinger",typeAffinity = ColumnInfo.BLOB)
    private byte[] leftForeFinger;

    @ColumnInfo(name = "right_thumb_url")
    private String rightThumbURl;

    @ColumnInfo(name = "right_fore_finger_url")
    private String rightForeFingerURl;


    @ColumnInfo(name = "left_thumb_url")
    private String leftThumbURL;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @ColumnInfo(name = "left_fore_finger_url")
    private String leftForeFingerURL;

    @ColumnInfo(name = "status")
    private String status;

    public String getRightThumbURl() {
        return rightThumbURl;
    }

    public void setRightThumbURl(String rightThumbURl) {
        this.rightThumbURl = rightThumbURl;
    }

    public String getRightForeFingerURl() {
        return rightForeFingerURl;
    }

    public void setRightForeFingerURl(String rightForeFingerURl) {
        this.rightForeFingerURl = rightForeFingerURl;
    }

    public String getLeftThumbURL() {
        return leftThumbURL;
    }

    public void setLeftThumbURL(String leftThumbURL) {
        this.leftThumbURL = leftThumbURL;
    }

    public String getLeftForeFingerURL() {
        return leftForeFingerURL;
    }

    public void setLeftForeFingerURL(String leftForeFingerURL) {
        this.leftForeFingerURL = leftForeFingerURL;
    }


    public StudentIDCard() {
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFingerPrint() {
        return fingerPrint;
    }

    public void setFingerPrint(String fingerPrint) {
        this.fingerPrint = fingerPrint;
    }

    public String getGeneratedCardURI() {
        return generatedCardURI;
    }

    public void setGeneratedCardURI(String generatedCardURI) {
        this.generatedCardURI = generatedCardURI;
    }

    public String getRawProfileImage() {
        return rawProfileImage;
    }

    public void setRawProfileImage(String rawProfileImage) {
        this.rawProfileImage = rawProfileImage;
    }

    public String getGuardianContact() {
        return guardianContact;
    }

    public void setGuardianContact(String guardianContact) {
        this.guardianContact = guardianContact;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getClassID() {
        return classID;
    }


    public void setClassID(String classID) {
        this.classID = classID;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public byte[] getRightThumb() {
        return rightThumb;
    }

    public void setRightThumb(byte[] rightThumb) {
        this.rightThumb = Arrays.copyOf(rightThumb, rightThumb.length);
    }

    public byte[]  getRightForeFinger() {
        return rightForeFinger;
    }

    public void setRightForeFinger(@NonNull  byte[]  rightForeFinger) {
        this.rightForeFinger = Arrays.copyOf(rightForeFinger, rightForeFinger.length);
    }

    public byte[]  getLeftThumb() {
        return leftThumb;
    }

    public void setLeftThumb(@NonNull  byte[]  leftThumb) {
        this.leftThumb = Arrays.copyOf(leftThumb, leftThumb.length);
    }

    public byte[]  getLeftForeFinger() {
        return leftForeFinger;
    }

    public void setLeftForeFinger(@NonNull byte[]  leftForeFinger) {
        this.leftForeFinger = Arrays.copyOf(leftForeFinger, leftForeFinger.length);
    }

    public String getShs_number() {
        return shs_number;
    }

    public void setShs_number(String shs_number) {
        this.shs_number = shs_number;
    }

    public String getIndex_number_jhs() {
        return index_number_jhs;
    }

    public void setIndex_number_jhs(String index_number_jhs) {
        this.index_number_jhs = index_number_jhs;
    }

    public String getSchoolCode() {
        return schoolCode;
    }

    public void setSchoolCode(String schoolCode) {
        this.schoolCode = schoolCode;
    }


    @Override
    public String toString() {
        return "StudentIDCard{" +
                "id='" + id + '\'' +
                ", schoolID='" + schoolID + '\'' +
                ", schoolCode='" + schoolCode + '\'' +
                ", shs_number='" + shs_number + '\'' +
                ", index_number_jhs='" + index_number_jhs + '\'' +
                ", schoolName='" + schoolName + '\'' +
                ", fullName='" + fullName + '\'' +
                ", otherNames='" + otherNames + '\'' +
                ", dob='" + dob + '\'' +
                ", gender='" + gender + '\'' +
                ", fingerPrint='" + fingerPrint + '\'' +
                ", generatedCardURI='" + generatedCardURI + '\'' +
                ", rawProfileImage='" + rawProfileImage + '\'' +
                ", guardianContact='" + guardianContact + '\'' +
                ", expirationDate='" + expirationDate + '\'' +
                ", issueDate='" + issueDate + '\'' +
                ", studentID='" + studentID + '\'' +
                ", classID='" + classID + '\'' +
                ", rightThumbURl='" + rightThumbURl + '\'' +
                ", rightForeFingerURl='" + rightForeFingerURl + '\'' +
                ", leftThumbURL='" + leftThumbURL + '\'' +
                ", leftForeFingerURL='" + leftForeFingerURL + '\'' +
                ", Status=" + status +
                '}';
    }


    @Override
    public int compare(StudentIDCard studentIDCard1, StudentIDCard studentIDCard2) {
        return studentIDCard1.status.compareTo(studentIDCard2.status);
    }
}
