package com.realstudiosonline.sid.models;

public class FileUploader {
    private String contentType;
    private byte[] content;

    public FileUploader(String contentType, byte[] content) {

        this.contentType = contentType;
        this.content = content;
    }

    public String getContentType() {
        return contentType;
    }

    public byte[] getContent() {
        return content;
    }
}
