package com.realstudiosonline.sid.models;

public class StudentInfoSelected {
    private int id;

    public StudentInfoSelected(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
