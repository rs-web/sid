package com.realstudiosonline.sid.models;

public class SelectedFragment {
    private int id;
    private boolean selected;


    public SelectedFragment(int id, boolean selected) {
        this.id = id;
        this.selected = selected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
