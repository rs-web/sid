package com.realstudiosonline.sid.models;

public class DrawerSelectedEvent {
    private int id;
    private boolean isSelected;

    public DrawerSelectedEvent(int id, boolean isSelected) {
        this.id = id;
        this.isSelected = isSelected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
