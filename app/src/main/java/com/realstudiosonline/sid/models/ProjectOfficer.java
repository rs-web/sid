package com.realstudiosonline.sid.models;

public class ProjectOfficer {
    private String project_id;
    private String project_officer_id;


    public ProjectOfficer() {
    }

    public ProjectOfficer(String project_id, String project_officer_id) {
        this.project_id = project_id;
        this.project_officer_id = project_officer_id;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getProject_officer_id() {
        return project_officer_id;
    }

    public void setProject_officer_id(String project_officer_id) {
        this.project_officer_id = project_officer_id;
    }

    @Override
    public String toString() {
        return "ProjectOfficer{" +
                "project_id='" + project_id + '\'' +
                ", project_officer_id='" + project_officer_id + '\'' +
                '}';
    }
}
