package com.realstudiosonline.sid.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "session_logs")
public class SessionLogObject {

    @PrimaryKey
    @NonNull
    private String id;

    @ColumnInfo(name = "school_code")
    @NonNull
    private String school_code;

    @ColumnInfo(name = "session_log")
    private String session_log;

    @ColumnInfo(name = "session_date")
    private String session_date;

    @ColumnInfo(name = "project_officer")
    private String project_officer;

    @ColumnInfo(name = "project_id")
    private String project_id;

    @ColumnInfo(name = "school_name")
    private String school_name;



    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public SessionLogObject() {
    }



    @NonNull
    public String getSchool_code() {
        return school_code;
    }

    public void setSchool_code(@NonNull String school_code) {
        this.school_code = school_code;
    }

    public String getSession_log() {
        return session_log;
    }

    public void setSession_log(String session_log) {
        this.session_log = session_log;
    }

    public String getSession_date() {
        return session_date;
    }

    public void setSession_date(String session_date) {
        this.session_date = session_date;
    }

    public String getProject_officer() {
        return project_officer;
    }

    public void setProject_officer(String project_officer) {
        this.project_officer = project_officer;
    }


    public String  getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    @Override
    public String toString() {
        return "SessionLogObject{" +
                "id='" + id + '\'' +
                ", school_code='" + school_code + '\'' +
                ", session_log='" + session_log + '\'' +
                ", session_date='" + session_date + '\'' +
                ", project_officer='" + project_officer + '\'' +
                ", project_id='" + project_id + '\'' +
                ", school_name='" + school_name + '\'' +
                '}';
    }
}
