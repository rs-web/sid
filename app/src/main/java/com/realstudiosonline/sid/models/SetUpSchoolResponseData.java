package com.realstudiosonline.sid.models;

public class SetUpSchoolResponseData {

    private SetUpSchoolResponse data;

    public SetUpSchoolResponse getData() {
        return data;
    }

    public void setData(SetUpSchoolResponse data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SetUpSchoolResponseData{" +
                "data=" + data +
                '}';
    }
    /*
    * {"data":{"id":350,"schoolCode":"0020102",
    * "contactPerson":"Nick","contactNumber":"0245422636",
    * "status":"started","card_count":0,
    * "isSchoolSetupAlready":false}}*/
}
