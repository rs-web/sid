package com.realstudiosonline.sid.models;

public class PhotoBioSelected {
    private int id;

    public PhotoBioSelected(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
