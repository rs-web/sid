package com.realstudiosonline.sid.models;

public class WorkHistory {
    private SessionLog sessionLog;
    private PressReport pressReport;
    private String id;
    private String date;
    private String schoolName;


    public WorkHistory(SessionLog sessionLog, PressReport pressReport, String id, String date, String schoolName) {
        this.sessionLog = sessionLog;
        this.pressReport = pressReport;
        this.id = id;
        this.date = date;
        this.schoolName = schoolName;
    }

    public WorkHistory( String id, String date, String schoolName) {
        this.id = id;
        this.date = date;
        this.schoolName = schoolName;
    }

    public SessionLog getSessionLog() {
        return sessionLog;
    }

    public void setSessionLog(SessionLog sessionLog) {
        this.sessionLog = sessionLog;
    }

    public PressReport getPressReport() {
        return pressReport;
    }

    public void setPressReport(PressReport pressReport) {
        this.pressReport = pressReport;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    @Override
    public String toString() {
        return "WorkHistory{" +
                "sessionLog=" + sessionLog +
                ", pressReport=" + pressReport +
                ", id='" + id + '\'' +
                ", date='" + date + '\'' +
                ", schoolName='" + schoolName + '\'' +
                '}';
    }
}
