package com.realstudiosonline.sid.models;

import java.util.List;

public class SchoolDataResponse {
    List<School> data;

    public List<School> getData() {
        return data;
    }

    public void setData(List<School> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SchoolDataResponse{" +
                "data=" + data +
                '}';
    }
}
