package com.realstudiosonline.sid.models;

public class LoginResponse {

    private String token;
    private String project_id;
    private String project_officer_id;
    private String lastname;
    private String firstname;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getProject_officer_id() {
        return project_officer_id;
    }

    public void setProject_officer_id(String project_officer_id) {
        this.project_officer_id = project_officer_id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
}
