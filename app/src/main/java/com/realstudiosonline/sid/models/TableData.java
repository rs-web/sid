package com.realstudiosonline.sid.models;

import android.content.Context;

import java.util.List;

public class TableData implements TableDataSource<String, String,String, WorkHistory> {


    private int mRowsCount;
    private int mColumnsCount;
    private List<WorkHistory> workHistoryList;
    private final Context mContext;

    public TableData(int mRowsCount, int mColumnsCount, List<WorkHistory> workHistoryList, Context mContext) {
        this.mRowsCount = mRowsCount;
        this.mColumnsCount = mColumnsCount;
        this.workHistoryList = workHistoryList;
        this.mContext = mContext;
    }

    @Override
    public int getRowsCount() {
        return mRowsCount;
    }

    @Override
    public int getColumnsCount() {
        return mColumnsCount;
    }

    @Override
    public String getFirstHeaderData() {
        return null;
    }

    @Override
    public String getRowHeaderData(int index) {
           return getItemData(index, 0).getSchoolName();
    }

    @Override
    public String getColumnHeaderData(int index) {
         return getItemData(0, index).getSchoolName();
    }

    @Override
    public WorkHistory getItemData(int rowIndex, int columnIndex) {
        return workHistoryList.get(rowIndex);
    }





    /*@Override
    public String getFirstHeaderData() {
        return getItemData(0, 0);
    }

    @Override
    public String getRowHeaderData(int index) {
        return getItemData(index, 0);
    }

    @Override
    public String getColumnHeaderData(int index) {
        return getItemData(0, index);
    }

    @Override
    public WorkHistory getItemData(int rowIndex, int columnIndex) {
        try {
            List<String> rowList = getRow(rowIndex);
            return rowList == null ? "" : rowList.get(columnIndex);
        } catch (Exception e) {
            Log.e(TAG, "get rowIndex=" + rowIndex + "; colIndex=" + columnIndex + ";\ncache = " +
                    mItemsCache.toString(), e);
            return null;
        }
    }*/
}
