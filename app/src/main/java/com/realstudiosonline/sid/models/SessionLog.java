package com.realstudiosonline.sid.models;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class SessionLog {

    public final String sessionType;

    // ... type definitions
    // Describes when the annotation will be discarded
    @Retention(RetentionPolicy.SOURCE)
    // Enumerate valid values for this interface
    @StringDef({ START,PAUSE,CONTINUE,END, STARTED,ENDED,PAUSED})
    // Create an interface for validating String types
    public @interface FilterSessionTypeDef {}
    // Declare the constants
    public static final String START = "Start";
    public static final String PAUSE = "Pause";
    public static final String CONTINUE = "Continue";
    public static final String END = "End";
    public static final String STARTED = "Started";
    public static final String ENDED = "Ended";
    public static final String PAUSED = "Paused";

    // Mark the argument as restricted to these enumerated types
    public SessionLog(@FilterSessionTypeDef String sessionType) {
        this.sessionType = sessionType;
    }
}
