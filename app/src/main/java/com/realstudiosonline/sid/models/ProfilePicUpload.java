package com.realstudiosonline.sid.models;

public class ProfilePicUpload {

    private String contentType;
    private byte[] content;

    public ProfilePicUpload(String contentType, byte[] content) {
        this.contentType = contentType;
        this.content = content;
    }

    public String getContentType() {
        return contentType;
    }

    public byte[] getContent() {
        return content;
    }
}
