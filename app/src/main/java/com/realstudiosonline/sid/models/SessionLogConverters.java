package com.realstudiosonline.sid.models;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class SessionLogConverters {

    @TypeConverter
    public static SessionLog fromString(String value) {
        Type listType = new TypeToken<SessionLog>() {}.getType();
        return new Gson().fromJson(value, listType);
    }
    @TypeConverter
    public static String fromSessionLog(SessionLog sessionLog) {
        Gson gson = new Gson();
        return gson.toJson(sessionLog);
    }
}
