package com.realstudiosonline.sid.models;

import java.util.List;

public class StudentsResponse {
   // private int last_page;
    //private int current_page;
    private List<Student> data;
    //private int total;

    public List<Student> getData() {
        return data;
    }

    public void setData(List<Student> data) {
        this.data = data;
    }


    @Override
    public String toString() {
        return "StudentsResponse{" +
                "data=" + data +
                '}';
    }
}
