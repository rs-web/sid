package com.realstudiosonline.sid.models;

public class GeneratedCard{
    private int id;
    private String cardUrl;
    private String cardNo;


    public GeneratedCard(int id, String cardUrl, String cardNo) {
        this.id = id;
        this.cardUrl = cardUrl;
        this.cardNo = cardNo;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCardUrl() {
        return cardUrl;
    }

    public void setCardUrl(String cardUrl) {
        this.cardUrl = cardUrl;
    }
}
