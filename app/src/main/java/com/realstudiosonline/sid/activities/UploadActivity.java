package com.realstudiosonline.sid.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Pair;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.api.ApiClient;
import com.realstudiosonline.sid.models.AddStudentResponse;
import com.realstudiosonline.sid.models.MessageEvent;
import com.realstudiosonline.sid.models.ProjectOfficer;
import com.realstudiosonline.sid.models.StudentIDCard;
import com.realstudiosonline.sid.models.UploadStatus;
import com.realstudiosonline.sid.room.StudentIDListAdapter;
import com.realstudiosonline.sid.room.StudentIDViewModel;
import com.realstudiosonline.sid.utils.PreferenceHelper;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.observers.DisposableObserver;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.sentry.Sentry;
import io.sentry.SentryLevel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

import static java.util.concurrent.Executors.newSingleThreadExecutor;

public class UploadActivity extends AppCompatActivity implements StudentIDListAdapter.setOnCardSelectedListener{

    private RecyclerView recyclerView;
    private StudentIDViewModel mStudentViewModel;
    private View imgEmpty;
    private StudentIDListAdapter studentIDListAdapter;
    AppBarLayout appBarLayout;
    View submitLayout;
    private final String TAG = "UploadActivity";
    ProjectOfficer projectOfficer;
    List<StudentIDCard> studentIDCardList;
    Toolbar toolbar;
    AlertDialog alertDialogInternetConnectivity,alertDialogExit;
    AppCompatSeekBar seekBar;
    boolean isSubmitting = false;
    double total =0;
    double submitted = 0;
    double percentage = 0;
    MaterialTextView tvTotalSubmitted, tvTotalPending;
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        appBarLayout = findViewById(R.id.appbar);

        tvTotalSubmitted = findViewById(R.id.tv_total_submitted);
        tvTotalPending = findViewById(R.id.tv_total_pending);

        seekBar = findViewById(R.id.seek_bar_success);


        seekBar.setOnTouchListener((view, motionEvent) -> true);

        studentIDCardList = new ArrayList<>();

        findViewById(R.id.btn_close).setOnClickListener(v ->{
            if(alertDialogExit==null)
                createExitDialog();
            alertDialogExit.show();
        });

        String projectOfficerJson = PreferenceHelper.getPreferenceHelperInstace().getString(this, "projectOfficer", null);

        if(projectOfficerJson!=null){

            projectOfficer =  new Gson().fromJson(projectOfficerJson, ProjectOfficer.class);
        }


        submitLayout = findViewById(R.id.submit_layout);


        findViewById(R.id.generate_dummy).setOnClickListener(v -> {
            isSubmitting = true;
            mStudentViewModel.getAllUnSubmitted(UploadStatus.FAILED)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe(new SingleObserver<List<StudentIDCard>>() {
                @Override
                public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                    //d.dispose();
                }

                @Override
                public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull List<StudentIDCard> studentIDCards) {
                    if(studentIDCards.size()>0) {
                        generateDummy(studentIDCards);
                    }

                }

                @Override
                public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                    e.printStackTrace();
                    Log.d(TAG, "onError:"+e.getMessage());
                }
            });

        });
        submitLayout.setOnClickListener(v -> {

            isSubmitting = true;
            mStudentViewModel.getAllUnSubmitted(UploadStatus.SENT)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe(new SingleObserver<List<StudentIDCard>>() {
                @Override
                public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                    //d.dispose();
                }

                @Override
                public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull List<StudentIDCard> studentIDCards) {
                    Log.d(TAG, "studentIDCards:"+studentIDCards.toString());
                    String imei = PreferenceHelper.getPreferenceHelperInstace().getString(getApplicationContext(),"IMEI","12345");


                    if(projectOfficer!=null) {
                        if(studentIDCards.size()>0) {
                            processData(studentIDCards, imei, projectOfficer.getProject_officer_id(),
                                    projectOfficer.getProject_id());
                        }
                    }
                    else {
                        Toast.makeText(UploadActivity.this, "Error Occurred.\nLogout and login again", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                    e.printStackTrace();
                    Log.d(TAG, "onError:"+e.getMessage());
                }
            });

        });

        appBarLayout.setExpanded(true, true);



        imgEmpty = findViewById(R.id.empty_view);

        // generatedCards = new ArrayList<>();

        recyclerView = findViewById(R.id.recycler_view_review_cards);
        studentIDListAdapter = new StudentIDListAdapter(new StudentIDListAdapter.StudentIDDiff(), this, UploadActivity.this);
        // ReviewCardsAdapter reviewCardsAdapter = new ReviewCardsAdapter(getGeneratedCards());

        recyclerView.setLayoutManager(new GridLayoutManager(UploadActivity.this, 2));
          /*  ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(5,2);
            recyclerView.addItemDecoration(itemDecoration);*/
        recyclerView.setAdapter(studentIDListAdapter);


        // Get a new or existing ViewModel from the ViewModelProvider.
        mStudentViewModel = new ViewModelProvider(this).get(StudentIDViewModel.class);

        createExitDialog();

       new Handler().postDelayed(()-> mStudentViewModel.getAllStudentIDs().observe(this, studentIDCards -> {
           submitted = 0;
           total = studentIDCards.size();
           if(studentIDCards.size() == 0){
               imgEmpty.setVisibility(View.VISIBLE);
               recyclerView.setVisibility(View.GONE);

           }

           for (StudentIDCard studentIDCard:studentIDCards) {

               if(studentIDCard.getStatus().equals(UploadStatus.SENT))
                   submitted+=1;
           }

          // submitted = submitted-500;
           String submittedText = "Submitted: "+((int)submitted)+"/"+((int)total);
           int pending = (int)(total- submitted);
           String pendingText = "Pending: "+pending+ "/"+((int)total);
           percentage =(submitted/total)*100;
           runOnUiThread(() -> {
               tvTotalSubmitted.setText(submittedText);
               tvTotalPending.setText(pendingText);
               seekBar.post(() -> seekBar.setProgress((int)percentage));
           });
           studentIDCardList.clear();
           Collections.sort(studentIDCards, (o1, o2) -> o1.getStatus().compareTo(o2.getStatus()));
           studentIDCardList.addAll(studentIDCards);
           studentIDListAdapter.submitList(studentIDCards);
       }),200);

    }


    private boolean internetConnectionAvailable() {
        InetAddress inetAddress = null;
        try {
            Future<InetAddress> future = newSingleThreadExecutor().submit(() -> {
                try {
                    return InetAddress.getByName("google.com");
                } catch (UnknownHostException e) {
                    return null;
                }
            });
            inetAddress = future.get(2000, TimeUnit.MILLISECONDS);
            future.cancel(true);
        }
        catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
        return inetAddress!=null && !inetAddress.toString().equals("");
    }

    public  boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }



    private static MultipartBody.Part createFormData(String pathName, File file){
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        return MultipartBody.Part.createFormData(pathName, file.getName(),requestFile);
    }

    private static MultipartBody.Part createFormData(String pathName,String name, byte [] file){
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        return MultipartBody.Part.createFormData(pathName,name,requestFile);
    }


    private static RequestBody createRequestBody(String value){

        return RequestBody.create(MediaType.parse("text/plain"), value==null ? "": value);
    }


    private void generateDummy(List<StudentIDCard> studentIDCards){
              for (int i=0; i<studentIDCards.size(); i++) {
            StudentIDCard studentIDCard = studentIDCards.get(i);
                  studentIDCard.setFullName(studentIDCard.getFullName()+""+i);
            studentIDCard.setId(UUID.randomUUID().toString());
            mStudentViewModel.insert(studentIDCard);
        }
    }

    private void processData(List<StudentIDCard> studentIDCards,
                             String iMEI, String projectOfficerId,String projectId){

        if(projectOfficer!=null) {

            if(isNetworkConnected()) {
                if(internetConnectionAvailable()){


                    for (StudentIDCard studentIDCard:studentIDCards) {
                        File rawFileSize = new File(studentIDCard.getRawProfileImage());
                        long len = rawFileSize.length()/(1024*1024);
                        Log.d("RAW_FILE_SIZE", "file size:"+len);
                        mStudentViewModel.updateStudent(UploadStatus.SUBMITTING, studentIDCard.getId());
                    }

                    runOnUiThread(() -> submitLayout.setVisibility(View.GONE));

                    Observable.fromIterable(studentIDCards).flatMap((Function<StudentIDCard, ObservableSource<androidx.core.util.Pair<String,
                            Response<AddStudentResponse>>>>) studentIDCard -> Observable.zip(
                            Observable.just(studentIDCard.getId()),
                            ApiClient.getApiClient().getInstance().addStudent(
                                    createRequestBody("completed"),
                                    createRequestBody(iMEI),
                                    createRequestBody(projectId),
                                    createRequestBody(projectOfficerId),
                                    createRequestBody(studentIDCard.getClassID()),
                                    createRequestBody(studentIDCard.getSchoolCode()),
                                    createRequestBody(studentIDCard.getStudentID()),
                                    createRequestBody(studentIDCard.getFullName()),
                                    createRequestBody(studentIDCard.getFullName()),
                                    createRequestBody(studentIDCard.getDob()),
                                    createRequestBody(studentIDCard.getGender()),
                                    createRequestBody(studentIDCard.getGuardianContact()),
                                    createFormData("bioRightThumb", "bioRightThumb",studentIDCard.getRightThumb()),
                                    createFormData("bioLeftThumb", "bioLeftThumb",studentIDCard.getLeftThumb()),
                                    createFormData("bioRightFore", "bioRightFore", studentIDCard.getRightForeFinger()),
                                    createFormData("bioLeftFore", "bioLeftFore", studentIDCard.getLeftForeFinger()),
                                    createFormData("profile", new File(studentIDCard.getRawProfileImage()))
                                    /* createFormData("card",new File(studentIDCard.getGeneratedCardURI()))*/
                            ),
                            androidx.core.util.Pair::new))
                            .onErrorResumeNext(throwable -> Observable.empty())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(new DisposableObserver<Pair<String,
                                    Response<AddStudentResponse>>>() {
                                @Override
                                public void onNext(@NonNull Pair<String, Response<AddStudentResponse>> studentResponsePair) {
                                    try {
                                        Sentry.captureMessage("Log of onNext in upload task: "+studentResponsePair.second.toString(), SentryLevel.INFO);
                                        Sentry.captureMessage("Log of onNext in upload message: "+studentResponsePair.second.body(), SentryLevel.INFO);

                                    }
                                    catch (Exception e){
                                        Sentry.captureException(e,"Exception trying to access studentResponsePair.second.toString() ");
                                    }
                                    if(studentResponsePair.second!=null) {

                                        //Sentry.captureMessage("Something went wrong");

                                        if(studentResponsePair.second.isSuccessful()){
                                            submitted+=1;
                                            percentage = (submitted/total)*100;

                                            Log.d("ProgressPercentage", "percentage: "+percentage);
                                            Log.d("ProgressPercentage", "submitted: "+submitted);
                                            Log.d("ProgressPercentage", "total: "+total);

                                            runOnUiThread(() -> seekBar.post(() -> seekBar.setProgress((int)percentage)));
                                            AddStudentResponse data = studentResponsePair.second.body();

                                            if(data!=null){
                                                Log.d(TAG,"addStudentResponse:"+studentResponsePair.second.toString());
                                                mStudentViewModel.updateStudent(new UploadStatus(UploadStatus.SENT).uploadStatusType,studentResponsePair.first);
                                               /* Log.d(TAG, "cardId:" + data.getCardId());
                                                Log.d(TAG, "cardImg:" + data.getCardImg());
                                                Log.d(TAG, "profileImg:" + data.getProfileImg());
                                                Log.d(TAG, "dob:" + data.getDob());
                                                Log.d(TAG, "firstName:" + data.getFirstName());
                                                Log.d(TAG, "lastName:" + data.getLastName());
                                                Log.d(TAG, "gender:" + data.getGender());
                                                Log.d(TAG, "guardianPhone:" + data.getGuardianPhone());
                                                Log.d(TAG, "id:" + data.getId());*/
                                            }

                                        }


                                        else {
                                            Sentry.captureMessage("Log of else in upload task: "+studentResponsePair.second.toString(), SentryLevel.INFO);
                                            Sentry.captureMessage("Log of else in upload message: "+studentResponsePair.second.body(), SentryLevel.INFO);

                                            mStudentViewModel.updateStudent(new UploadStatus(UploadStatus.FAILED).uploadStatusType,studentResponsePair.first);
                                            Log.d("addStudentResponse: ",studentResponsePair.second.toString());
                                            Toast.makeText(UploadActivity.this, ""+studentResponsePair.second.message(), Toast.LENGTH_SHORT).show();
                                        }
                                        //Log.d(TAG,"updatedAt:"+addStudent.updatedAt());

                                    }
                                    else{
                                        Sentry.captureMessage("Log upload task: "+studentResponsePair.second.toString(), SentryLevel.INFO);
                                        Sentry.captureMessage("Log of  in upload message: "+studentResponsePair.second.body(), SentryLevel.INFO);

                                        Log.d(TAG, "Error occurred in adding student");
                                    }

                                }

                                @Override
                                public void onError(@NonNull Throwable e) {

                                    Sentry.captureException(e, SentryLevel.ERROR);

                                    Toast.makeText(UploadActivity.this, "onError", Toast.LENGTH_SHORT).show();
                                    runOnUiThread(() -> submitLayout.setVisibility(View.VISIBLE));

                                    mStudentViewModel.getAllUnSubmitted(UploadStatus.SENT)
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribeOn(Schedulers.io()).subscribe(new SingleObserver<List<StudentIDCard>>() {
                                        @Override
                                        public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                                            //d.dispose();
                                        }

                                        @Override
                                        public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull List<StudentIDCard> studentIDCards) {

                                            for (StudentIDCard studentIDCard:studentIDCards) {
                                                mStudentViewModel.updateStudent(UploadStatus.FAILED, studentIDCard.getId());
                                            }
                                        }

                                        @Override
                                        public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                                            e.printStackTrace();
                                            Log.d(TAG, "onError:"+e.getMessage());
                                        }
                                    });

                                    e.printStackTrace();
                                }

                                @Override
                                public void onComplete() {

                                    runOnUiThread(() -> submitLayout.setVisibility(View.VISIBLE));
                                    mStudentViewModel.getAllUnSubmitted(UploadStatus.SENT)
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribeOn(Schedulers.io()).subscribe(new SingleObserver<List<StudentIDCard>>() {
                                        @Override
                                        public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                                            //d.dispose();
                                        }

                                        @Override
                                        public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull List<StudentIDCard> studentIDCards) {

                                            for (StudentIDCard studentIDCard:studentIDCards) {
                                                mStudentViewModel.updateStudent(UploadStatus.FAILED, studentIDCard.getId());
                                            }
                                        }

                                        @Override
                                        public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                                            e.printStackTrace();
                                            Log.d(TAG, "onError:"+e.getMessage());
                                        }
                                    });

                                }
                            });

                }

                else {
                    if(alertDialogInternetConnectivity==null)
                        createInternetConnectionDialog();
                    alertDialogInternetConnectivity.show();

                }
            }
            else {
                if(alertDialogInternetConnectivity==null)
                    createInternetConnectionDialog();
                alertDialogInternetConnectivity.show();
            }



        }
        else {
            Toast.makeText(UploadActivity.this, "Error occurred in submitting.", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    protected void onPause() {
        if(alertDialogExit!=null)
            alertDialogExit.cancel();
        if(alertDialogInternetConnectivity!=null)
            alertDialogInternetConnectivity.cancel();
        super.onPause();

    }



    @Override
    public void onBackPressed() {

        if(alertDialogExit==null)
            createExitDialog();
        alertDialogExit.show();


    }
    @Override
    protected void onDestroy() {
        if(alertDialogExit!=null)
            alertDialogExit.cancel();
        if(alertDialogInternetConnectivity!=null)
            alertDialogInternetConnectivity.cancel();
        super.onDestroy();
    }



    private void createExitDialog(){

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this,R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.alert_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialogExit =  builder.create();

        MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);

        tv_message.setText(R.string.exit_confirmation);
        AppCompatTextView btnNo = alert_view.findViewById(R.id.btn_no);
        btnNo.setOnClickListener(v -> alertDialogExit.hide());

        AppCompatTextView btnYes =  alert_view.findViewById(R.id.btn_yes);
        btnYes.setOnClickListener(v -> finish());

        alertDialogExit.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialogExit.getWindow()!=null)
            alertDialogExit.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }

    @SuppressLint("SetTextI18n")
    private void createInternetConnectionDialog(){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(UploadActivity.this,
                R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.alert_dialog_warning, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);
        if(tv_message!=null){
            String message = "<b>Connect to active internet to proceed</b>";
            tv_message.setText(Html.fromHtml(message));
        }
        AppCompatTextView btn_no = alert_view.findViewById(R.id.btn_no);
        if(btn_no!=null){
            btn_no.setVisibility(View.GONE);
        }
        AppCompatTextView btn_yes = alert_view.findViewById(R.id.btn_yes);
        if(btn_yes!=null){
            btn_yes.setText("Cancel");
            btn_yes.setOnClickListener(v -> alertDialogInternetConnectivity.dismiss());
        }
        alertDialogInternetConnectivity =  builder.create();
        alertDialogInternetConnectivity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialogInternetConnectivity.getWindow()!=null)
            alertDialogInternetConnectivity.getWindow()
                    .setBackgroundDrawableResource(android.R.color.transparent);

    }


    @Override
    public void onCardSelected(StudentIDCard studentIDCard) {
        EventBus.getDefault().post(studentIDCard);
        EventBus.getDefault().post(new MessageEvent(false, "Student Info",1));
    }

    @Override
    public void onCardLongPress(StudentIDCard studentIDCard) {

        Toast.makeText(this, "STUDENT SHS: "+studentIDCard.getShs_number(), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, IDCardViewActivity.class);
        intent.putExtra("card_url", studentIDCard.getGeneratedCardURI());
        startActivity(intent);
    }
}