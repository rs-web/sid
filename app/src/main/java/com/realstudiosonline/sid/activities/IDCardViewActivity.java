package com.realstudiosonline.sid.activities;

import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.realstudiosonline.sid.R;

public class IDCardViewActivity extends AppCompatActivity {


    PhotoView photoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_id_card_view);

        if(getIntent()!=null) {
            String url = getIntent().getStringExtra("card_url");
            photoView = findViewById(R.id.photo_view);
            new Handler().postDelayed(() -> {
                Glide.with(this)
                        .asBitmap()
                        .load("file://"+url)
                        .placeholder(R.drawable.student_id)
                        .into(photoView);
            }, 200);
        }
    }
}
