package com.realstudiosonline.sid.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textview.MaterialTextView;
import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.utils.PreferenceHelper;

public class OnboardingActivity extends AppCompatActivity {

    AlertDialog  alertDialogExit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);

       // PreferenceHelper.getPreferenceHelperInstace().setBoolean(getApplicationContext(), "studentSetUp",false);
      /*  startActivity(new Intent(this, StudentInfoPhotoGenerateCardActivity.class));
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();*/

       // System.load("data/data/com.realstudiosonline.studentidcard/lib/arm64-v8a/libDevKitApi.so");
        boolean isLoggedIn = PreferenceHelper.getPreferenceHelperInstace().getBoolean(this, "isLoggedIn", false);
        String  token = PreferenceHelper.getPreferenceHelperInstace().getString(this, "token", null);
        boolean isSchoolSetUpAlready =  PreferenceHelper.getPreferenceHelperInstace().getBoolean(getApplicationContext(), "isSchoolSetUpAlready",false);
        boolean studentSetUp = PreferenceHelper.getPreferenceHelperInstace().getBoolean(getApplicationContext(), "studentSetUp",false);

        createExitDialog();
        Log.d("OnboardingActivity", "token "+token);
        Log.d("OnboardingActivity", "isLoggedIn "+isLoggedIn);
        Log.d("OnboardingActivity", "isSchoolSetUpAlready "+isSchoolSetUpAlready);
        Log.d("OnboardingActivity", "studentSetUp "+studentSetUp);

        if(isLoggedIn && token!=null){


           if(isSchoolSetUpAlready){
               if(studentSetUp){
                   startActivity(new Intent(this, StudentInfoPhotoGenerateCardActivity.class));
               }

               else startActivity(new Intent(this, SchoolInfoConfirmationStepOneActivity.class));
           }

           else {
               startActivity(new Intent(this, SchoolSetUpActivity.class));

           }
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();


        }


        findViewById(R.id.btn_project_officer).setOnClickListener(v -> {
            startActivity(new Intent(this, ProjectOfficerSignInActivity.class));
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();


        });

        findViewById(R.id.btn_student).setOnClickListener(v -> {
          //  Toast.makeText(this, "Student callback", Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public void onBackPressed() {

        if(alertDialogExit==null)
            createExitDialog();
        alertDialogExit.show();


    }

    @Override
    protected void onDestroy() {
        if(alertDialogExit!=null)
            alertDialogExit.cancel();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        if(alertDialogExit!=null)
            alertDialogExit.cancel();
        super.onPause();

    }

    private void createExitDialog(){

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this,R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.alert_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialogExit = (AlertDialog) builder.create();

        MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);

        tv_message.setText(R.string.exit_confirmation);
        AppCompatTextView btnNo = (AppCompatTextView) alert_view.findViewById(R.id.btn_no);
        btnNo.setOnClickListener(v -> alertDialogExit.hide());

        AppCompatTextView btnYes = (AppCompatTextView) alert_view.findViewById(R.id.btn_yes);
        btnYes.setOnClickListener(v -> {
            finish();
        });

        alertDialogExit.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialogExit.getWindow()!=null)
            alertDialogExit.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }




}