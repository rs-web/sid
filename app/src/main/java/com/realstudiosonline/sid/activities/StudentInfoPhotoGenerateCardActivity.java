package com.realstudiosonline.sid.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.util.Pair;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;

import com.apollographql.apollo.api.Error;
import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.adapter.StudentInfoPhotoPagerAdapter;
import com.realstudiosonline.sid.api.ApiClientSchools;
import com.realstudiosonline.sid.graphqlserver.Server;
import com.realstudiosonline.sid.models.DrawerSelectedEvent;
import com.realstudiosonline.sid.models.GenerateCardSelected;
import com.realstudiosonline.sid.models.GeneratedSuccess;
import com.realstudiosonline.sid.models.LoginResponse;
import com.realstudiosonline.sid.models.MessageEvent;
import com.realstudiosonline.sid.models.PhotoBioSelected;
import com.realstudiosonline.sid.models.ProjectOfficer;
import com.realstudiosonline.sid.models.ReviewCardSelected;
import com.realstudiosonline.sid.models.School;
import com.realstudiosonline.sid.models.SchoolSession;
import com.realstudiosonline.sid.models.SelectedFragment;
import com.realstudiosonline.sid.models.SessionLog;
import com.realstudiosonline.sid.models.SessionLogObject;
import com.realstudiosonline.sid.models.SetUpSchoolResponse;
import com.realstudiosonline.sid.models.StudentIDCard;
import com.realstudiosonline.sid.models.StudentInfoSelected;
import com.realstudiosonline.sid.models.UploadStatus;
import com.realstudiosonline.sid.room.SchoolSessionViewModel;
import com.realstudiosonline.sid.room.SchoolViewModel;
import com.realstudiosonline.sid.room.SessionLogViewModel;
import com.realstudiosonline.sid.room.StudentIDViewModel;
import com.realstudiosonline.sid.utils.PreferenceHelper;
import com.realstudiosonline.sid.utils.PxUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.observers.DisposableObserver;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static java.util.concurrent.Executors.newSingleThreadExecutor;


public class StudentInfoPhotoGenerateCardActivity extends AppCompatActivity{

    private static final String TAG = StudentInfoPhotoGenerateCardActivity.class.getSimpleName();
    DrawerLayout drawer;
    NavigationView navigationView;
    private int id = 0;
    ViewPager2 viewPager;
    StudentIDViewModel mStudentViewModel;
    SessionLogViewModel mSessionLogViewModel;
    SchoolSessionViewModel mSchoolSessionViewModel;
    LoginResponse user;
    AlertDialog alertDialogExit, alertDialogLogout,alertDialogEndSession, alertDialogInternetConnectivity,alertDialogProgress;
    ProjectOfficer projectOfficer;
    School school;
    private MaterialTextView tvCurrentSchool;
    private AppCompatImageView imageViewSchoolCrest;
    SessionLog sessionLog;
    private SchoolViewModel mSchoolViewModel;
    private void createLogOutDialog(){


        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(StudentInfoPhotoGenerateCardActivity.this,R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.alert_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialogLogout = (AlertDialog) builder.create();
        MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);

        tv_message.setText(R.string.logout_confirmation_message);
        AppCompatTextView btnNo = (AppCompatTextView) alert_view.findViewById(R.id.btn_no);
        btnNo.setOnClickListener(v -> alertDialogLogout.hide());

        AppCompatTextView btnYes = (AppCompatTextView) alert_view.findViewById(R.id.btn_yes);
        btnYes.setOnClickListener(v -> {

            mStudentViewModel.getAllUnSubmitted(UploadStatus.SENT)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe(new SingleObserver<List<StudentIDCard>>() {
                @Override
                public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                    //d.dispose();
                }

                @Override
                public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull List<StudentIDCard> studentIDCards) {
                    Log.d(TAG, "studentIDCards:"+studentIDCards.toString());
                    //Toast.makeText(StudentInfoPhotoGenerateCardActivity.this, "studentIDCards: size -> "+studentIDCards.size(), Toast.LENGTH_SHORT).show();
                    String imei = PreferenceHelper.getPreferenceHelperInstace().getString(getApplicationContext(),"IMEI",UUID.randomUUID().toString());

                    if(studentIDCards.size()>0)
                    {
                        Toast.makeText(StudentInfoPhotoGenerateCardActivity.this, "Submit session before logging out", Toast.LENGTH_LONG).show();
                    }
                    else {
                        PreferenceHelper.getPreferenceHelperInstace().setString(
                                StudentInfoPhotoGenerateCardActivity.this,"user", "");
                        PreferenceHelper.getPreferenceHelperInstace().setString(StudentInfoPhotoGenerateCardActivity.this, "token", "");
                        PreferenceHelper.getPreferenceHelperInstace().setBoolean(StudentInfoPhotoGenerateCardActivity.this, "isLoggedIn", false);
                        PreferenceHelper.getPreferenceHelperInstace().setBoolean(getApplicationContext(), "isSchoolSetUpAlready",false);
                        startActivity(new Intent(StudentInfoPhotoGenerateCardActivity.this, ProjectOfficerSignInActivity.class));
                        finish();
                    }
                }

                @Override
                public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                    e.printStackTrace();
                    Log.d(TAG, "onError:"+e.getMessage());
                }
            });


        });

        alertDialogLogout.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialogLogout.getWindow()!=null)
            alertDialogLogout.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }



    private void downloadSchoolCrest(String school_code){

        ApiClientSchools.getApiClient().getInstance().downloadSchoolCrest(school_code.trim()+".png")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(
                new DisposableSingleObserver<retrofit2.Response<ResponseBody>>() {
                    @Override
                    public void onSuccess(@NonNull Response<ResponseBody> responseBodyResponse) {

                        if(responseBodyResponse.isSuccessful()) {
                            try {
                                if (responseBodyResponse.body() != null) {
                                    File path = new File(getExternalFilesDir(
                                            Environment.DIRECTORY_PICTURES), ".CREST");

                                    if (!path.mkdirs()) {
                                        path.mkdir();
                                    }

                                    File file = new File(path, "." + school_code + ".png");
                                    school.setCrestPath(file.getPath());
                                    Log.d("PATH", "PATH: "+file.getPath());
                                    Log.d("PATH", "PATH: "+file.getAbsolutePath());
                                    mSchoolViewModel.updateSchool(school);
                                    String schoolJson  = new Gson().toJson(school);
                                    Glide.with(getApplicationContext()).asBitmap().load("file://"+file.getPath()).override(100,100).into(imageViewSchoolCrest);

                                    PreferenceHelper.getPreferenceHelperInstace().setString(StudentInfoPhotoGenerateCardActivity.this,"selectedSchool", schoolJson);
                                    PreferenceHelper.getPreferenceHelperInstace().setString(StudentInfoPhotoGenerateCardActivity.this,"CREST", file.getPath());
                                    try {
                                        FileOutputStream outStream = new FileOutputStream(file);
                                        outStream.write(responseBodyResponse.body().bytes());
                                        outStream.flush();
                                        outStream.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }

                        else{


                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                });
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav);


        drawer = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.nav_view);

        viewPager = findViewById(R.id.view_pager_student_info_photo_gen);

        tvCurrentSchool = navigationView.findViewById(R.id.tv_current_school);

        imageViewSchoolCrest = findViewById(R.id.appCompatImageView);

        viewPager.setOnTouchListener((v, event) -> true);

        viewPager.setUserInputEnabled(false);

        viewPager.setOrientation(ViewPager2.ORIENTATION_VERTICAL);

        viewPager.setAdapter(getStudentInfoPhotoAdapter());

        viewPager.setOffscreenPageLimit(3);

        mSchoolSessionViewModel = new ViewModelProvider(this).get(SchoolSessionViewModel.class);

        mSessionLogViewModel  =  new ViewModelProvider(this).get(SessionLogViewModel.class);

        mStudentViewModel = new ViewModelProvider(this).get(StudentIDViewModel.class);

        mSchoolViewModel = new ViewModelProvider(this).get(SchoolViewModel.class);

       /* viewPager.setPageTransformer((page, position) -> {
            page.setAlpha(0f);
            page.setVisibility(View.VISIBLE);

            // Start Animation for a short period of time
            page.animate()
                    .alpha(1f)
                    .setDuration(page.getResources().getInteger(android.R.integer.config_shortAnimTime));
        });*/


        String crest = PreferenceHelper.getPreferenceHelperInstace().getString(getApplicationContext(),"CREST", "");
        Glide.with(this).asBitmap().load("file://"+crest).override(100,100).into(imageViewSchoolCrest);

        //Toast.makeText(this, "CREST: "+crest, Toast.LENGTH_SHORT).show();

        if(viewPager!=null){
            viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    super.onPageScrolled(position, positionOffset, positionOffsetPixels);
                }

                @Override
                public void onPageSelected(int position) {
                    //Toast.makeText(StudentInfoPhotoGenerateCardActivity.this, "current fragment: "+ position, Toast.LENGTH_SHORT).show();
                    super.onPageSelected(position);
                    PreferenceHelper.getPreferenceHelperInstace()
                            .setInteger(StudentInfoPhotoGenerateCardActivity.this, "current_fragment", position);


                    if(position==1) {
                        EventBus.getDefault().post(new SelectedFragment(position, true));
                        //Toast.makeText(StudentInfoPhotoGenerateCardActivity.this, "camerax", Toast.LENGTH_SHORT).show();
                    }
                    else if(position == 3){
                       // Toast.makeText(StudentInfoPhotoGenerateCardActivity.this, "review", Toast.LENGTH_SHORT).show();
                        EventBus.getDefault().post(new ReviewCardSelected(1,true));
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    super.onPageScrollStateChanged(state);
                }
            });
        }


        Gson gson = new Gson();




        String settingUpSchoolJson = PreferenceHelper.getPreferenceHelperInstace().getString(this, "setUpSchoolResponse",null );

        if(settingUpSchoolJson!=null){
            SetUpSchoolResponse settingUpSchool;
            settingUpSchool = gson.fromJson(settingUpSchoolJson,SetUpSchoolResponse.class);

            Log.d("settingUpSchool",""+settingUpSchool.toString());

        }
        String userString  = PreferenceHelper.getPreferenceHelperInstace().getString(this,"user", "");
        user = gson.fromJson(userString, LoginResponse.class);

        String projectOfficerJson =  PreferenceHelper.getPreferenceHelperInstace().getString(this, "projectOfficer", "");

        projectOfficer   = new Gson().fromJson(projectOfficerJson,  ProjectOfficer.class);

        String schoolJson  = PreferenceHelper.getPreferenceHelperInstace().getString(this,"selectedSchool", "");

        school = gson.fromJson(schoolJson,School.class);


        if(school!=null)
        mSchoolSessionViewModel.getSchoolSessionsBySchoolCode(school.getSchool_code()).observe(this, schoolSessions -> {
            String currentSchool = school.getSchool_name();
            tvCurrentSchool.setText(currentSchool);
            if(schoolSessions.size()>0){
                SchoolSession session = schoolSessions.get(0);
                sessionLog = new SessionLog(session.getCurrent_session());
                String currentSessionJson = new Gson().toJson(sessionLog);
                PreferenceHelper.getPreferenceHelperInstace().setString(getApplicationContext(),"currentSession",
                        currentSessionJson);
                if(sessionLog.sessionType.equalsIgnoreCase(SessionLog.PAUSED)) {
                    navigationView.findViewById(R.id.btn_pause_session).setVisibility(View.GONE);
                    navigationView.findViewById(R.id.btn_start_resume_session).setVisibility(View.VISIBLE);
                }
                else{
                    navigationView.findViewById(R.id.btn_pause_session).setVisibility(View.VISIBLE);
                    navigationView.findViewById(R.id.btn_start_resume_session).setVisibility(View.GONE);
                }
            }

        });
        navigationView.findViewById(R.id.btn_continue_capture).setOnClickListener(v -> {
            drawer.closeDrawer(GravityCompat.START);
            new Handler(Looper.getMainLooper()).postDelayed(() -> {
                viewPager.setCurrentItem(0,false);
                id = 0;
            },300);
        });

        navigationView.findViewById(R.id.btn_submit_session).setOnClickListener(v -> {

            startActivity(new Intent(this,UploadActivity.class));

        });
        navigationView.findViewById(R.id.btn_review_cards).setOnClickListener(v -> {
            id = 1;
            new Handler(Looper.getMainLooper()).postDelayed(()->drawer.closeDrawer(GravityCompat.START),300);
            EventBus.getDefault().post(new ReviewCardSelected(1,true));
            viewPager.setCurrentItem(3,false);
            PreferenceHelper.getPreferenceHelperInstace()
                    .setInteger(StudentInfoPhotoGenerateCardActivity.this, "current_fragment", 3);
        });

        navigationView.findViewById(R.id.btn_start_resume_session).setOnClickListener(v -> {
            new Handler(Looper.getMainLooper()).postDelayed(()->drawer.closeDrawer(GravityCompat.START),300);
            id = 1;
            PreferenceHelper.getPreferenceHelperInstace().setInteger(StudentInfoPhotoGenerateCardActivity.this, "current_fragment", 0);
            if(school!=null) {
                SessionLogObject sessionLogObject = new SessionLogObject();
                sessionLogObject.setSession_date(new Date().toString());
                sessionLogObject.setSchool_code(school.getSchool_code());
                sessionLogObject.setId(UUID.randomUUID().toString());
                sessionLogObject.setProject_officer(projectOfficer.getProject_officer_id());
                sessionLogObject.setProject_id(projectOfficer.getProject_id());
                sessionLogObject.setSchool_name(school.getSchool_name());
                sessionLogObject.setSession_log(new SessionLog(SessionLog.STARTED).sessionType);
                mSessionLogViewModel.insert(sessionLogObject);

                mSchoolSessionViewModel.updateSchoolSession(new SessionLog(SessionLog.STARTED).sessionType, new Date().toString(), school.getSchool_code(), school.getSchool_name());
            }

            viewPager.setCurrentItem(0,false);

          /*  Server.updateTaskStatus(projectOfficer.id(), school.getSchool_code(), "started")
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe(new Observer<Response<UpdateTaskStatusMutation.Data>>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {

                }

                @Override
                public void onNext(@NonNull Response<UpdateTaskStatusMutation.Data> dataResponse) {

                    UpdateTaskStatusMutation.Data responseData = dataResponse.getData();
                    if(responseData!=null){
                        Log.d(TAG,""+responseData.sid().updateTaskStatus().toString());
                    }
                }

                @Override
                public void onError(@NonNull Throwable e) {

                }

                @Override
                public void onComplete() {

                }
            })
            ;
            //Toast.makeText(this, "review cards selected", Toast.LENGTH_SHORT).show();
            *//*
            *
            *  UpdateTaskStatusMutation.Data responseData = dataResponse.getData();
                    if(responseData!=null){

                    }
                    * */
        });

        navigationView.findViewById(R.id.btn_review_sessions).setOnClickListener(v -> {
            id = 2;
            new Handler(Looper.getMainLooper()).postDelayed(()->drawer.closeDrawer(GravityCompat.START),300);
            viewPager.setCurrentItem(4,false);
            PreferenceHelper.getPreferenceHelperInstace()
                    .setInteger(StudentInfoPhotoGenerateCardActivity.this, "current_fragment", 4);
            //Toast.makeText(this, "review session selected", Toast.LENGTH_SHORT).show();
        });


        navigationView.findViewById(R.id.btn_log_out).setOnClickListener(v -> {
            if(alertDialogLogout==null)
               createLogOutDialog();
            alertDialogLogout.show();
        });


        navigationView.findViewById(R.id.btn_pause_session).setOnClickListener(v -> {
            id = 3;


            if(school!=null) {
                SessionLogObject sessionLogObject = new SessionLogObject();
                sessionLogObject.setSession_date(new Date().toString());
                sessionLogObject.setSchool_code(school.getSchool_code());
                sessionLogObject.setProject_officer(projectOfficer.getProject_officer_id());
                sessionLogObject.setProject_id(projectOfficer.getProject_id());
                sessionLogObject.setId(UUID.randomUUID().toString());
                sessionLogObject.setSchool_name(school.getSchool_name());
                sessionLogObject.setSession_log(new SessionLog(SessionLog.PAUSED).sessionType);
                mSessionLogViewModel.insert(sessionLogObject);
                // Toast.makeText(this, "pause session selected", Toast.LENGTH_SHORT).show();

                mSchoolSessionViewModel.updateSchoolSession(new SessionLog(SessionLog.PAUSED).sessionType, new Date().toString(), school.getSchool_code(), school.getSchool_name());
            }

            new Handler(Looper.getMainLooper()).postDelayed(()->drawer.closeDrawer(GravityCompat.START),300);

        });

        navigationView.findViewById(R.id.btn_end_session).setOnClickListener(v -> {
            new Handler(Looper.getMainLooper()).postDelayed(()->drawer.closeDrawer(GravityCompat.START),300);

            if(alertDialogEndSession==null)
                createEndSessionDialog();
                alertDialogEndSession.show();
            id = 4;

          //  Toast.makeText(this, "end session selected", Toast.LENGTH_SHORT).show();
        });

        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            createExitDialog();
            createLogOutDialog();
            createEndSessionDialog();
        },300);

        if(school!=null){
            if(TextUtils.isEmpty(crest))
                downloadSchoolCrest(school.getSchool_code());
           /* if(TextUtils.isEmpty(school.getCrestPath()))
            downloadSchoolCrest(school.getSchool_code());*/
           // else Toast.makeText(this, ""+school.getCrestPath(), Toast.LENGTH_SHORT).show();
        }

    }




    private void createProgressDialog(){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this,
                R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.progress_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        MaterialTextView  textViewMessage = alert_view.findViewById(R.id.tv_message);
        if(textViewMessage!=null){
            String message = "Uploading students details ...";
            textViewMessage.setText(message);
        }
        alertDialogProgress = (androidx.appcompat.app.AlertDialog) builder.create();
        alertDialogProgress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialogProgress.getWindow()!=null)
            alertDialogProgress.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }

    public void hideDialogProgressDialog(){
        if(alertDialogProgress!=null){
            alertDialogProgress.hide();
        }
    }

    private void showDialogProgressDialog(){
        if(alertDialogProgress==null)
            createProgressDialog();

        alertDialogProgress.show();
    }


    private boolean internetConnectionAvailable(int timeOut) {
        InetAddress inetAddress = null;
        try {
            Future<InetAddress> future = newSingleThreadExecutor().submit(() -> {
                try {
                    return InetAddress.getByName("google.com");
                } catch (UnknownHostException e) {
                    return null;
                }
            });
            inetAddress = future.get(timeOut, TimeUnit.MILLISECONDS);
            future.cancel(true);
        }
        catch (InterruptedException | ExecutionException | TimeoutException e) {
        }
        return inetAddress!=null && !inetAddress.equals("");
    }

    public  boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }




    private void createInternetConnectionDialog(){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(StudentInfoPhotoGenerateCardActivity.this,R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.alert_dialog_warning, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);
        if(tv_message!=null){
            String message = "<b>Connect to active internet to proceed</b>";
            tv_message.setText(Html.fromHtml(message));
        }
        AppCompatTextView btn_no = alert_view.findViewById(R.id.btn_no);
        if(btn_no!=null){
            btn_no.setVisibility(View.GONE);
        }
        AppCompatTextView btn_yes = alert_view.findViewById(R.id.btn_yes);
        if(btn_yes!=null){
            btn_yes.setText("OKAY");
            btn_yes.setOnClickListener(v -> {
                alertDialogInternetConnectivity.dismiss();
            });
        }
        alertDialogInternetConnectivity = (AlertDialog) builder.create();
        alertDialogInternetConnectivity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialogInternetConnectivity.getWindow()!=null)
            alertDialogInternetConnectivity.getWindow()
                    .setBackgroundDrawableResource(android.R.color.transparent);

    }

    private void createEndSessionDialog(){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(StudentInfoPhotoGenerateCardActivity.this,R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.alert_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialogEndSession = (AlertDialog) builder.create();

        MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);

        String schoolName = "Are you sure you want to end session for <br> <b> <font color='black'>"+school.getSchool_name()+"</font> </b> ?";
        tv_message.setText(Html.fromHtml(schoolName));
        AppCompatTextView btnNo = (AppCompatTextView) alert_view.findViewById(R.id.btn_no);
        btnNo.setOnClickListener(v -> alertDialogEndSession.hide());

        AppCompatTextView btnYes = (AppCompatTextView) alert_view.findViewById(R.id.btn_yes);
        btnYes.setOnClickListener(v -> {


            mStudentViewModel.getAllUnSubmitted(UploadStatus.SENT)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe(new SingleObserver<List<StudentIDCard>>() {
                @Override
                public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                    //d.dispose();
                }

                @Override
                public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull List<StudentIDCard> studentIDCards) {
                    Log.d(TAG, "studentIDCards:"+studentIDCards.toString());
                  //  Toast.makeText(StudentInfoPhotoGenerateCardActivity.this, "studentIDCards: size -> "+studentIDCards.size(), Toast.LENGTH_SHORT).show();
                    String imei = PreferenceHelper.getPreferenceHelperInstace().getString(getApplicationContext(),"IMEI",UUID.randomUUID().toString());

                    if(studentIDCards.size()>0)
                    {
                        Toast.makeText(StudentInfoPhotoGenerateCardActivity.this, "Submit session before ending session", Toast.LENGTH_LONG).show();
                    }
                    else {

                        if(school!=null) {
                            SessionLogObject sessionLogObject = new SessionLogObject();
                            sessionLogObject.setSession_date(new Date().toString());
                            sessionLogObject.setSchool_code(school.getSchool_code());
                            sessionLogObject.setProject_officer(projectOfficer.getProject_officer_id());
                            sessionLogObject.setProject_id(projectOfficer.getProject_id());
                            sessionLogObject.setId(UUID.randomUUID().toString());
                            sessionLogObject.setSchool_name(school.getSchool_name());
                            sessionLogObject.setSession_log(new SessionLog(SessionLog.ENDED).sessionType);
                            mSessionLogViewModel.insert(sessionLogObject);
                            // Toast.makeText(this, "pause session selected", Toast.LENGTH_SHORT).show();

                            mSchoolSessionViewModel.updateSchoolSession(new SessionLog(SessionLog.ENDED).sessionType, new Date().toString(), school.getSchool_code(), school.getSchool_name());
                        }


                        //DialogUtil.showDialog(this, "Sorry something went wrong!!!");
                        PreferenceHelper.getPreferenceHelperInstace().setString(getApplicationContext(),"selectedSchool", null);
                        PreferenceHelper.getPreferenceHelperInstace().setBoolean(getApplicationContext(),"isSchoolSetUpAlready", false);
                        startActivity(new Intent(StudentInfoPhotoGenerateCardActivity.this, SchoolSetUpActivity.class));
                        finish();
                    }
                }

                @Override
                public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                    e.printStackTrace();
                    Log.d(TAG, "onError:"+e.getMessage());
                }
            });

        });

        alertDialogEndSession.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialogEndSession.getWindow()!=null)
            alertDialogEndSession.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

   private void createExitDialog(){
       MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(StudentInfoPhotoGenerateCardActivity.this,R.style.MaterialAlertDialog_rounded);
       View alert_view = getLayoutInflater().inflate(R.layout.alert_dialog, null);
       builder.setCancelable(false);
       builder.setView(alert_view);
       alertDialogExit = (AlertDialog) builder.create();

       MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);

       String exit = "<b>Are you sure you want to exit?</b>";
       tv_message.setText(Html.fromHtml(exit));
       AppCompatTextView btnNo = (AppCompatTextView) alert_view.findViewById(R.id.btn_no);
       btnNo.setOnClickListener(v -> alertDialogExit.hide());

       AppCompatTextView btnYes = (AppCompatTextView) alert_view.findViewById(R.id.btn_yes);
       btnYes.setOnClickListener(v -> {
           finish();
       });

       alertDialogExit.requestWindowFeature(Window.FEATURE_NO_TITLE);
       if(alertDialogExit.getWindow()!=null)
           alertDialogExit.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
   }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDrawerMenuClicked(DrawerSelectedEvent drawerSelectedEvent){
        drawer.openDrawer(GravityCompat.START, true);
        switch (drawerSelectedEvent.getId()){
            case 1:

                break;

            case 2:
                break;

            case 3:
                break;

            case 4:
                break;
        }

    }




    @Override
    public void onBackPressed() {

        id = PreferenceHelper.getPreferenceHelperInstace()
                .getInteger(StudentInfoPhotoGenerateCardActivity.this, "current_fragment", 0);
        if(id!=0){
            new Handler(Looper.getMainLooper()).postDelayed(() -> {
                viewPager.setCurrentItem(0,false);
                id = 0;
            },300);

        }
        else{
            if(alertDialogExit==null)
                createExitDialog();
            alertDialogExit.show();
           // DialogUtil.showDialog(this, "Sorry something went wrong!!!");
            //super.onBackPressed();
        }


    }

    @Override
    protected void onPause() {
        if(alertDialogLogout!=null){
            alertDialogLogout.cancel();
        }
        if(alertDialogExit!=null){
            alertDialogExit.cancel();
        }
        if(alertDialogEndSession!=null)
            alertDialogEndSession.cancel();

        if(alertDialogProgress!=null)
            alertDialogProgress.cancel();

        if(alertDialogInternetConnectivity!=null)
            alertDialogInternetConnectivity.cancel();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if(alertDialogLogout!=null){
            alertDialogLogout.cancel();
        }
        if(alertDialogExit!=null){
            alertDialogExit.cancel();
        }
        if(alertDialogEndSession!=null)
            alertDialogEndSession.cancel();

        if(alertDialogProgress!=null)
            alertDialogProgress.cancel();

        if(alertDialogInternetConnectivity!=null)
            alertDialogInternetConnectivity.cancel();

        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
     //   Toast.makeText(this, ""+event.toString(), Toast.LENGTH_SHORT).show();
        Log.d(TAG,"onMessageEvent :"+ event.toString());
        switch (event.getId()){

            case 0:
                if(sessionLog.sessionType.equalsIgnoreCase(SessionLog.PAUSED)){
                    Toast.makeText(this, "Session is paused", Toast.LENGTH_SHORT).show();
                }
                else {
                    viewPager.setCurrentItem(0, false);
                    EventBus.getDefault().post(new GeneratedSuccess(true));
                }
                break;
            case 1:
                if(sessionLog.sessionType.equalsIgnoreCase(SessionLog.PAUSED)){
                    Toast.makeText(this, "Session is paused", Toast.LENGTH_SHORT).show();
                }
                else {
                    viewPager.setCurrentItem(0, false);
                    EventBus.getDefault().post(new StudentInfoSelected(0));
                }
                break;

            case 2:
                if(sessionLog.sessionType.equalsIgnoreCase(SessionLog.PAUSED)){
                    Toast.makeText(this, "Session is paused", Toast.LENGTH_SHORT).show();
                }
                else {
                    viewPager.setCurrentItem(1, false);
                    EventBus.getDefault().post(new PhotoBioSelected(1));
                }
                break;

            case 3:
                viewPager.setCurrentItem(2,false);
                EventBus.getDefault().post(new GenerateCardSelected(2));
                break;

            case 4:
                viewPager.setCurrentItem(3,false);
                EventBus.getDefault().post(new ReviewCardSelected(1,true));
                break;

            case 5:
                viewPager.setCurrentItem(4,false);
                //EventBus.getDefault().post(new GenerateCardSelected(4));
                break;

            case 6:
                viewPager.setCurrentItem(5,false);
               // EventBus.getDefault().post(new GenerateCardSelected(5));
                break;
        }


    }


    private StudentInfoPhotoPagerAdapter getStudentInfoPhotoAdapter() {
        return new StudentInfoPhotoPagerAdapter(getSupportFragmentManager(), getLifecycle());

    }

    @Override
    public void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }



}