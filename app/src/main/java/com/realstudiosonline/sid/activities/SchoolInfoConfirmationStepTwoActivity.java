package com.realstudiosonline.sid.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.realstudiosonline.sid.R;



public class SchoolInfoConfirmationStepTwoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_info_confirmation_step_two);



        findViewById(R.id.btn_submit_confirmation).setOnClickListener(v -> {
            startActivity(new Intent(this, StudentInfoPhotoGenerateCardActivity.class));
            //startActivity(new Intent(this, CameraXFragment.class));
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        });
    }
}