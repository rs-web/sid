
package com.realstudiosonline.sid.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import android.content.Intent;
import android.os.Bundle;

import com.google.gson.Gson;
import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.models.LoginResponse;
import com.realstudiosonline.sid.utils.PreferenceHelper;

import java.util.Timer;
import java.util.TimerTask;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        AppCompatTextView tvWelcome = findViewById(R.id.welcome_text);


        Gson gson = new Gson();
        String name = "";

        String userString = PreferenceHelper.getPreferenceHelperInstace().getString(
                this,"user", null);

        if(userString!=null){
            LoginResponse user  = gson.fromJson(userString, LoginResponse.class);
            name = user.getFirstname() + " "+ user.getLastname();
        }


        String userWelcome = String.format(getResources().getString(R.string.welcome_name), name);

        tvWelcome.setText(userWelcome);


        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {

                startActivity(new Intent(WelcomeActivity.this, SchoolSetUpActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        }, 3000);
    }
}