package com.realstudiosonline.sid.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.lifecycle.ViewModelProvider;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.apollographql.apollo.api.Error;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.realstudiosonline.sid.R;

import com.realstudiosonline.sid.adapter.SchoolDropDownAdapter;
import com.realstudiosonline.sid.api.ApiClient;
import com.realstudiosonline.sid.api.ApiClientSchools;
import com.realstudiosonline.sid.graphqlserver.Server;
import com.realstudiosonline.sid.models.ProjectOfficer;
import com.realstudiosonline.sid.models.School;
import com.realstudiosonline.sid.models.SchoolSession;
import com.realstudiosonline.sid.models.SessionLog;
import com.realstudiosonline.sid.models.SessionLogObject;
import com.realstudiosonline.sid.models.SetUpSchoolResponse;
import com.realstudiosonline.sid.models.SetUpSchoolResponseData;
import com.realstudiosonline.sid.models.Student;
import com.realstudiosonline.sid.models.StudentInfo;
import com.realstudiosonline.sid.models.StudentsResponse;
import com.realstudiosonline.sid.room.SchoolSessionViewModel;
import com.realstudiosonline.sid.room.SchoolViewModel;
import com.realstudiosonline.sid.room.SessionLogViewModel;
import com.realstudiosonline.sid.room.StudentInfoViewModel;
import com.realstudiosonline.sid.utils.PreferenceHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.realstudiosonline.sid.activities.SchoolSetUpActivity.toBase64;


public class SchoolInfoConfirmationStepOneActivity extends AppCompatActivity implements OnMapReadyCallback {

    AutoCompleteTextView autoCompleteTextViewSchool;
    ArrayList<School> schools;
    School selectedSchool;
    SchoolDropDownAdapter schoolDropDownAdapter;
    TextInputLayout inputLayoutSchoolName;
    AppCompatTextView tvNameOfSchool, tvSchoolRegion, titleRegion, tvSchoolDistrict, titleDistrict, tvSchoolLocation, tvSchoolId;
    boolean isEditing = false;
    private School school;
    View view_district,view_region;
    private SchoolViewModel mSchoolViewModel;
    //private String pageNum ="1";
    private int total = 434601;
    private int currentPage  = 0;

    StudentInfoViewModel mStudentViewModel;

    ProjectOfficer projectOfficer;
    SessionLogViewModel mSessionLogViewModel;

    SchoolSessionViewModel mSchoolSessionViewModel;
    AlertDialog alertDialogExit,alertDialog, alertDialogSettingUpSchool, alertDialogProceed;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setup_school_step_one);


        school = (School) getIntent().getSerializableExtra("selectedSchool");

        inputLayoutSchoolName = findViewById(R.id.spinner_input_school_name);

        tvSchoolRegion = findViewById(R.id.tv_school_region);

        tvNameOfSchool = findViewById(R.id.tv_name_of_school);

        titleDistrict = findViewById(R.id.title_district);

        titleRegion = findViewById(R.id.title_region);

        tvSchoolDistrict = findViewById(R.id.tv_school_district);

        tvSchoolId = findViewById(R.id.tv_school_id);

        tvSchoolLocation = findViewById(R.id.tv_school_location);
        autoCompleteTextViewSchool =  findViewById(R.id.spinner_school_name);

        schools = new ArrayList<>();

        view_region = findViewById(R.id.view_region);

        view_district = findViewById(R.id.view_district);
       /* alertDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Please wait...")
                .build();*/

        PreferenceHelper.getPreferenceHelperInstace().setString(this,"CREST", "");

        createProgressDialog();
        mStudentViewModel = new ViewModelProvider(this).get(StudentInfoViewModel.class);

        schoolDropDownAdapter = new SchoolDropDownAdapter(SchoolInfoConfirmationStepOneActivity.this, schools);
        autoCompleteTextViewSchool.setTextColor(getResources().getColor(R.color.colorHintTextColor));
        autoCompleteTextViewSchool.setAdapter(schoolDropDownAdapter);
        autoCompleteTextViewSchool.setThreshold(1);


        mSchoolSessionViewModel = new ViewModelProvider(this).get(SchoolSessionViewModel.class);


        mSessionLogViewModel  =  new ViewModelProvider(this).get(SessionLogViewModel.class);


        // Get a new or existing ViewModel from the ViewModelProvider.
        mSchoolViewModel = new ViewModelProvider(this).get(SchoolViewModel.class);
        // Add an observer on the LiveData returned by getAllStudentIDs.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        mSchoolViewModel.getAllSchools().observe(this, schoolsData -> {
            // Update the cached copy of the words in the adapter.
            //adapter.submitList(words);
            schools.clear();
            schools.addAll(schoolsData);
            schoolDropDownAdapter.notifyDataSetChanged();

            for(int i=0; i < schools.size(); i++){
                if(schools.get(i).getSchool_id() == school.getSchool_id()){
                    autoCompleteTextViewSchool.setText(school.getSchool_name(),false);
                }
            }
            Log.d("SchoolSetUpActivity","schools from db:" +schools.toString());
        });

        AppCompatImageButton btn_edit_school = findViewById(R.id.btn_edit_school);

        btn_edit_school.setOnClickListener(v -> {

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    5.0f
            );
            inputLayoutSchoolName.setLayoutParams(param);
            btn_edit_school.setVisibility(View.GONE);

            if(isEditing){
                isEditing = false;
                inputLayoutSchoolName.setVisibility(View.GONE);
                tvNameOfSchool.setVisibility(View.VISIBLE);
                if(school!=null){
                    selectedSchool.setContact_person_number(school.getContact_person_number());
                    selectedSchool.setContact_person(school.getContact_person());
                    Gson gson = new Gson();
                    String schoolJson = gson.toJson(selectedSchool);
                    PreferenceHelper.getPreferenceHelperInstace().setString(
                            this,"selectedSchool", schoolJson);

                }


            }

            else {
                isEditing = true;
                inputLayoutSchoolName.setVisibility(View.VISIBLE);
                tvNameOfSchool.setVisibility(View.GONE);
            }



        });


        tvNameOfSchool.setText(school.getSchool_name());

        if(TextUtils.isEmpty(school.getDistrict())){
            view_district.setVisibility(View.GONE);
        }

        if(TextUtils.isEmpty(school.getRegion())){
            view_region.setVisibility(View.GONE);
        }
        tvSchoolDistrict.setText(school.getDistrict());
        tvSchoolRegion.setText(school.getRegion());
        tvSchoolLocation.setText(school.getLocation());
        tvSchoolId.setText(school.getSchool_code());

        autoCompleteTextViewSchool.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //schoolDropDownAdapter.notifyDataSetChanged();
            }
        });

        autoCompleteTextViewSchool.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
                autoCompleteTextViewSchool.showDropDown();
        });

        autoCompleteTextViewSchool.setOnTouchListener((v, event) -> {
            autoCompleteTextViewSchool.showDropDown();
            return false;
        });

        String base64Token = toBase64("2y10eZiFIBKBuS5SEtcgZ0YJAezQN63xcjOOm9IJBI8ploxw10Hf832");

        autoCompleteTextViewSchool.setOnItemClickListener((parent, view12, position, id) ->{
            selectedSchool = schoolDropDownAdapter.getItem(position);
            tvNameOfSchool.setText(selectedSchool.getSchool_name());
            tvSchoolDistrict.setText(selectedSchool.getDistrict());
            tvSchoolRegion.setText(selectedSchool.getRegion());
            tvSchoolLocation.setText(selectedSchool.getDistrict());
            tvSchoolId.setText(selectedSchool.getSchool_code());
            if(school!=null){
                selectedSchool.setContact_person_number(school.getContact_person_number());
                selectedSchool.setContact_person(school.getContact_person());
                Gson gson = new Gson();
                String schoolJson = gson.toJson(selectedSchool);
                PreferenceHelper.getPreferenceHelperInstace().setString(
                        this,"selectedSchool", schoolJson);
                PreferenceHelper.getPreferenceHelperInstace().getString(getApplicationContext(),"CREST", "");
                downloadSchoolCrest(school.getSchool_code());

            }
            //editTextSchoolPopulation.setText(" "+selectedSchool.getTotalEnrollment());
        } );

        String projectOfficerJson =  PreferenceHelper.getPreferenceHelperInstace().getString(this, "projectOfficer", "");

        projectOfficer   = new Gson().fromJson(projectOfficerJson,  ProjectOfficer.class);


        mSchoolSessionViewModel.getAllSchoolSessions().observe(this, schoolSessions -> {
            Log.d("mSchoolSessionViewModel", "schoolSessions: "+schoolSessions.toString());
        });

        mSessionLogViewModel.getAllSessions().observe(this, sessionLogObjects -> {
            Log.d("mSessionLogViewModel", "sessionLogObjects: "+sessionLogObjects.toString());
        });
        findViewById(R.id.btn_submit_verification).setOnClickListener(v -> {

            mStudentViewModel.getStudentsByID(school.getSchool_code())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<List<StudentInfo>>() {
                        @Override
                        public void onSubscribe(@NonNull Disposable d) {

                        }

                        @Override
                        public void onSuccess(@NonNull List<StudentInfo> studentInfos) {
                            if(studentInfos.size()>0){
                                if(projectOfficer!=null){
                                    Log.d("SchoolInfoConfirmation", " projectOfficer is not");
                                    processDataRes();

                                }
                                else {
                                    Log.d("SchoolInfoConfirmation", " projectOfficer is null");
                                    //DialogUtil.showDialog(this, "Sorry something went wrong!!!");
                                }
                            }
                            else {

                                Log.d("SchoolInfoConfirmation", " getSchools(base64Token, school.getSchool_code())");
                                getStudents(base64Token, school.getSchool_code());
                            }
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {

                        }
                    });


        });
    }


    private void downloadSchoolCrest(String school_code){

        ApiClientSchools.getApiClient().getInstance().downloadSchoolCrest(school_code.trim()+".png")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(
                new DisposableSingleObserver<retrofit2.Response<ResponseBody>>() {
                    @Override
                    public void onSuccess(@NonNull Response<ResponseBody> responseBodyResponse) {
                        hideDialogSettingUpSchool();
                        if(responseBodyResponse.isSuccessful()) {
                            if(TextUtils.isEmpty(school.getCrestPath())){
                                try {
                                    if (responseBodyResponse.body() != null) {
                                        File path = new File(getExternalFilesDir(
                                                Environment.DIRECTORY_PICTURES), ".CREST");

                                        if (!path.mkdirs()) {
                                            path.mkdir();
                                        }

                                        File file = new File(path, "." + school_code + ".png");
                                        school.setCrestPath(file.getPath());
                                      //  Toast.makeText(SchoolInfoConfirmationStepOneActivity.this, "", Toast.LENGTH_SHORT).show();
                                        Log.d("PATH", "PATH: "+file.getPath());
                                        Log.d("PATH", "PATH: "+file.getAbsolutePath());

                                        mSchoolViewModel.updateSchool(school);
                                        String schoolJson  = new Gson().toJson(school);
                                        PreferenceHelper.getPreferenceHelperInstace().setString(getApplicationContext(),"CREST", file.getPath());
                                        PreferenceHelper.getPreferenceHelperInstace().setString(getApplicationContext(),"selectedSchool", schoolJson);

                                        try {
                                            FileOutputStream outStream = new FileOutputStream(file);
                                            outStream.write(responseBodyResponse.body().bytes());
                                            outStream.flush();
                                            outStream.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }

                        else{


                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                });
    }

    private void processDataRes(){


     showDialogSettingUpSchool();


        if(projectOfficer!=null){

            ApiClient.getApiClient()
                    .getInstance()
                    .setUpSchool(school.getSchool_code(), projectOfficer.getProject_id(),
                            school.getContact_person(),school.getContact_person_number(),"started")
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe(
                            new DisposableSingleObserver<Response<SetUpSchoolResponseData>>() {
                @Override
                public void onSuccess(@NonNull Response<SetUpSchoolResponseData> setUpSchoolResponseDataResponse) {
                    Log.d("setUpSchool", setUpSchoolResponseDataResponse.toString());

                    mSchoolViewModel.getSchoolBySchoolCode(school.getSchool_code())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<School>() {
                        @Override
                        public void onSuccess(@NonNull School school) {

                            /*if(TextUtils.isEmpty(school.getCrestPath())){
                                downloadSchoolCrest(school.getSchool_code());
                            }*/
                            String crest = PreferenceHelper.getPreferenceHelperInstace().getString(getApplicationContext(),"CREST", "");

                            if(TextUtils.isEmpty(crest))
                            downloadSchoolCrest(school.getSchool_code());
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {

                        }
                    });
                    if(setUpSchoolResponseDataResponse.isSuccessful()){

                        if(setUpSchoolResponseDataResponse.body()!=null){
                            if(!setUpSchoolResponseDataResponse.body().getData().isSchoolSetupAlready()){
                                SchoolSession schoolSession = new SchoolSession();
                                schoolSession.setProject_officer(projectOfficer.getProject_officer_id());
                                schoolSession.setSchool_code(school.getSchool_code());
                                schoolSession.setSession_date(new Date().toString());
                                schoolSession.setSchool_name(school.getSchool_name());
                                schoolSession.setCurrent_session(new SessionLog(SessionLog.STARTED).sessionType);
                                schoolSession.setSchool_name(school.getSchool_name());
                                mSchoolSessionViewModel.insert(schoolSession);

                                SessionLogObject sessionLogObject = new SessionLogObject();
                                sessionLogObject.setProject_officer(projectOfficer.getProject_officer_id());
                                sessionLogObject.setId(UUID.randomUUID().toString());
                                sessionLogObject.setSchool_code(school.getSchool_code());
                                sessionLogObject.setSession_date(new Date().toString());
                                sessionLogObject.setProject_id(projectOfficer.getProject_id());
                                sessionLogObject.setSchool_name(school.getSchool_name());
                                sessionLogObject.setSession_log(new SessionLog(SessionLog.STARTED).sessionType);
                                mSessionLogViewModel.insert(sessionLogObject);
                                PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolInfoConfirmationStepOneActivity.this, "isSchoolSetUpAlready",true);
                                PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolInfoConfirmationStepOneActivity.this, "studentSetUp",true);


                                String settingUpSchoolJson =  new Gson().toJson(setUpSchoolResponseDataResponse.body().getData());
                                PreferenceHelper.getPreferenceHelperInstace().setString(SchoolInfoConfirmationStepOneActivity.this,
                                        "setUpSchoolResponse",settingUpSchoolJson );
                                alertDialog.cancel();
                                startActivity(new Intent(SchoolInfoConfirmationStepOneActivity.this, StudentInfoPhotoGenerateCardActivity.class));
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                finish();
                                //TODO SETUP SCHOOL
                            }
                            else {
                                showProceedAlertDialog("School already setup for\n"+ school.getSchool_name());
                                SchoolSession schoolSession = new SchoolSession();
                                schoolSession.setProject_officer(projectOfficer.getProject_officer_id());
                                schoolSession.setSchool_code(school.getSchool_code());
                                schoolSession.setSession_date(new Date().toString());
                                schoolSession.setSchool_name(school.getSchool_name());
                                schoolSession.setCurrent_session(new SessionLog(SessionLog.STARTED).sessionType);
                                schoolSession.setSchool_name(school.getSchool_name());
                                mSchoolSessionViewModel.insert(schoolSession);

                                SessionLogObject sessionLogObject = new SessionLogObject();
                                sessionLogObject.setProject_officer(projectOfficer.getProject_officer_id());
                                sessionLogObject.setId(UUID.randomUUID().toString());
                                sessionLogObject.setSchool_code(school.getSchool_code());
                                sessionLogObject.setSession_date(new Date().toString());
                                sessionLogObject.setProject_id(projectOfficer.getProject_id());
                                sessionLogObject.setSchool_name(school.getSchool_name());
                                sessionLogObject.setSession_log(new SessionLog(SessionLog.STARTED).sessionType);
                                mSessionLogViewModel.insert(sessionLogObject);
                                PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolInfoConfirmationStepOneActivity.this, "isSchoolSetUpAlready",true);
                                PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolInfoConfirmationStepOneActivity.this, "studentSetUp",true);
                                alertDialog.cancel();
                                //TODO SETUP SCHOOL

                                Log.d("SchoolInfoConfirmation", " sid is null");
                            }

                        }

                        else {

                        }
                    }

                    else {
                        showProceedAlertDialog("School already setup for\n"+ school.getSchool_name());
                        SchoolSession schoolSession = new SchoolSession();
                        schoolSession.setProject_officer(projectOfficer.getProject_officer_id());
                        schoolSession.setSchool_code(school.getSchool_code());
                        schoolSession.setSession_date(new Date().toString());
                        schoolSession.setSchool_name(school.getSchool_name());
                        schoolSession.setCurrent_session(new SessionLog(SessionLog.STARTED).sessionType);
                        schoolSession.setSchool_name(school.getSchool_name());
                        mSchoolSessionViewModel.insert(schoolSession);

                        SessionLogObject sessionLogObject = new SessionLogObject();
                        sessionLogObject.setProject_officer(projectOfficer.getProject_officer_id());
                        sessionLogObject.setId(UUID.randomUUID().toString());
                        sessionLogObject.setSchool_code(school.getSchool_code());
                        sessionLogObject.setSession_date(new Date().toString());
                        sessionLogObject.setProject_id(projectOfficer.getProject_id());
                        sessionLogObject.setSchool_name(school.getSchool_name());
                        sessionLogObject.setSession_log(new SessionLog(SessionLog.STARTED).sessionType);
                        mSessionLogViewModel.insert(sessionLogObject);
                        PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolInfoConfirmationStepOneActivity.this, "isSchoolSetUpAlready",true);
                        PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolInfoConfirmationStepOneActivity.this, "studentSetUp",true);
                        alertDialog.cancel();
                        //TODO SETUP SCHOOL

                        Log.d("SchoolInfoConfirmation", " sid is null");
                    }
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    hideDialogSettingUpSchool();
                    PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolInfoConfirmationStepOneActivity.this, "isSchoolSetUpAlready",false);
                    PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolInfoConfirmationStepOneActivity.this, "studentSetUp",false);

                }
            });
           /* Server.setUpSchool(school.getSchool_code(), projectOfficer.projectId(),school.getContact_person(),school.getContact_person_number(),"started")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<SetUpSchoolMutation.Data>>() {
                        @Override
                        public void onSubscribe(@NonNull Disposable d) {

                        }

                        @Override
                        public void onNext(@NonNull Response<SetUpSchoolMutation.Data> dataResponse) {

                            hideDialogSettingUpSchool();
                            SetUpSchoolMutation.Data data = dataResponse.getData();

                            if(data!=null) {
                                SetUpSchoolMutation.Sid sid = data.sid();




                                if(sid!=null){
                                    SchoolSession schoolSession = new SchoolSession();
                                    schoolSession.setProject_officer(projectOfficer.id());
                                    schoolSession.setSchool_code(school.getSchool_code());
                                    schoolSession.setSession_date(new Date().toString());
                                    schoolSession.setSchool_name(school.getSchool_name());
                                    schoolSession.setCurrent_session(new SessionLog(SessionLog.STARTED).sessionType);
                                    schoolSession.setSchool_name(school.getSchool_name());
                                    mSchoolSessionViewModel.insert(schoolSession);

                                    SessionLogObject sessionLogObject = new SessionLogObject();
                                    sessionLogObject.setProject_officer(projectOfficer.id());
                                    sessionLogObject.setId(UUID.randomUUID().toString());
                                    sessionLogObject.setSchool_code(school.getSchool_code());
                                    sessionLogObject.setSession_date(new Date().toString());
                                    sessionLogObject.setProject_id(projectOfficer.projectId());
                                    sessionLogObject.setSchool_name(school.getSchool_name());
                                    sessionLogObject.setSession_log(new SessionLog(SessionLog.STARTED).sessionType);
                                    mSessionLogViewModel.insert(sessionLogObject);
                                    PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolInfoConfirmationStepOneActivity.this, "isSchoolSetUpAlready",true);
                                    PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolInfoConfirmationStepOneActivity.this, "studentSetUp",true);

                                    SetUpSchoolMutation.SettingUpSchool settingUpSchool = sid.settingUpSchool();
                                    String settingUpSchoolJson =  new Gson().toJson(settingUpSchool);
                                    PreferenceHelper.getPreferenceHelperInstace().setString(SchoolInfoConfirmationStepOneActivity.this,
                                            "setUpSchoolResponse",settingUpSchoolJson );
                                    alertDialog.cancel();
                                    //TODO SETUP SCHOOL
                                    startActivity(new Intent(SchoolInfoConfirmationStepOneActivity.this, StudentInfoPhotoGenerateCardActivity.class));
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    finish();
                                }

                                else {
                                    String errorMessage=null;
                                    if(dataResponse.component3()!=null){
                                        if(dataResponse.component3().size()>0){

                                            Error error = dataResponse.component3().get(0);
                                            Log.d("SID IS ERROR ", "ERROR"+ error.getMessage());
                                            String errorArray[] = error.getMessage().split(":");
                                            errorMessage = errorArray[1];
                                        }

                                    }
                                    if(errorMessage!=null)
                                    showProceedAlertDialog(errorMessage);
                                    SchoolSession schoolSession = new SchoolSession();
                                    schoolSession.setProject_officer(projectOfficer.id());
                                    schoolSession.setSchool_code(school.getSchool_code());
                                    schoolSession.setSession_date(new Date().toString());
                                    schoolSession.setSchool_name(school.getSchool_name());
                                    schoolSession.setCurrent_session(new SessionLog(SessionLog.STARTED).sessionType);
                                    schoolSession.setSchool_name(school.getSchool_name());
                                    mSchoolSessionViewModel.insert(schoolSession);

                                    SessionLogObject sessionLogObject = new SessionLogObject();
                                    sessionLogObject.setProject_officer(projectOfficer.id());
                                    sessionLogObject.setId(UUID.randomUUID().toString());
                                    sessionLogObject.setSchool_code(school.getSchool_code());
                                    sessionLogObject.setSession_date(new Date().toString());
                                    sessionLogObject.setProject_id(projectOfficer.projectId());
                                    sessionLogObject.setSchool_name(school.getSchool_name());
                                    sessionLogObject.setSession_log(new SessionLog(SessionLog.STARTED).sessionType);
                                    mSessionLogViewModel.insert(sessionLogObject);
                                    PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolInfoConfirmationStepOneActivity.this, "isSchoolSetUpAlready",true);
                                    PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolInfoConfirmationStepOneActivity.this, "studentSetUp",true);
                                    alertDialog.cancel();
                                    //TODO SETUP SCHOOL

                                    Log.d("SchoolInfoConfirmation", " sid is null");
                                }
                            }
                            else {
                                Log.d("SchoolInfoConfirmation", " data is null");
                            }

                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            hideDialogSettingUpSchool();
                            PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolInfoConfirmationStepOneActivity.this, "isSchoolSetUpAlready",false);
                            PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolInfoConfirmationStepOneActivity.this, "studentSetUp",false);

                        }

                        @Override
                        public void onComplete() {

                        }
                    });*/
        }
        else {
            Log.d("SchoolInfoConfirmation", " projectOfficer is null");
        }


    }


    private void getStudents(String base64Token, String schoolCode){
       showDialogProgressDialog();
        ApiClientSchools.getApiClient().getInstance()
                .getStudents(" Bearer " + base64Token, schoolCode)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new SingleObserver<StudentsResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onSuccess(@NonNull StudentsResponse studentsResponse) {

                hideDialogProgressDialog();
                Log.d("StudentsResponseDATAIS", studentsResponse.toString());
                //pageNum =String.valueOf(studentsResponse.getCurrent_page()+1);
                //total = studentsResponse.getTotal();

           /*     for (Student student:studentsResponse.getData()) {
                    Log.d("StudentDataIS", student.toString());
                    StudentInfo studentInfo = student.getInfo();
                    studentInfo.setId(student.getId());
                    mStudentViewModel.insert(studentInfo);
                }
*/


                EventBus.getDefault().post(studentsResponse);

                processDataRes();
        //startActivity(new Intent(this, CameraXFragment.class));


           //     getSchools(base64Token, schoolCode);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                hideDialogProgressDialog();
            }
        });
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void StudentDone(StudentsResponse studentsResponse){
        Log.d("SchoolInfoConfirmation", " pStudentDone");
        PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolInfoConfirmationStepOneActivity.this, "isSchoolSetUpAlready",true);
        PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolInfoConfirmationStepOneActivity.this, "studentSetUp",true);

        new Thread(() -> {
            for (Student student:studentsResponse.getData()) {
                Log.d("StudentDataIS", student.toString() + "\t"+ "ID: "+student.getId());
                StudentInfo studentInfo = student.getInfo();
                studentInfo.setId(student.getId());
                mStudentViewModel.insert(studentInfo);
            }

            // do onPostExecute stuff
            // do your stuff
            runOnUiThread(() -> {
               hideDialogProgressDialog();
                Log.d("SchoolInfoConfirmation", " runOnUiThread processDataRes");

            });
        }).start();

    }


    private void showDialogProgressDialog(){
        if(alertDialog==null)
            createProgressDialog();

        alertDialog.show();
    }
    private void createProgressDialog(){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(SchoolInfoConfirmationStepOneActivity.this,
                R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.progress_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialog = (androidx.appcompat.app.AlertDialog) builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialog.getWindow()!=null)
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }


    public void hideDialogProgressDialog(){
        if(alertDialog!=null){
            alertDialog.hide();
        }
    }

    public void cancelDialogProgressDialog(){
        if(alertDialog!=null){
            alertDialog.cancel();
        }
    }












    private void showDialogSettingUpSchool(){
        if(alertDialogSettingUpSchool==null)
            createDialogSettingUpSchool();

        alertDialogSettingUpSchool.show();
    }
    private void createDialogSettingUpSchool(){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(SchoolInfoConfirmationStepOneActivity.this,
                R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.progress_dialog, null);
        MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);
        tv_message.setText(R.string.setting_up_school);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialogSettingUpSchool = (androidx.appcompat.app.AlertDialog) builder.create();
        alertDialogSettingUpSchool.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialogSettingUpSchool.getWindow()!=null)
            alertDialogSettingUpSchool.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }


    public void hideDialogSettingUpSchool(){
        if(alertDialogSettingUpSchool!=null){
            alertDialogSettingUpSchool.hide();
        }
    }

    public void cancelDialogSettingUpSchool(){
        if(alertDialogSettingUpSchool!=null){
            alertDialogSettingUpSchool.cancel();
        }
    }



    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onBackPressed() {

        if(alertDialogExit==null)
            createExitDialog();
        alertDialogExit.show();


    }

    @Override
    protected void onDestroy() {
        cancelDialogProgressDialog();
        cancelDialogSettingUpSchool();
        if(alertDialogExit!=null)
            alertDialogExit.cancel();
        if(alertDialogProceed!=null){
            alertDialogProceed.cancel();
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        if(alertDialogProceed!=null){
            alertDialogProceed.cancel();
        }
        if(alertDialogExit!=null)
            alertDialogExit.cancel();
        cancelDialogSettingUpSchool();
        cancelDialogProgressDialog();
        super.onPause();

    }

   void showProceedAlertDialog(String message){
       MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(SchoolInfoConfirmationStepOneActivity.this,
               R.style.MaterialAlertDialog_rounded);
       View alert_view = getLayoutInflater().inflate(R.layout.school_setup_dialog, null);
       MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);
       MaterialButton btnContinue  = alert_view.findViewById(R.id.btn_continue);
       btnContinue.setOnClickListener(v -> {

           startActivity(new Intent(SchoolInfoConfirmationStepOneActivity.this, StudentInfoPhotoGenerateCardActivity.class));
           overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
           finish();
       });
       tv_message.setText(message);
       builder.setCancelable(false);
       builder.setView(alert_view);
       alertDialogProceed = (androidx.appcompat.app.AlertDialog) builder.create();
       alertDialogProceed.requestWindowFeature(Window.FEATURE_NO_TITLE);
       if(alertDialogProceed.getWindow()!=null)
           alertDialogProceed.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

       alertDialogProceed.show();
   }
    private void createExitDialog(){

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this,R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.alert_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialogExit = (AlertDialog) builder.create();

        MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);

        tv_message.setText(R.string.exit_confirmation);
        AppCompatTextView btnNo = (AppCompatTextView) alert_view.findViewById(R.id.btn_no);
        btnNo.setOnClickListener(v -> alertDialogExit.hide());

        AppCompatTextView btnYes = (AppCompatTextView) alert_view.findViewById(R.id.btn_yes);
        btnYes.setOnClickListener(v -> {
            finish();
        });

        alertDialogExit.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialogExit.getWindow()!=null)
            alertDialogExit.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));

            if (!success) {
                Log.e("TAG", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("TAG", "Can't find style. Error: ", e);
        }
        // Position the map's camera near Sydney, Australia.
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(-34, 151)));
    }

}