package com.realstudiosonline.sid.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.api.ApiClient;
import com.realstudiosonline.sid.models.LoginResponse;
import com.realstudiosonline.sid.models.ProjectOfficer;
import com.realstudiosonline.sid.utils.PreferenceHelper;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Response;

import static java.util.concurrent.Executors.newSingleThreadExecutor;

public class ProjectOfficerSignInActivity extends AppCompatActivity {

    TextInputEditText textEmailPhone, textPassword;
    Dialog dialog;
    private static final String TAG = ProjectOfficerSignInActivity.class.getSimpleName();
    AlertDialog alertDialog, alertDialogExit,alertDialogInternetConnectivity;
    TextInputLayout input_mobile_no, input_pass_code;
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_officer_sign_in);


        view = findViewById(R.id.view_sign_in);

        textEmailPhone = findViewById(R.id.edt_mobile_no);

        input_pass_code = findViewById(R.id.input_pass_code);

        input_mobile_no = findViewById(R.id.input_mobile_no);

        createProgressDialog();

        createExitDialog();


        textPassword = findViewById(R.id.edt_pass_code);

        textPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                input_pass_code.setErrorEnabled(s.toString().trim().length() <= 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        textEmailPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                input_mobile_no.setErrorEnabled(s.toString().trim().length() <= 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        findViewById(R.id.btn_submit).setOnClickListener(v -> {

            if(textEmailPhone.getText()!=null && textPassword.getText()!=null){
                if(textEmailPhone.getText().toString().length() >0 && textPassword.getText().toString().length()>0){
                    if(isNetworkConnected()) {
                        if(internetConnectionAvailable())
                        login(textEmailPhone.getText().toString(), textPassword.getText().toString());

                        else {
                            if(alertDialogInternetConnectivity==null)
                                createInternetConnectionDialog();
                            alertDialogInternetConnectivity.show();
                        }
                    }
                    else {
                        if(alertDialogInternetConnectivity==null)
                            createInternetConnectionDialog();
                        alertDialogInternetConnectivity.show();
                    }
                }

                else {
                    if(textEmailPhone.getText().length()==0){
                     input_mobile_no.setError("Enter Mobile number");
                     input_mobile_no.setErrorEnabled(true);
                    }
                    else if(textPassword.getText().length()==0){
                        input_pass_code.setError("Enter Password");
                        input_pass_code.setErrorEnabled(true);
                    }
                }

            }
        });

    /*    findViewById(R.id.btn_submit).setOnClickListener(v -> {
            startActivity(new Intent(this, ProjectOfficerVerificationActivity.class));
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        });*/
    }

    @SuppressLint("SetTextI18n")
    private void createInternetConnectionDialog(){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(
                ProjectOfficerSignInActivity.this,R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.alert_dialog_warning, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);
        if(tv_message!=null){
            String message = "<b>Connect to active internet to proceed</b>";
            tv_message.setText(Html.fromHtml(message));
        }
        AppCompatTextView btn_no = alert_view.findViewById(R.id.btn_no);
        if(btn_no!=null){
            btn_no.setVisibility(View.GONE);
        }
        AppCompatTextView btn_yes = alert_view.findViewById(R.id.btn_yes);
        if(btn_yes!=null){
            btn_yes.setText("OKAY");
            btn_yes.setOnClickListener(v -> alertDialogInternetConnectivity.dismiss());
        }
        alertDialogInternetConnectivity = builder.create();
        alertDialogInternetConnectivity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialogInternetConnectivity.getWindow()!=null)
            alertDialogInternetConnectivity.getWindow()
                    .setBackgroundDrawableResource(android.R.color.transparent);

    }


    private void createExitDialog(){

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this,R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.alert_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialogExit = builder.create();

        MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);

        tv_message.setText(R.string.exit_confirmation);
        AppCompatTextView btnNo = alert_view.findViewById(R.id.btn_no);
        btnNo.setOnClickListener(v -> alertDialogExit.hide());

        AppCompatTextView btnYes = alert_view.findViewById(R.id.btn_yes);
        btnYes.setOnClickListener(v -> finish());

        alertDialogExit.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialogExit.getWindow()!=null)
            alertDialogExit.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }



    public void login(String emailPhone, String password){
        showDialogProgressDialog();
        ApiClient.getApiClient().getInstance().auth(emailPhone,password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Response<LoginResponse>>() {
                    @Override
                    public void onSuccess(@NonNull Response<LoginResponse> loginResponseResponse) {
                        hideDialogProgressDialog();
                        if(loginResponseResponse.isSuccessful()){
                            if(loginResponseResponse.body()!=null) {
                                Toast.makeText(ProjectOfficerSignInActivity.this, "Successfully logged in", Toast.LENGTH_LONG).show();
                                LoginResponse loginResponse = loginResponseResponse.body();
                                if (loginResponse.getToken().length() > 0) {
                                    String token = loginResponse.getToken();
                                    Gson gson = new Gson();
                                    String userString = gson.toJson(loginResponse);

                                    ProjectOfficer projectOfficer = new ProjectOfficer(loginResponse.getProject_id(), loginResponse.getProject_officer_id());

                                    String projectOfficerJson = gson.toJson(projectOfficer);

                                    PreferenceHelper.getPreferenceHelperInstace().setString(ProjectOfficerSignInActivity.this, "projectOfficer", projectOfficerJson);

                                    PreferenceHelper.getPreferenceHelperInstace().setString(
                                            ProjectOfficerSignInActivity.this, "user", userString);
                                    PreferenceHelper.getPreferenceHelperInstace().setString(ProjectOfficerSignInActivity.this, "token", token);


                                    PreferenceHelper.getPreferenceHelperInstace().setBoolean(ProjectOfficerSignInActivity.this, "isLoggedIn", true);

                                    Log.d(TAG, "LOGIN RESPONSE :" + loginResponse.toString());

                                    startActivity(new Intent(ProjectOfficerSignInActivity.this, WelcomeActivity.class));
                                /*startActivity(new Intent(ProjectOfficerSignInActivity.this,
                                        SchoolInfoConfirmationStepOneActivity.class));*/
                                    // overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    finish();

                                } else {

                                    Toast.makeText(ProjectOfficerSignInActivity.this, "" + loginResponseResponse.message(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                        else {
                            Toast.makeText(ProjectOfficerSignInActivity.this, ""+loginResponseResponse.message(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                });
    }

    public void showDialog(Activity activity){
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.lottie_loading);
        if(dialog.getWindow()!=null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        dialog.show();
    }


    @Override
    protected void onPause() {
        if(alertDialogExit!=null)
            alertDialogExit.cancel();
        if(alertDialogInternetConnectivity!=null)
            alertDialogInternetConnectivity.cancel();
        super.onPause();

    }



    @Override
    public void onBackPressed() {

        if(alertDialogExit==null)
            createExitDialog();
        alertDialogExit.show();


    }
    @Override
    protected void onDestroy() {
        cancelDialogProgressDialog();
        if(alertDialogExit!=null)
            alertDialogExit.cancel();
        if(alertDialogInternetConnectivity!=null)
            alertDialogInternetConnectivity.cancel();
        super.onDestroy();
    }

    public void hideDialogProgressDialog(){
        if(alertDialog!=null){
            alertDialog.hide();
        }
    }


    private void createProgressDialog(){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(ProjectOfficerSignInActivity.this,
                R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.progress_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialog.getWindow()!=null)
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }

    private void showDialogProgressDialog(){
        if(alertDialog==null)
            createProgressDialog();

        alertDialog.show();
    }

    public void cancelDialogProgressDialog(){
        if(alertDialog!=null){
            alertDialog.cancel();
        }
    }

  /*  public void cancelDialog(){
        if(dialog!=null){
            dialog.cancel();
        }
    }
*/

    private boolean internetConnectionAvailable() {
        InetAddress inetAddress = null;
        try {
            Future<InetAddress> future = newSingleThreadExecutor().submit(() -> {
                try {
                    return InetAddress.getByName("google.com");
                } catch (UnknownHostException e) {
                    return null;
                }
            });
            inetAddress = future.get(2000, TimeUnit.MILLISECONDS);
            future.cancel(true);
        } catch (InterruptedException | TimeoutException | ExecutionException e) {
            e.printStackTrace();
        }
        return inetAddress!=null && !inetAddress.toString().equals("");
    }



    public  boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

}