package com.realstudiosonline.sid.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import com.google.android.material.textfield.TextInputEditText;
import com.realstudiosonline.sid.R;

public class ProjectOfficerVerifyActivity extends AppCompatActivity {

    TextInputEditText edtOTPOne,edtOTPTwo,edtOTPThree,edtOTPFour,edtOTPFive;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_officer_verify);


        edtOTPOne = findViewById(R.id.edt_otp_one);
        edtOTPTwo = findViewById(R.id.edt_otp_two);
        edtOTPThree = findViewById(R.id.edt_otp_three);
        edtOTPFour = findViewById(R.id.edt_otp_four);
        edtOTPFive = findViewById(R.id.edt_otp_five);


        edtOTPOne.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().length() == 1){
                    edtOTPTwo.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtOTPTwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().length() == 1){
                    edtOTPThree.requestFocus();
                }
                else  if(s.toString().length() == 0){
                    edtOTPOne.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtOTPThree.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().length() == 1){
                    edtOTPFour.requestFocus();
                }

                else  if(s.toString().length() == 0){
                    edtOTPTwo.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtOTPFour.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().length() == 1){
                    edtOTPFive.requestFocus();
                }
                else  if(s.toString().length() == 0){
                    edtOTPThree.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtOTPFive.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().length() == 0){
                    edtOTPFour.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        findViewById(R.id.btn_submit_verification).setOnClickListener(v -> {
            startActivity(new Intent(this, WelcomeActivity.class));
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        });
    }
}