package com.realstudiosonline.sid.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.lifecycle.ViewModelProvider;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.adapter.SchoolDropDownAdapter;
import com.realstudiosonline.sid.api.ApiClient;
import com.realstudiosonline.sid.api.ApiClientSchools;
import com.realstudiosonline.sid.models.School;
import com.realstudiosonline.sid.models.SchoolDataResponse;
import com.realstudiosonline.sid.room.SchoolViewModel;
import com.realstudiosonline.sid.utils.KeyBoard;
import com.realstudiosonline.sid.utils.PreferenceHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.realstudiosonline.sid.fragments.CameraXFragment.biometricFingerPrint;

public class SchoolSetUpActivity extends AppCompatActivity {

    MaterialAutoCompleteTextView autoCompleteTextViewSchool, autoCompleteTextViewClassType;
    SchoolDropDownAdapter schoolDropDownAdapter;
    ArrayList<School> schools;
    School selectedSchool;
    private TextInputEditText editTextSchoolPopulation, editTextSchoolContactPersonName, editTextSchoolContactPersonNumber;
    TextInputLayout inputContactPersonName, inputSchoolContactNo, inputSpinnerSchoolName, inputSpinnerClassType,inputLayoutClassPopulation;
    private AppCompatTextView tvSubmit;
    private String mSchoolContactPersonName, mSchoolContactPersonNumber;
    private int selectedClassPopulation =0;
    private static final String TAG = SchoolSetUpActivity.class.getSimpleName();
    private SchoolViewModel mSchoolViewModel;
    private boolean isSchoolsLoaded = false;
    String selectedClass;
    AlertDialog alertDialog, alertDialogExit;

    private View view_school_set_up;

    private String classType[] = {
            "SHS 1",
            "SHS 2",
            "SHS 3"
    };



    public static String toBase64(String message) {
        byte[] data;
        try {
            data = message.getBytes("UTF-8");
            return Base64.encodeToString(data, Base64.NO_WRAP);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    @SuppressLint({"ClickableViewAccessibility", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_set_up);





        view_school_set_up = findViewById(R.id.view_school_set_up);

        inputContactPersonName = findViewById(R.id.input_contact_person_name);
        inputSchoolContactNo = findViewById(R.id.input_school_contact_no);
        inputSpinnerSchoolName = findViewById(R.id.spinner_input_school_name);
        inputSpinnerClassType = findViewById(R.id.spinner_input_class_type);
        inputLayoutClassPopulation = findViewById(R.id.input_student_population);

        createProgressDialog();
        /*alertDialog = new SpotsDialog.Builder()
                .setContext(SchoolSetUpActivity.this)
                .setMessage("Setting up Schools please wait")
                .build();*/

        isSchoolsLoaded = PreferenceHelper.getPreferenceHelperInstace().getBoolean(SchoolSetUpActivity.this,
                "isSchoolsLoaded",false);
        schools = new ArrayList<>();

        // Get a new or existing ViewModel from the ViewModelProvider.
        mSchoolViewModel = new ViewModelProvider(this).get(SchoolViewModel.class);

        // Add an observer on the LiveData returned by getAllStudentIDs.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        mSchoolViewModel.getAllSchools().observe(this, schoolsData -> {
            // Update the cached copy of the words in the adapter.
            //adapter.submitList(words);

            runOnUiThread(() -> {

                schools.clear();
                for (School school:schoolsData) {
                    if(school.getSchool_code().length()>0){
                        schools.add(school);
                    }
                }
                schoolDropDownAdapter.notifyDataSetChanged();
                //   autoCompleteTextViewSchool.g.invalidateViews();
            });

            Log.d("SchoolSetUpActivity","schools from db:" +schools.toString());
        });




        String base64Token = toBase64("2y10eZiFIBKBuS5SEtcgZ0YJAezQN63xcjOOm9IJBI8ploxw10Hf832");
        if(!isSchoolsLoaded){
            showDialogProgressDialog();
            ApiClientSchools.getApiClient().getInstance()
                    .getAllSchools(" Bearer " + base64Token)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe(new Observer<SchoolDataResponse>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {

                }

                @Override
                public void onNext(@NonNull SchoolDataResponse schoolDataResponse) {
                   hideDialogProgressDialog();
                    for(School school : schoolDataResponse.getData()){
                        mSchoolViewModel.insert(school);
                    }
                    PreferenceHelper.getPreferenceHelperInstace().setBoolean(SchoolSetUpActivity.this, "isSchoolsLoaded",true);
                    Log.d("SchoolSetUpActivity", "Schools data is: "+ schoolDataResponse.toString());

                }

                @Override
                public void onError(@NonNull Throwable e) {

                    hideDialogProgressDialog();
                }

                @Override
                public void onComplete() {

                }
            });
        }

        tvSubmit = findViewById(R.id.btn_submit_school_setup);

        editTextSchoolPopulation = findViewById(R.id.edt_student_population);

        editTextSchoolContactPersonName = findViewById(R.id.edt_school_contact_person_name);
        editTextSchoolContactPersonName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSchoolContactPersonName = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextSchoolContactPersonNumber = findViewById(R.id.edt_school_contact_no);
        editTextSchoolContactPersonNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSchoolContactPersonNumber = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        autoCompleteTextViewClassType = findViewById(R.id.my_spinner_class_type);

        autoCompleteTextViewClassType.setEnabled(false);

        autoCompleteTextViewClassType.setTextColor(getResources().getColor(R.color.colorHintTextColor));
        ArrayAdapter<String>  classTypeDropDownAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, classType);
        autoCompleteTextViewClassType.setAdapter(classTypeDropDownAdapter);
        autoCompleteTextViewClassType.setThreshold(1);

        autoCompleteTextViewClassType.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
                autoCompleteTextViewClassType.showDropDown();
        });

        autoCompleteTextViewClassType.setOnTouchListener((v, event) -> {
            autoCompleteTextViewClassType.showDropDown();
            return true;
        });

        autoCompleteTextViewClassType.setOnItemClickListener((parent, view12, position, id) -> {
            KeyBoard.hide(SchoolSetUpActivity.this, view_school_set_up);


            if(selectedSchool!=null) {
                inputSpinnerClassType.setErrorEnabled(false);
                switch (position){

                    case 0:
                        selectedClass = "1";
                        break;
                    case 1:
                        selectedClass = "2";
                        break;
                    case  2:
                        selectedClass = "3";
                        break;
                }

                if(selectedClass!=null){
                    if (selectedClass.equals("1")){
                        if(selectedSchool.getForm_one_count()==0){
                            selectedClassPopulation = 0;
                            inputLayoutClassPopulation.setVisibility(View.GONE);
                        }
                        else {
                            selectedClassPopulation = selectedSchool.getForm_one_count();
                            inputLayoutClassPopulation.setVisibility(View.VISIBLE);
                            editTextSchoolPopulation.setText("" + selectedSchool.getForm_one_count());
                        }
                    }

                    else if (selectedClass.equals("2")) {
                        if(selectedSchool.getForm_two_count()==0){
                            selectedClassPopulation = 0;
                            inputLayoutClassPopulation.setVisibility(View.GONE);
                        }
                        else {
                            selectedClassPopulation = selectedSchool.getForm_two_count();
                            inputLayoutClassPopulation.setVisibility(View.VISIBLE);
                            editTextSchoolPopulation.setText("" + selectedSchool.getForm_two_count());
                        }
                    }
                    else if (selectedClass.equals("3")) {
                        if(selectedSchool.getForm_three_count()==0){
                            selectedClassPopulation = 0;
                            inputLayoutClassPopulation.setVisibility(View.GONE);
                        }
                        else {
                            selectedClassPopulation = selectedSchool.getForm_three_count();
                            inputLayoutClassPopulation.setVisibility(View.VISIBLE);
                            editTextSchoolPopulation.setText("" + selectedSchool.getForm_three_count());
                        }
                    }
                }


            }
            else {
                autoCompleteTextViewClassType.clearListSelection();
                autoCompleteTextViewClassType.setText("",false);
                autoCompleteTextViewClassType.clearFocus();
                Toast.makeText(this, "select school to proceed", Toast.LENGTH_LONG).show();
            }
        });



        autoCompleteTextViewSchool = (MaterialAutoCompleteTextView)findViewById(R.id.spinner_school_name);

        autoCompleteTextViewSchool.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
               schoolDropDownAdapter.notifyDataSetChanged();
            }
        });



        autoCompleteTextViewSchool.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
                autoCompleteTextViewSchool.showDropDown();
        });

        autoCompleteTextViewSchool.setOnTouchListener((v, event) -> {
            autoCompleteTextViewSchool.showDropDown();
            return false;
        });

        schoolDropDownAdapter = new SchoolDropDownAdapter(SchoolSetUpActivity.this, schools);
        autoCompleteTextViewSchool.setTextColor(getResources().getColor(R.color.colorHintTextColor));
        autoCompleteTextViewSchool.setAdapter(schoolDropDownAdapter);

        autoCompleteTextViewSchool.setThreshold(1);

        autoCompleteTextViewSchool.setOnItemClickListener((parent, view12, position, id) ->{
            KeyBoard.hide(SchoolSetUpActivity.this, view_school_set_up);
            if(schoolDropDownAdapter.getItem(position)!=null){
                inputSpinnerSchoolName.setErrorEnabled(false);
                selectedSchool = schoolDropDownAdapter.getItem(position);
                /*if(selectedSchool.getForm_one_count()==0){
                    classType =  new String[] {
                        "SHS 2", "SHS 3"
                    };
                }

                if(selectedSchool.getForm_two_count()==0){
                    classType =  new String[] {
                            "SHS 1", "SHS 3"
                    };
                }
                if(selectedSchool.getForm_three_count()==0){
                    classType =  new String[] {
                            "SHS 1", "SHS 2"
                    };
                }*/
                Log.d(TAG, ""+  schoolDropDownAdapter.getItem(position).toString());
                Log.d(TAG, "position: "+position);
                autoCompleteTextViewClassType.clearListSelection();
                autoCompleteTextViewClassType.setEnabled(true);
            }
            else {
                autoCompleteTextViewClassType.clearListSelection();
                autoCompleteTextViewClassType.setEnabled(false);
            }
        } );


        tvSubmit.setOnClickListener(v -> {

            //String base64Token = toBase64("2y10eZiFIBKBuS5SEtcgZ0YJAezQN63xcjOOm9IJBI8ploxw10Hf832");
            //Toast.makeText(this, "base64 "+base64Token, Toast.LENGTH_SHORT).show();
            Log.d("SchoolSetUpActivity", "base64Token: "+base64Token);

            if(selectedSchool!=null){

                if(mSchoolContactPersonName==null){
                    inputContactPersonName.setError("Contact person's name cannot be empty!");
                    inputContactPersonName.setErrorEnabled(true);
                }
                else if(mSchoolContactPersonNumber == null){
                    inputSchoolContactNo.setError("Contact person's number cannot be empty!");
                    inputSchoolContactNo.setErrorEnabled(true);
                }

                else if(selectedClass == null){
                    inputSpinnerClassType.setErrorEnabled(true);
                    inputSpinnerClassType.setError("Select Class");
                }


                else {

                    if(selectedClassPopulation>0){
                        inputSpinnerClassType.setErrorEnabled(false);
                        selectedSchool.setContact_person(mSchoolContactPersonName);
                        selectedSchool.setContact_person_number(mSchoolContactPersonNumber);
                        selectedSchool.setClass_id(selectedClass);
                        Gson gson = new Gson();
                        String schoolJson = gson.toJson(selectedSchool);
                        PreferenceHelper.getPreferenceHelperInstace().setString(
                                this,"selectedSchool", schoolJson);

                        new Timer().schedule(new TimerTask() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(SchoolSetUpActivity.this, SchoolInfoConfirmationStepOneActivity.class);
                                intent.putExtra("selectedSchool", selectedSchool);
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                finish();
                            }
                        }, 300);
                    }
                    else {
                        inputSpinnerClassType.setError("Select a class with population");
                        inputSpinnerClassType.setErrorEnabled(true);
                    }


                }



            }

            else {
                inputSpinnerSchoolName.setErrorEnabled(true);
               // inputSpinnerSchoolName.setError("Select School to proceed");
            }
        });

        PreferenceHelper.getPreferenceHelperInstace().setString(this,"CREST", "");
    }


    private void showDialogProgressDialog(){
        if(alertDialog==null)
            createProgressDialog();

        alertDialog.show();
    }
    private void createProgressDialog(){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(SchoolSetUpActivity.this,
                R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.progress_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialog = (androidx.appcompat.app.AlertDialog) builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialog.getWindow()!=null)
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }

    public void hideDialogProgressDialog(){
        if(alertDialog!=null){
            alertDialog.hide();
        }
    }

    public void cancelDialogProgressDialog(){
        if(alertDialog!=null){
            alertDialog.cancel();
        }
    }

    @Override
    public void onBackPressed() {

        if(alertDialogExit==null)
            createExitDialog();
        alertDialogExit.show();
    }

    @Override
    protected void onDestroy() {
        cancelDialogProgressDialog();
        if(alertDialogExit!=null)
            alertDialogExit.cancel();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        if(alertDialogExit!=null)
            alertDialogExit.cancel();
        super.onPause();

    }

    private void createExitDialog(){

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this,R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.alert_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialogExit = (AlertDialog) builder.create();

        MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);

        tv_message.setText(R.string.exit_confirmation);
        AppCompatTextView btnNo = (AppCompatTextView) alert_view.findViewById(R.id.btn_no);
        btnNo.setOnClickListener(v -> alertDialogExit.hide());

        AppCompatTextView btnYes = (AppCompatTextView) alert_view.findViewById(R.id.btn_yes);
        btnYes.setOnClickListener(v -> {
            finish();
        });

        alertDialogExit.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialogExit.getWindow()!=null)
            alertDialogExit.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }

}