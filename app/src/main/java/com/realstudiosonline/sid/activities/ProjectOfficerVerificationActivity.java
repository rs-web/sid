package com.realstudiosonline.sid.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.realstudiosonline.sid.R;

public class ProjectOfficerVerificationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_officer_verification);

        findViewById(R.id.btn_verify).setOnClickListener(v -> {
            startActivity(new Intent(this, ProjectOfficerVerifyActivity.class));
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        });
    }
}