package com.realstudiosonline.sid.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.realstudiosonline.sid.R;

import java.io.File;

public class AddProjectActivity extends AppCompatActivity {

    TextInputLayout input_name, input_initials, input_year,spinner_input_closure,input_card_title,
            input_start_date,input_end_date,input_card_size;

    TextInputEditText edt_name,edt_initials,edt_year, edt_card_title,edt_start_date,edt_end_date,edt_card_size;

    AutoCompleteTextView spinner_project_closure;
    AlertDialog alertDialog;

    AppCompatImageView select_image;
    File file;
    String filePath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_project);

        spinner_input_closure = findViewById(R.id.spinner_input_closure);

        input_name = findViewById(R.id.input_name);
        input_initials = findViewById(R.id.input_initials);
        input_year = findViewById(R.id.input_year);
        input_card_title = findViewById(R.id.input_card_title);
        input_start_date = findViewById(R.id.input_start_date);
        input_end_date = findViewById(R.id.input_end_date);
        input_card_size = findViewById(R.id.input_card_size);

        edt_name = findViewById(R.id.edt_name);
        edt_initials = findViewById(R.id.edt_initials);
        edt_year = findViewById(R.id.edt_year);
        edt_card_title = findViewById(R.id.edt_card_title);
        edt_start_date = findViewById(R.id.edt_start_date);
        edt_end_date = findViewById(R.id.edt_end_date);
        edt_card_size = findViewById(R.id.edt_card_size);

/*

        alertDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Please wait...")
                .build();
*/



        select_image = findViewById(R.id.select_image);

        findViewById(R.id.select_image).setOnClickListener(v -> {
            ImagePicker.Companion.with(this)
                    .crop()	    			//Crop image(Optional), Check Customization for more option
                    .compress(1024)			//Final image size will be less than 1 MB(Optional)
                    .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
                    .start();
        });
        findViewById(R.id.btn_submit).setOnClickListener(v -> {
           try {
               String name =   edt_name.getText().toString();
               String initials =   edt_initials.getText().toString();
               String year =   edt_year.getText().toString();
               boolean closure = false;
               String cardTitle =   edt_card_title.getText().toString();
               String startDate =   edt_start_date.getText().toString();
               String endDate =   edt_end_date.getText().toString();
               String cardSize =   edt_card_size.getText().toString();
               String  cardBgImg =  "";
               String BgImg =  filePath;

              // AddProject(name, initials, year, closure, cardTitle, startDate, endDate, cardSize,cardBgImg, BgImg);
           }
           catch (NullPointerException e){
               e.printStackTrace();
           }
        });


    }


/*
    private void AddProject(String name,
                            String initials,
                            String year,
                            boolean closure,
                            String cardTitle,
                            String startDate,
                            String endDate,
                            String cardSize,
                            String  cardBgImg,
                            String BgImg){

        Server.addProject(name,initials,year,closure,cardTitle,startDate,endDate,cardSize,cardBgImg, BgImg)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribeWith(new Observer<Response<AddProjectMutation.Data>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull Response<AddProjectMutation.Data> dataResponse) {

                Log.d("AddProjectActivity", dataResponse.toString());
                if(dataResponse.getData()!=null){
                    AddProjectMutation.Sid sid = dataResponse.getData().sid();

                    if(sid!=null){
                        Log.d("AddProjectActivity", sid.addProject().toString());

                    }
                }


            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            if(data!=null) {
                Uri fileUri = data.getData();
                select_image.setImageURI(fileUri);

                file = ImagePicker.Companion.getFile(data);

                //You can also get File Path from intent
                filePath = ImagePicker.Companion.getFilePath(data);
                Log.d("AddProjectActivity","filePath is "+filePath) ;
            }
        }
        else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.Companion.getError(data), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
        }
    }
}