package com.realstudiosonline.sid.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.realstudiosonline.sid.R;
import java.util.Timer;
import java.util.TimerTask;

public class SplashScreenActivity extends AppCompatActivity {

    AppCompatImageView img_splash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        img_splash = findViewById(R.id.img_splash);
        Glide.with(this).load(R.drawable.splash).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).into(img_splash);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreenActivity.this, OnboardingActivity.class));
                // overridePendingTransition(0, 0);
                finish();
            }
        }, 3000);
    }

}
