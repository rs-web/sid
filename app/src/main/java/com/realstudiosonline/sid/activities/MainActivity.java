package com.realstudiosonline.sid.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import com.realstudiosonline.sid.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tvOnBoarding = findViewById(R.id.onboading_id);



        startActivity(new Intent(this, OnboardingActivity.class));
        //startActivity(new Intent(this, StudentInfoPhotoGenerateCardActivity.class));
        //startActivity(new Intent(this, CardActivity.class));
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();


        tvOnBoarding.setOnClickListener(v -> {

            startActivity(new Intent(this, OnboardingActivity.class));
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        });
    }
}