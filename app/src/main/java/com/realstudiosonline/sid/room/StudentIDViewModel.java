package com.realstudiosonline.sid.room;

/*
 * Copyright (C) 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.realstudiosonline.sid.models.StudentIDCard;

import java.util.List;

import io.reactivex.rxjava3.core.Single;

/**
 * View Model to keep a reference to the word repository and
 * an up-to-date list of all words.
 */

public class StudentIDViewModel extends AndroidViewModel {

    private StudentIDRepository mRepository;

    private final LiveData<List<StudentIDCard>> mAllStudentIDs;

    public StudentIDViewModel(Application application) {
        super(application);
        mRepository = new StudentIDRepository(application);
        mAllStudentIDs = mRepository.getAllStudentIDs();
    }

    public LiveData<List<StudentIDCard>> getAllStudentIDs() {
        return mAllStudentIDs;
    }

    public Single<List<StudentIDCard>> getAllStudentID() {
        return mRepository.getAllStudentID();
    }

    public Single<List<StudentIDCard>> getAllUnSubmitted(String status) {
        return mRepository.getAllUnSubmitted(status);
    }

    public LiveData<List<StudentIDCard>> getAllStudentsIDBySchool(String schoolID) {
        return mRepository.getAllStudentsIDBySchool(schoolID);
    }

    public LiveData<List<StudentIDCard>> getAllStudentsIDBySchoolAndClass(String schoolID, String classID) {
        return mRepository.getAllStudentsIDBySchoolAndClass(schoolID,classID);
    }

    public void insert(StudentIDCard studentIDCard) {
        Log.d("StudentIDViewModel", "Insertion made for "+ studentIDCard.getFullName());
        mRepository.insert(studentIDCard);
    }

    public void updateStudent(String status, String studentID) {
        mRepository.updateStudent(status,studentID);
    }

    public Single<List<StudentIDCard>> getStudent(String schoolID, String id) {
       return mRepository.getStudent(schoolID,id);
    }
}
