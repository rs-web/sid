package com.realstudiosonline.sid.room;

/*
 * Copyright (C) 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.realstudiosonline.sid.models.School;
import com.realstudiosonline.sid.models.SchoolSession;
import com.realstudiosonline.sid.models.SessionLogObject;
import com.realstudiosonline.sid.models.StudentIDCard;
import com.realstudiosonline.sid.models.StudentInfo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This is the backend. The database. This used to be done by the OpenHelper.
 * The fact that this has very few comments emphasizes its coolness.  In a real
 * app, consider exporting the schema to help you with migrations.
 */

@Database(entities = {StudentIDCard.class, School.class, StudentInfo.class, SchoolSession.class, SessionLogObject.class}, version = 1, exportSchema = false)
abstract class StudentIDRoomDatabase extends RoomDatabase {

    abstract StudentIDCardDao studentIDCardDao();
    abstract SchoolDao schoolDao();
    abstract StudentInfoDao studentDao();
    abstract SessionLogDao sessionLogDao();
    abstract SchoolSessionDao schoolSessionDao();

    // marking the instance as volatile to ensure atomic access to the variable
    private static volatile StudentIDRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static StudentIDRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (StudentIDRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            StudentIDRoomDatabase.class, "sid_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Override the onCreate method to populate the database.
     * For this sample, we clear the database every time it is created.
     */
    private static Callback sRoomDatabaseCallback = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

         /*   databaseWriteExecutor.execute(() -> {
                // Populate the database in the background.
                // If you want to start with more words, just add them.
                WordDao dao = INSTANCE.wordDao();
                dao.deleteAll();

                Word word = new Word("Hello");
                dao.insert(word);
                word = new Word("World");
                dao.insert(word);
            });*/
        }
    };
}
