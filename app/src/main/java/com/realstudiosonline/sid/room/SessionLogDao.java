package com.realstudiosonline.sid.room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.realstudiosonline.sid.models.SessionLogObject;

import java.util.List;

@Dao
public interface SessionLogDao {

    @Query("SELECT * FROM session_logs")
    LiveData<List<SessionLogObject>> getAllSessions();

    @Query("SELECT * FROM session_logs WHERE school_code =:schoolCode ORDER BY id ASC")
    LiveData<List<SessionLogObject>> getSessionBySchoolCode(String schoolCode);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(SessionLogObject sessionLogObject);

    @Query("DELETE FROM session_logs")
    void deleteAllSessions();

    @Query("DELETE FROM session_logs WHERE school_code =:schoolCode")
    void deleteByID(String schoolCode);

}
