package com.realstudiosonline.sid.room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.realstudiosonline.sid.models.SchoolSession;

import java.util.List;

@Dao
public interface SchoolSessionDao {

    @Query("SELECT * FROM school_session ORDER BY school_code ASC")
    LiveData<List<SchoolSession>> getAllSchoolSessions();

    @Query("SELECT * FROM school_session WHERE school_code =:schoolCode ORDER BY session_date ASC")
    LiveData<List<SchoolSession>> getSchoolSessionsBySchoolCode(String schoolCode);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(SchoolSession schoolSession);

    @Query("UPDATE school_session SET current_session = :sessionLog, updated_at= :updated_at, school_name=:schoolName WHERE school_code =:schoolCode")
    void updateSchoolSession(String sessionLog, String updated_at, String schoolCode, String schoolName);

    @Query("DELETE FROM school_session")
    void deleteAll();

    @Query("DELETE FROM school_session WHERE school_code =:schoolCode")
    void deleteSchoolByID(String schoolCode);
}
