package com.realstudiosonline.sid.room;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.realstudiosonline.sid.models.SessionLogObject;

import java.util.List;

class SessionLogRepository {

    private SessionLogDao mSchoolDao;
    private LiveData<List<SessionLogObject>> mAllSchools;

    SessionLogRepository(Application application) {
        StudentIDRoomDatabase db = StudentIDRoomDatabase.getDatabase(application);
        mSchoolDao = db.sessionLogDao();
        mAllSchools = mSchoolDao.getAllSessions();
    }


    LiveData<List<SessionLogObject>> getSessionBySchoolCode(String schoolID) {
        return mSchoolDao.getSessionBySchoolCode(schoolID);
    }

    LiveData<List<SessionLogObject>> getAllSessions() {
        return mAllSchools;
    }

    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
    // that you're not doing any long running operations on the main thread, blocking the UI.
    void insert(SessionLogObject sessionLog) {
        StudentIDRoomDatabase.databaseWriteExecutor.execute(() -> {
            mSchoolDao.insert(sessionLog);
        });
    }
}
