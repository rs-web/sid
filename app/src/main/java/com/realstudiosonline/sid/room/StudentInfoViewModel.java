package com.realstudiosonline.sid.room;

/*
 * Copyright (C) 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.realstudiosonline.sid.models.StudentInfo;

import java.util.List;

import io.reactivex.rxjava3.core.Single;


/**
 * View Model to keep a reference to the word repository and
 * an up-to-date list of all words.
 */

public class StudentInfoViewModel extends AndroidViewModel {

    private StudentInfoRepository mRepository;

    private final LiveData<List<StudentInfo>> mAllStudents;

    public StudentInfoViewModel(Application application) {
        super(application);
        mRepository = new StudentInfoRepository(application);
        mAllStudents = mRepository.getStudents();
    }

    public LiveData<List<StudentInfo>> getAllStudents() {
        return mAllStudents;
    }

    public LiveData<List<StudentInfo>> getStudentsBySchoolID(String schoolID) {
        return mRepository.getStudentsBySchoolID(schoolID);
    }
    public Single<List<StudentInfo>> getStudentsByID(String schoolID) {
        return mRepository.getStudentsByID(schoolID);
    }

    public Single<StudentInfo> getStudentByID(String schoolID, int studentId) {
        return mRepository.getStudentByID(schoolID,studentId);
    }


    public void insert(StudentInfo student) {

        //Log.d("StudentInfoInsert", student.getSchool_code());
        mRepository.insert(student);
    }

    public void updateStudent(String editedName, int id){
        mRepository.updateStudent(editedName, id);
    }
}
