package com.realstudiosonline.sid.room;

/*
 * Copyright (C) 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.realstudiosonline.sid.models.SessionLogObject;

import java.util.List;

/**
 * View Model to keep a reference to the word repository and
 * an up-to-date list of all words.
 */

public class SessionLogViewModel extends AndroidViewModel {

    private SessionLogRepository mRepository;

    private final LiveData<List<SessionLogObject>>  mAllSessionLogObjects;

    public SessionLogViewModel(Application application) {
        super(application);
        mRepository = new SessionLogRepository(application);
        mAllSessionLogObjects = mRepository.getAllSessions();
    }

    public LiveData<List<SessionLogObject>> getAllSessions() {
        return mAllSessionLogObjects;
    }

    public LiveData<List<SessionLogObject>> getSessionBySchoolCode(String schoolID) {
        return mRepository.getSessionBySchoolCode(schoolID);
    }


    public void insert(SessionLogObject sessionLog) {
        Log.d("StudentIDViewModel", "Insertion made for "+sessionLog.toString());
        mRepository.insert(sessionLog);
    }
}
