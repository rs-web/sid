package com.realstudiosonline.sid.room;

import android.content.Context;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import com.realstudiosonline.sid.models.StudentIDCard;

public class StudentIDListAdapter extends ListAdapter<StudentIDCard, StudentIDViewHolder> {

    private setOnCardSelectedListener listener;
    private Context context;
    public StudentIDListAdapter(@NonNull DiffUtil.ItemCallback<StudentIDCard> diffCallback,
                                @NonNull setOnCardSelectedListener listener, Context context) {
        super(diffCallback);
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public StudentIDViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return StudentIDViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(StudentIDViewHolder holder, int position) {
        StudentIDCard current = getItem(position);
        holder.bind(context,current, listener);

    }

    public static class StudentIDDiff extends DiffUtil.ItemCallback<StudentIDCard> {

        @Override
        public boolean areItemsTheSame(@NonNull StudentIDCard oldItem, @NonNull StudentIDCard newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull StudentIDCard oldItem, @NonNull StudentIDCard newItem) {
            return oldItem.getId().equals(newItem.getId());
        }
    }

   public interface setOnCardSelectedListener{
        void onCardSelected(StudentIDCard studentIDCard);
        void onCardLongPress(StudentIDCard studentIDCard);
    }
}
