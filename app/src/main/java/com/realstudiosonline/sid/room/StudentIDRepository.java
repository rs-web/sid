package com.realstudiosonline.sid.room;

import android.app.Application;
import androidx.lifecycle.LiveData;

import com.realstudiosonline.sid.models.StudentIDCard;

import java.util.List;

import io.reactivex.rxjava3.core.Single;

class StudentIDRepository {

    private StudentIDCardDao mStudentIDDao;
    private LiveData<List<StudentIDCard>> mAllStudentsID;

    StudentIDRepository(Application application) {
        StudentIDRoomDatabase db = StudentIDRoomDatabase.getDatabase(application);
        mStudentIDDao = db.studentIDCardDao();
        mAllStudentsID = mStudentIDDao.getAllStudentsID();
    }

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    LiveData<List<StudentIDCard>> getAllStudentsIDBySchool(String schoolID) {
        return mStudentIDDao.getAllStudentsIDBySchool(schoolID);
    }

    LiveData<List<StudentIDCard>> getAllStudentsIDBySchoolAndClass(String schoolID, String classID) {
        return mStudentIDDao.getAllStudentsIDBySchoolAndClass(schoolID,classID);
    }

    LiveData<List<StudentIDCard>> getAllStudentIDs() {
        return mAllStudentsID;
    }
    Single<List<StudentIDCard>> getAllStudentID() {
        return mStudentIDDao.getAllStudentID();
    }

    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
    // that you're not doing any long running operations on the main thread, blocking the UI.
    void insert(StudentIDCard studentIDCard) {
        StudentIDRoomDatabase.databaseWriteExecutor.execute(() -> {
            mStudentIDDao.insert(studentIDCard);
        });
    }

    public Single<List<StudentIDCard>> getAllUnSubmitted(String status) {
        return mStudentIDDao.getAllUnSubmitted(status);
    }

    public Single<List<StudentIDCard>> getStudent(String schoolID, String id) {
        return mStudentIDDao.getStudent(schoolID,id);
    }

    public void updateStudent(String status, String studentID) {
        StudentIDRoomDatabase.databaseWriteExecutor.execute(() -> {
            mStudentIDDao.updateStudent(status,studentID);
        });
    }

}
