package com.realstudiosonline.sid.room;

/*
 * Copyright (C) 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.realstudiosonline.sid.models.School;

import java.util.List;

import io.reactivex.rxjava3.core.Single;

/**
 * View Model to keep a reference to the word repository and
 * an up-to-date list of all words.
 */

public class SchoolViewModel extends AndroidViewModel {

    private SchoolRepository mRepository;

    private final LiveData<List<School>> mAllSchools;

    public SchoolViewModel(Application application) {
        super(application);
        mRepository = new SchoolRepository(application);
        mAllSchools = mRepository.getAllSchools();
    }

    public LiveData<List<School>> getAllSchools() {
        return mAllSchools;
    }

    public Single<School> getSchoolBySchoolCode(String schoolCode) {
        return mRepository.getSchoolBySchoolCode(schoolCode);
    }


    public void updateSchool(School school) {
        mRepository.updateSchool(school);
    }

    public void insert(School school) {
        mRepository.insert(school);
    }
}
