package com.realstudiosonline.sid.room;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.realstudiosonline.sid.models.SchoolSession;

import java.util.List;

class SchoolSessionRepository {

    private SchoolSessionDao mSchoolSessionDao;
    private LiveData<List<SchoolSession>> mAllSchoolSessions;

    SchoolSessionRepository(Application application) {
        StudentIDRoomDatabase db = StudentIDRoomDatabase.getDatabase(application);
        mSchoolSessionDao = db.schoolSessionDao();
        mAllSchoolSessions = mSchoolSessionDao.getAllSchoolSessions();
    }


    LiveData<List<SchoolSession>> getSchoolSessionsBySchoolCode(String schoolID) {
        return mSchoolSessionDao.getSchoolSessionsBySchoolCode(schoolID);
    }

    LiveData<List<SchoolSession>> getAllSchoolSessions() {
        return mAllSchoolSessions;
    }

    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
    // that you're not doing any long running operations on the main thread, blocking the UI.
    void insert(SchoolSession schoolSession) {
        StudentIDRoomDatabase.databaseWriteExecutor.execute(() -> {
            mSchoolSessionDao.insert(schoolSession);
        });
    }

    void  updateSchoolSession(String sessionLog, String updatedAt, String schoolCode, String schoolName){
        StudentIDRoomDatabase.databaseWriteExecutor.execute(() -> {
            mSchoolSessionDao.updateSchoolSession(sessionLog, updatedAt, schoolCode,schoolName);
        });

    }}
