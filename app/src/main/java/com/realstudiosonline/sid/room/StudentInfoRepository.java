package com.realstudiosonline.sid.room;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.realstudiosonline.sid.models.StudentInfo;

import java.util.List;

import io.reactivex.rxjava3.core.Single;


class StudentInfoRepository {

    private StudentInfoDao mStudentDao;
    private LiveData<List<StudentInfo>> mAllSchools;

    StudentInfoRepository(Application application) {
        StudentIDRoomDatabase db = StudentIDRoomDatabase.getDatabase(application);
        mStudentDao = db.studentDao();
        mAllSchools = mStudentDao.getStudentInfo();
    }


    LiveData<List<StudentInfo>> getStudentsBySchoolID(String schoolID) {
        return mStudentDao.getStudentsBySchoolID(schoolID);
    }

    Single<List<StudentInfo>> getStudentsByID(String schoolID) {
        return mStudentDao.getStudentsByID(schoolID);
    }

    Single<StudentInfo> getStudentByID(String schoolID, int studentID) {
        return mStudentDao.getStudentByID(schoolID,studentID);
    }

    LiveData<List<StudentInfo>> getStudents() {
        return mAllSchools;
    }

    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
    // that you're not doing any long running operations on the main thread, blocking the UI.
    void insert(StudentInfo school) {
        StudentIDRoomDatabase.databaseWriteExecutor.execute(() -> {
            mStudentDao.insert(school);
        });
    }

    public void updateStudent(String editedName, int id){
        StudentIDRoomDatabase.databaseWriteExecutor.execute(() -> {
            mStudentDao.updateStudent(editedName,id);
        });
    }
}
