package com.realstudiosonline.sid.room;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.realstudiosonline.sid.models.School;

import java.util.List;

import io.reactivex.rxjava3.core.Single;

class SchoolRepository {

    private SchoolDao mSchoolDao;
    private LiveData<List<School>> mAllSchools;

    SchoolRepository(Application application) {
        StudentIDRoomDatabase db = StudentIDRoomDatabase.getDatabase(application);
        mSchoolDao = db.schoolDao();
        mAllSchools = mSchoolDao.getAllSchools();
    }


    public Single<School> getSchoolBySchoolCode(String schoolID) {
        return mSchoolDao.getSchoolBySchoolCode(schoolID);
    }

    LiveData<List<School>> getAllSchools() {
        return mAllSchools;
    }

    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
    // that you're not doing any long running operations on the main thread, blocking the UI.
    void insert(School school) {
        StudentIDRoomDatabase.databaseWriteExecutor.execute(() -> {
            mSchoolDao.insert(school);
        });
    }

    void updateSchool(School school) {
        StudentIDRoomDatabase.databaseWriteExecutor.execute(() -> {
            mSchoolDao.updateSchool(school);
        });
    }
}
