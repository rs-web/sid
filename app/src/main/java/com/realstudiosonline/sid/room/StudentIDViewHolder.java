/*
 * Copyright (C) 2020 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.realstudiosonline.sid.room;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.facebook.shimmer.Shimmer;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.models.StudentIDCard;
import com.realstudiosonline.sid.models.UploadStatus;

class StudentIDViewHolder extends RecyclerView.ViewHolder {
    private final TextView tvIDNo;
    private static final String TAG = "StudentIDViewHolder";
    private final AppCompatImageView imgGeneratedCard;
    private ShimmerFrameLayout container;
    private AppCompatImageView imgStatus;

    private StudentIDViewHolder(View itemView) {
        super(itemView);
        tvIDNo = itemView.findViewById(R.id.tv_card_no);
        imgGeneratedCard =  itemView.findViewById(R.id.img_card);
        container = itemView.findViewById(R.id.shimmer_view_container);
        imgStatus = itemView.findViewById(R.id.img_status);
    }

    public void bind(Context context, StudentIDCard studentIDCard, StudentIDListAdapter.setOnCardSelectedListener listener) {
        String idN = (studentIDCard.getStudentID()==null ||studentIDCard.getStudentID().isEmpty()) ?"N/A": studentIDCard.getStudentID();
        String iDNo = "ID No. "+idN;
        tvIDNo.setText(iDNo);
        Log.d(TAG,"StudentIDCard is "+studentIDCard.toString());

        Glide.with(context)
                .load("file://"+studentIDCard.getGeneratedCardURI())
                .placeholder(R.drawable.student_id)
                .into(imgGeneratedCard);

        imgGeneratedCard.setOnClickListener(v -> listener.onCardSelected(studentIDCard));
        imgGeneratedCard.setOnLongClickListener(v ->{
            listener.onCardLongPress(studentIDCard);
            return true;
        });

        if(studentIDCard.getStatus().equals(UploadStatus.SENT)){
            container.hideShimmer();
            imgStatus.setImageTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorGreen)));
            imgStatus.setImageResource(R.drawable.ic_check_circle_24);
        }

        else if(studentIDCard.getStatus().equals(UploadStatus.READY)){
            container.hideShimmer();
            imgStatus.setImageTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorYellowLine)));
            imgStatus.setImageResource(R.drawable.ic_check_circle_24);
        }

         else if(studentIDCard.getStatus().equals(UploadStatus.SUBMITTING)) {
                container.startShimmer();
                container.showShimmer(true);
            imgStatus.setImageTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorYellowLine)));
            imgStatus.setImageResource(R.drawable.ic_check_circle_24);
            }

        else if(studentIDCard.getStatus().equals(UploadStatus.FAILED)){
    /*         imgStatus.setImageTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorPrimaryRed)));
             imgStatus.setImageResource(R.drawable.ic_cancel_24);*/
            imgStatus.setImageTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorGreen)));
            imgStatus.setImageResource(R.drawable.ic_check_circle_24);
            container.hideShimmer();
        }

    }

    static StudentIDViewHolder create(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_generated_card, parent, false);
        return new StudentIDViewHolder(view);
    }
}
