package com.realstudiosonline.sid.api;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static ApiClient apiClient;

    private ApiInterface apiCallback;
    private final int RESULT_OK = 200;

    public static ApiClient getApiClient() {
        if (apiClient == null) {
            apiClient = new ApiClient();
        }
        return apiClient;
    }

    /**
     * Static method to to get api client instance
     *
     * @return ApiInterface instance
     */
    public ApiInterface getInstance() {
        try {
            if (apiCallback == null) {

               // TokenInterceptor interceptor=new TokenInterceptor();

                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                //httpClient.addInterceptor(interceptor);
                httpClient.connectTimeout(1, TimeUnit.MINUTES);
                httpClient.readTimeout(1, TimeUnit.MINUTES);
                httpClient.writeTimeout(1, TimeUnit.MINUTES);
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
                httpClient.addInterceptor(logging);  // <-- this is the important line!

                Retrofit client = new Retrofit.Builder()
                        .baseUrl(ApiInterface.baseUrl)
                        .client(httpClient.build())
                        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                apiCallback = client.create(ApiInterface.class);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return apiCallback;
    }




   /* public static MultipartBody.Part toMultiPartFile(String name, byte[] byteArray) {
        RequestBody reqFile = RequestBody.create(MediaType.parse("video/mp4"), byteArray);

        return MultipartBody.Part.createFormData(name,
                null, // filename, this is optional
                reqFile);
    }*/

/*    public boolean checkForSuccess(final @NonNull BaseModel weatherData) {
        String code = weatherData.getCod();
        Log.i("onNext :", String.valueOf(code));
        if (Integer.parseInt(code) == RESULT_OK) {
            return true;
        }
//       else if (showError) {
//            if (weatherData.getMessage() != null)
//                Toast.makeText(context, weatherData.getMessage(), Toast.LENGTH_SHORT).show();
//        }
        return false;
    }*/

  /*  public void showCommonError(Context context) {
        Toast.makeText(context, R.string.str_something_went_wrong, Toast.LENGTH_SHORT).show();
    }*/

}
