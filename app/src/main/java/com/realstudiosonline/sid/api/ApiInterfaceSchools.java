package com.realstudiosonline.sid.api;

import com.realstudiosonline.sid.models.SchoolDataResponse;
import com.realstudiosonline.sid.models.StudentsResponse;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface ApiInterfaceSchools {

 String baseUrl = "https://dextraclass.com";

 @GET("/api/sid/schools/all-schools?list=all")
 Observable<SchoolDataResponse> getAllSchools(@Header("Authorization") String token);


 //getStudents
 @GET("/api/sid/schools/students/{schoolCode}")
 Single<StudentsResponse> getStudents(@Header("Authorization") String token, @Path("schoolCode") String schoolCode);


 @GET("/uploads/schools/{school_code}")
 Single<Response<ResponseBody>> downloadSchoolCrest(@Path("school_code") String school_code);

}