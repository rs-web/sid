package com.realstudiosonline.sid.api;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClientSchools {

    private static ApiClientSchools apiClient;

    private ApiInterfaceSchools apiCallback;
    private final int RESULT_OK = 200;

    public static ApiClientSchools getApiClient() {
        if (apiClient == null) {
            apiClient = new ApiClientSchools();
        }
        return apiClient;
    }

    /**
     * Static method to to get api client instance
     *
     * @return ApiInterface instance
     */
    public ApiInterfaceSchools getInstance() {
        try {
            if (apiCallback == null) {

               // TokenInterceptor interceptor=new TokenInterceptor();

                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                //httpClient.addInterceptor(interceptor);
                httpClient.connectTimeout(1, TimeUnit.MINUTES);
                httpClient.readTimeout(1, TimeUnit.MINUTES);
                httpClient.writeTimeout(1, TimeUnit.MINUTES);
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                httpClient.addInterceptor(logging);  // <-- this is the important line!

                Retrofit client = new Retrofit.Builder()
                        .baseUrl(ApiInterfaceSchools.baseUrl)
                        .client(httpClient.build())
                        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                apiCallback = client.create(ApiInterfaceSchools.class);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return apiCallback;
    }
}
