package com.realstudiosonline.sid.api;

import com.realstudiosonline.sid.models.AddStudentResponse;
import com.realstudiosonline.sid.models.LoginResponse;
import com.realstudiosonline.sid.models.SetUpSchoolResponseData;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

  String baseUrl = "https://sid.desafrica.com";

   @Multipart
   @POST("/api/submit-student")
   Observable<Response<AddStudentResponse>> addStudent(
           @Part("status") RequestBody status,
           @Part("imei") RequestBody imei,
           @Part("projectId") RequestBody projectId,
           @Part("projectOfficerId") RequestBody projectOfficerId,
           @Part("classId") RequestBody classId,
           @Part("schoolCode") RequestBody schoolCode,
           @Part("studentId") RequestBody studentId,
           @Part("firstName") RequestBody firstName,
           @Part("lastName") RequestBody lastName,
           @Part("dob") RequestBody dob,
           @Part("gender") RequestBody gender,
           @Part("guardianPhone") RequestBody guardianPhone,
           @Part MultipartBody.Part bioRightThumb,
           @Part MultipartBody.Part bioLeftThumb,
           @Part MultipartBody.Part bioRightFore,
           @Part MultipartBody.Part bioLeftFore,
           @Part MultipartBody.Part profile
          /* @Part MultipartBody.Part card*/);



   @FormUrlEncoded
   @POST("api/setup-school")
   Single<Response<SetUpSchoolResponseData>> setUpSchool(@Field("schoolCode") String schoolCode,
                                               @Field("projectId") String projectId,
                                               @Field("contactPerson") String contactPerson,
                                               @Field("contactNumber") String contactNumber,
                                               @Field("status") String status);

   @FormUrlEncoded
   @POST("/api/login")
   Single<Response<LoginResponse>> auth(@Field("phone") String phone, @Field("password") String password);
   // @GET("forecast/daily")
  //  Flowable<ForecastData> getForecastData(@Query("appid") String apiKey, @Query("id") String id, @Query("units") String units);*/
}
