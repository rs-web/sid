package com.realstudiosonline.sid.api;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TokenInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {

        //rewrite the request to add bearer token
        Request newRequest=chain.request().newBuilder()
                .header("Authorization","Bearer "+ "2y10eZiFIBKBuS5SEtcgZ0YJAezQN63xcjOOm9IJBI8ploxw10Hf832")
                .build();

        return chain.proceed(newRequest);
    }
}