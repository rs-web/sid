package com.realstudiosonline.sid.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.models.GeneratedCard;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ReviewCardsAdapter extends RecyclerView.Adapter<ReviewCardsAdapter.ReviewCardsViewHolder> {


    List<GeneratedCard> cardList;
    public ReviewCardsAdapter(List<GeneratedCard> cardList) {
      this.cardList = cardList;
    }

    @NonNull
    @Override
    public ReviewCardsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ReviewCardsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_generated_card, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewCardsViewHolder holder, int position) {

        GeneratedCard generatedCard = cardList.get(position);

        //holder.tvCardNo.setText(generatedCard.getCardNo());
        Picasso.get().load(R.drawable.student_id).into(holder.imgGeneratedCard);
    }

    @Override
    public int getItemCount() {
        return cardList == null ? 0: cardList.size();
    }

    public static class ReviewCardsViewHolder extends RecyclerView.ViewHolder{

       // AppCompatTextView tvCardNo;
        AppCompatImageView imgGeneratedCard;
        public ReviewCardsViewHolder(@NonNull View itemView) {
            super(itemView);
           imgGeneratedCard =  itemView.findViewById(R.id.img_card);
          // tvCardNo = itemView.findViewById(R.id.tv_card_no);
        }
    }



}
