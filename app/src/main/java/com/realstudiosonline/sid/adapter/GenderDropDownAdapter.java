package com.realstudiosonline.sid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.models.Gender;

import java.util.ArrayList;

public class GenderDropDownAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private ArrayList<Gender> genders;
    private ArrayList<Gender> suggestions = new ArrayList<>();
    private Filter filter = new GenderFilter();
    /**
     * @param context      Context
     * @param genders Original list used to compare in constraints.
     */
    public GenderDropDownAdapter(Context context, ArrayList<Gender> genders) {
        this.context = context;
        this.genders = genders;
    }

    @Override
    public int getCount() {
        return suggestions.size(); // Return the size of the suggestions list.
    }

    @Override
    public Gender getItem(int position) {
        return suggestions.get(position);
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * This is where you inflate the layout and also where you set what you want to display.
     * Here we also implement a View Holder in order to recycle the views.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_autotext,
                    parent,
                    false);
            holder = new ViewHolder();
            holder.autoText = (TextView) convertView.findViewById(R.id.autoText);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.autoText.setText(suggestions.get(position).getName());

        return convertView;
    }



    @Override
    public Filter getFilter() {
        return filter;
    }

    private static class ViewHolder {
        TextView autoText;
    }


    private class GenderFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            suggestions.clear();

            if (genders != null && constraint != null) { // Check if the Original List and Constraint aren't null.
                for (int i = 0; i < genders.size(); i++) {
                    if (genders.get(i).getName().toLowerCase().contains(constraint.toString().toLowerCase())) { // Compare item in original list if it contains constraints.
                        suggestions.add(genders.get(i)); // If TRUE add item in Suggestions.
                    }
                }
            }
            FilterResults results = new FilterResults(); // Create new Filter Results and return this to publishResults;
            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
