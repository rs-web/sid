package com.realstudiosonline.sid.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.realstudiosonline.sid.fragments.CameraXFragment;
import com.realstudiosonline.sid.fragments.GenerateCardFragment;
import com.realstudiosonline.sid.fragments.ReviewCardsFragment;
import com.realstudiosonline.sid.fragments.WorkHistoryFragment;
import com.realstudiosonline.sid.fragments.StudentInfoFragment;
import com.realstudiosonline.sid.fragments.ReviewSessionFragment;


public class StudentInfoPhotoPagerAdapter  extends FragmentStateAdapter {




    public StudentInfoPhotoPagerAdapter(@NonNull FragmentManager fragmentManager,
                                        @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);

    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0:
                return StudentInfoFragment.newInstance("","");

            case 1:
                return CameraXFragment.newInstance();

            case 2:
                return GenerateCardFragment.newInstance("","");

            case 3:
                return ReviewCardsFragment.newInstance("","");

            case 4:
                return ReviewSessionFragment.newInstance("","");

            case 5:
                return WorkHistoryFragment.newInstance("","");

        }
        return null;
    }

    @Override
    public int getItemCount() {
        return 6;
    }



    /*
    *
    * switch (position){
            case 0:
                return StudentInfoFragment.newInstance("","");

            case 1:
                return PhotoBioFragment.newInstance("","");
            case 2:
                return GenerateCardFragment.newInstance("","");
        }
        return null;
    * */
}

