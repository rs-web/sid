package com.realstudiosonline.sid.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.models.School;
import com.realstudiosonline.sid.models.StudentInfo;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StudentsDropDownAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<StudentInfo> schools;
    private List<StudentInfo> suggestions = new ArrayList<>();
    private Filter filter = new GenderFilter();
    private int size = 0;

    public static class SchoolDiff extends DiffUtil.ItemCallback<School> {

        @Override
        public boolean areItemsTheSame(@NonNull School oldItem, @NonNull School newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull School oldItem, @NonNull School newItem) {
            return oldItem.getSchool_id() == (newItem.getSchool_id());
        }
    }
    /**
     * @param context      Context
     * @param schools Original list used to compare in constraints.
     */
    public StudentsDropDownAdapter(Context context, List<StudentInfo> schools) {
        this.context = context;
        this.schools = schools;
        size = suggestions.size();
    }

    @Override
    public int getCount() {
        return size;
        // Return the size of the suggestions list.
    }

    @Override
    public void notifyDataSetChanged() {
        size = suggestions.size();

        super.notifyDataSetChanged();
    }

    @Override
    public StudentInfo getItem(int position) {
        if(suggestions.size()>position)
        return suggestions.get(position);
        else return null;
    }


    @Override
    public long getItemId(int position) {
        return position< suggestions.size()?  suggestions.get(position).getId() : 0;
    }

    /**
     * This is where you inflate the layout and also where you set what you want to display.
     * Here we also implement a View Holder in order to recycle the views.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = LayoutInflater.from(context);

        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_student,
                    parent,
                    false);
            holder = new ViewHolder();
            holder.autoText = (TextView) convertView.findViewById(R.id.autoText);
            holder.tvProgram = (TextView) convertView.findViewById(R.id.tvProgram);
            holder.tvYear_group = (TextView) convertView.findViewById(R.id.tvYear_group);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if(suggestions.size()>0 ){

            if(position<suggestions.size()) {
                int year = Integer.parseInt(suggestions.get(position).getYear_group());
                DateTime now  = DateTime.now();
                DateTime d2 = new DateTime(year,1,1,0,0);
                Period period = new Period( d2,now, PeriodType.years());
                int differenceYears = period.getYears();


                holder.autoText.setText(suggestions.get(position).getStudent_name());
                //String yearGroup = "<b>Form :</b> "+suggestions.get(position).getYear_group();
                String yearGroup = "<b>Form :</b> "+differenceYears;
                String program = "<b>Program: </b>"+suggestions.get(position).getProgram();
                holder.tvYear_group.setText(Html.fromHtml(yearGroup));
                holder.tvProgram.setText(Html.fromHtml(program));
            }
        }

        return convertView;
    }



    @Override
    public Filter getFilter() {
        return filter;
    }

    private static class ViewHolder {
        TextView autoText;
        TextView tvProgram;
        TextView tvYear_group;
    }


    private class GenderFilter extends Filter {
        public boolean containsIgnoreCase(String hayStack, String needle){
            if(needle.equals(""))
            return true;
            if(hayStack == null || hayStack.equals(""))
                return false;

            Pattern pattern = Pattern.compile(needle,Pattern.CASE_INSENSITIVE+ Pattern.LITERAL);
            Matcher matcher = pattern.matcher(hayStack);
            return matcher.find();

        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            suggestions.clear();

            if(constraint != null) {
                if (constraint.toString().length() > 1){
                    if (schools != null) { // Check if the Original List and Constraint aren't null.
                        for (int i = 0; i < schools.size(); i++) {
                            if (containsIgnoreCase(schools.get(i).getIndex_number_jhs().toLowerCase(), constraint.toString().toLowerCase()) ||
                                    containsIgnoreCase(schools.get(i).getStudent_name().toLowerCase(), constraint.toString().toLowerCase())
                            ) { // Compare item in original list if it contains constraints.
                                suggestions.add(schools.get(i)); // If TRUE add item in Suggestions.
                            }
                        }
                    }
            }

               /* String [] strings = constraint.toString().split("\\s+");
                for (String string:strings) {
                    if (schools != null) { // Check if the Original List and Constraint aren't null.
                        for (int i = 0; i < schools.size(); i++) {
                            if (containsIgnoreCase(schools.get(i).getIndex_number_jhs().toLowerCase(),string.toLowerCase())||
                                    containsIgnoreCase(schools.get(i).getStudent_name().toLowerCase(),string.toLowerCase())
                            ) { // Compare item in original list if it contains constraints.
                                suggestions.add(schools.get(i)); // If TRUE add item in Suggestions.
                            }
                        }
                    }
                }
*/
            }

            FilterResults results = new FilterResults(); // Create new Filter Results and return this to publishResults;
            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
           /* context.runOnUiThread(() -> {
                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            });*/

           /* new Handler(Looper.getMainLooper()).post(()->{
                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            });*/
        }
    }
}
