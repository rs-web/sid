package com.realstudiosonline.sid.adapter;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DiffUtil;

import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.models.School;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Pattern;

public class SchoolDropDownAdapter extends BaseAdapter implements Filterable {

    private AppCompatActivity context;
    private ArrayList<School> schools;
    private ArrayList<School> suggestions = new ArrayList<>();
    private Filter filter = new GenderFilter();
    private int size = 0;

    public static class SchoolDiff extends DiffUtil.ItemCallback<School> {

        @Override
        public boolean areItemsTheSame(@NonNull School oldItem, @NonNull School newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull School oldItem, @NonNull School newItem) {
            return oldItem.getSchool_id() == (newItem.getSchool_id());
        }
    }
    /**
     * @param context      Context
     * @param schools Original list used to compare in constraints.
     */
    public SchoolDropDownAdapter(AppCompatActivity context, ArrayList<School> schools) {
        this.context = context;
        this.schools = schools;
        size = suggestions.size();
    }

    @Override
    public int getCount() {
        return size;
        // Return the size of the suggestions list.
    }

    @Override
    public void notifyDataSetChanged() {
        size = suggestions.size();

        super.notifyDataSetChanged();
    }

    @Override
    public School getItem(int position) {
        if(suggestions.size()>position)
        return suggestions.get(position);
        else return null;
    }


    @Override
    public long getItemId(int position) {
        return  0;
    }

    /**
     * This is where you inflate the layout and also where you set what you want to display.
     * Here we also implement a View Holder in order to recycle the views.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_school,
                    parent,
                    false);
            holder = new ViewHolder();
            holder.autoTextSchoolName = (TextView) convertView.findViewById(R.id.auto_text_school_name);
            holder.autoTextGender = (TextView) convertView.findViewById(R.id.auto_text_gender);
            holder.autoTextSchoolCode = (TextView) convertView.findViewById(R.id.auto_text_school_code);
            holder.autoTextDistrict = (TextView) convertView.findViewById(R.id.auto_text_district);
            holder.autoTextRegion = (TextView) convertView.findViewById(R.id.auto_text_region);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

       if(suggestions.size()>0 ){
           if(position<suggestions.size()) {
               School school = suggestions.get(position);
               holder.autoTextSchoolName.setText(suggestions.get(position).getSchool_name());
               String gender="";
               if(school.getGender().equalsIgnoreCase("mixed"))
                   gender = "<b>M</b>";
               else if(school.getGender().equalsIgnoreCase("boys"))
                   gender = "<b>B</b>";
               else if(school.getGender().equalsIgnoreCase("girls"))
                   gender = "<b>G</b>";

               String text = school.getRegion() + ", "+ school.getLocation() + " <b>|</b> G :- "+gender;
               holder.autoTextSchoolCode.setText(Html.fromHtml(text));
               /*String gender = "<b>Gender :</b> "+school.getGender();
               String schoolCode = "<b>School Code: </b>"+school.getSchool_code();
               String district = "<b>Gender :</b> "+school.getDistrict();
               String region = "<b>Region : </b>"+school.getRegion();
               holder.autoTextGender.setText(Html.fromHtml(gender));
               holder.autoTextDistrict.setText(Html.fromHtml(district));
               holder.autoTextSchoolCode.setText(Html.fromHtml(schoolCode));
               holder.autoTextRegion.setText(Html.fromHtml(region));*/



               /*
               *        School school = suggestions.get(position);
               holder.autoTextSchoolName.setText(suggestions.get(position).getSchool_name());
               String gender = "<b>Gender :</b> "+school.getGender();
               String schoolCode = "<b>School Code: </b>"+school.getSchool_code();
               String district = "<b>Gender :</b> "+school.getDistrict();
               String region = "<b>Region : </b>"+school.getRegion();
               holder.autoTextGender.setText(Html.fromHtml(gender));
               holder.autoTextDistrict.setText(Html.fromHtml(district));
               holder.autoTextSchoolCode.setText(Html.fromHtml(schoolCode));
               holder.autoTextRegion.setText(Html.fromHtml(region));*/

           }
       }

        return convertView;
    }



    @Override
    public Filter getFilter() {
        return filter;
    }

    private static class ViewHolder {
        TextView autoTextSchoolName;
        TextView autoTextGender;
        TextView autoTextSchoolCode;
        TextView autoTextDistrict;
        TextView autoTextRegion;
    }


    private class GenderFilter extends Filter {
        public boolean containsIgnoreCase(String hayStack, String needle){
            if(needle.equals(""))
            return true;
            if(hayStack == null || hayStack.equals(""))
                return false;

          return Pattern.compile(Pattern.quote(needle), Pattern.CASE_INSENSITIVE).matcher(hayStack).find();



        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
           // ArrayList<School> tempSchools = new ArrayList<>();
            suggestions.clear();

            if(constraint != null){
               /* String [] strings = constraint.toString().split("\\s+");
                for (int i = 0; i<strings.length; i++) {
                    String string = strings[i];
                    if (schools != null) { // Check if the Original List and Constraint aren't null.
                        for (int j = 0; j < schools.size(); j++) {
                            if (containsIgnoreCase(schools.get(j).getSchool_name().toLowerCase(),string.toLowerCase())) {
                                // Compare item in original list if it contains constraints.
                                //tempSchools.add(schools.get(j));

                               suggestions.add(schools.get(j)); // If TRUE add item in Suggestions.
                            }
                        }
                    }
                }*/
               /* for (School school : tempSchools) {
                    if(){

                    }
                }*/

                if (schools != null) { // Check if the Original List and Constraint aren't null.
                    for (School school: schools) {
                        if (containsIgnoreCase(school.getSchool_name().toLowerCase(),constraint.toString().toLowerCase())) {
                            // Compare item in original list if it contains constraints.
                            //tempSchools.add(schools.get(j));

                            suggestions.add(school); // If TRUE add item in Suggestions.
                        }
                    }
                }

                Collections.sort(suggestions, (school1, school2) -> school1.getSchool_code().compareToIgnoreCase(school2.getSchool_code()));


            }

            FilterResults results = new FilterResults();// Create new Filter Results and return this to publishResults;

            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
           /* context.runOnUiThread(() -> {
                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            });*/

           /* new Handler(Looper.getMainLooper()).post(()->{
                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            });*/
        }
    }
}
