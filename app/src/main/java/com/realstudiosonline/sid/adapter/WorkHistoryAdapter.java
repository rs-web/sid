package com.realstudiosonline.sid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.models.SessionLog;
import com.realstudiosonline.sid.models.SessionLogObject;

import java.util.List;

public class WorkHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    List<SessionLogObject> workHistoryList;
    private Context mContext;
    private static int TYPE_HEADER = -1;


    public WorkHistoryAdapter(List<SessionLogObject> workHistoryList, Context mContext) {
        this.workHistoryList = workHistoryList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder  onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {



        View view;
        if (viewType == TYPE_HEADER) { // for call layout
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_header, parent, false);
            return new HeaderViewHolder(view);

        }

        else { // for email layout
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_review_session, parent, false);
            return new WorkHistoryViewHolder(view);
        }

    }


    @Override
    public int getItemViewType(int position) {
        if (workHistoryList.get(position).getId().equals("-1")) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        SessionLogObject workHistory = workHistoryList.get(position);

        if (getItemViewType(position) == TYPE_HEADER) {
            ((HeaderViewHolder) holder).bind(workHistory);
        } else {
            ((WorkHistoryViewHolder) holder).bind(workHistory);
        }



    }

    @Override
    public int getItemCount() {
        return workHistoryList == null ? 0: workHistoryList.size();
    }

    public static class WorkHistoryViewHolder extends RecyclerView.ViewHolder{

        AppCompatTextView btnSession,btnPressReport, tvDate, tvNameOfSchool;
        public WorkHistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            btnSession = itemView.findViewById(R.id.btn_session);
            tvDate = itemView.findViewById(R.id.tv_date);
            tvNameOfSchool = itemView.findViewById(R.id.tv_name_of_school);
            btnPressReport = itemView.findViewById(R.id.btn_report_press);


        }

        public void bind(SessionLogObject workHistory){
            switch (new SessionLog(workHistory.getSession_log()).sessionType){

                case
                        SessionLog.PAUSED:
                    btnSession.setBackgroundResource(R.drawable.selector_black);
                    break;

                case
                        SessionLog.CONTINUE:
                    btnSession.setBackgroundResource(R.drawable.selector_yellow);
                    break;
                case
                        SessionLog.ENDED:
                    btnSession.setBackgroundResource(R.drawable.selector_pink);

                    break;

                case
                        SessionLog.STARTED:
                    btnSession.setBackgroundResource(R.drawable.selector_green);
                    break;

            }

           /* switch (workHistory.getPressReport().reportType){


                case
                        PressReport.LOADING:
                    break;


                case
                        PressReport.PRINTED:
                    btnPressReport.setBackgroundResource(R.drawable.selector_blue);
                    break;

                case
                        PressReport.START:
                    btnPressReport.setBackgroundResource(R.drawable.selector_green);
                    break;


                case
                        PressReport.SUBMITTED:
                    btnPressReport.setBackgroundResource(R.drawable.selector_white_borderless);
                    btnPressReport.setTextColor(mContext.getResources().getColor(R.color.colorBlack));

                    break;

            }*/
            btnSession.setText(new SessionLog(workHistory.getSession_log()).sessionType);
            tvNameOfSchool.setText(workHistory.getSchool_name());
          //  LocalDateTime ldt = LocalDateTime.parse(workHistory.getSession_date(), DateTimeFormat.forPattern("yyyy/MM/dd hh:mm aa").withLocale(Locale.ENGLISH));
            //DateTime dateTime = DateTime.parse(workHistory.getSession_date(), DateTimeFormat.forPattern("yyyy/MM/dd"));
            //tvDate.setText(ldt.toString());
            //btnPressReport.setText(workHistory.getPressReport().reportType);
        }
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder{

        AppCompatTextView tvSessionLog,tvPressReport, tvSchoolName;
        public HeaderViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSessionLog = itemView.findViewById(R.id.tv_session_log);
            tvPressReport = itemView.findViewById(R.id.tv_press_report);
            tvSchoolName = itemView.findViewById(R.id.tv_name_of_school);

        }

        public void bind(SessionLogObject workHistory){

        }
    }
}
