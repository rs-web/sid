package com.realstudiosonline.sid.utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;

import com.realstudiosonline.sid.R;

import java.util.HashMap;

public class SoundPoolPlayer {
    private SoundPool mShortPlayer= null;
    private HashMap mSounds = new HashMap();

    public SoundPoolPlayer(Context pContext)
    {
        // setup Soundpool


        mShortPlayer = new SoundPool.Builder()
                .setMaxStreams(10)
                .build();

        mSounds.put(R.raw.verified, this.mShortPlayer.load(pContext, R.raw.verified, 1));
        mSounds.put(R.raw.failed, this.mShortPlayer.load(pContext, R.raw.failed, 1));
        mSounds.put(R.raw.please_try_again, this.mShortPlayer.load(pContext, R.raw.please_try_again, 1));
        mSounds.put(R.raw.unrecognized, this.mShortPlayer.load(pContext, R.raw.unrecognized, 1));
    }

    public void playShortResource(int piResource) {
        int iSoundId = (Integer) mSounds.get(piResource);
        this.mShortPlayer.play(iSoundId, 0.99f, 0.99f, 0, 0, 1);
    }

    // Cleanup
    public void release() {
        // Cleanup
        this.mShortPlayer.release();
        this.mShortPlayer = null;
    }
}
