package com.realstudiosonline.sid.utils.fingerprint;

import java.util.Arrays;

/**
 * Created by Administrator on 2018/6/6.
 */

public class UserData {
   public   String name;
    public  byte[] template;

    public UserData(String name, byte[] data, int len) {
        this.name = name;
        this.template = Arrays.copyOf(data, len);
    }
}
