package com.realstudiosonline.sid.utils;

import android.content.Context;
import android.util.TypedValue;

public class PxUtils {
    public static int dpToPx(int dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public static int spToPx(int sp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }


   /* public  static int getDrawableByID(Context context, String schoolCode){
        return context.getResources().getIdentifier("sch"+schoolCode, "drawable", context.getPackageName());
    }*/
}
