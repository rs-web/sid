package com.realstudiosonline.sid.utils.fingerprint;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.devkit.api.Misc;
import android.graphics.Bitmap;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.realstudiosonline.sid.R;
import com.suprema.BioMiniFactory;
import com.suprema.CaptureResponder;
import com.suprema.IBioMiniDevice;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

public class BioMini2Activity extends AppCompatActivity implements View.OnClickListener {
    public static final boolean mbUsbExternalUSBManager = false;
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private UsbManager mUsbManager = null;
    private PendingIntent mPermissionIntent = null;
    private static BioMiniFactory mBioMiniFactory = null;
    public static final int REQUEST_WRITE_PERMISSION = 786;
    public IBioMiniDevice mCurrentDevice = null;
    private BioMini2Activity mainContext;
    public final static String TAG = "BioMini Sample";
    private EditText mLogView;
    private ScrollView mScrollLog = null;
    private static final int VERI=1;
    private static final int CAPT=2;
    private int flag=0;


    public static void start(Context context) {
        context.startActivity(new Intent(context, BioMini2Activity.class));
    }
    class MyHandler extends Handler {
        private WeakReference<BioMini2Activity> mActivityRef;

        public MyHandler(BioMini2Activity activity) {
            mActivityRef = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            BioMini2Activity activity = mActivityRef.get();
            switch (msg.what) {
                case VERI:
                    //verify();
                    break;
                case CAPT:
                    if(flag==0) {
                        reCapture();
                    }
                    break;

            }
        }
    }

    private final MyHandler mHandler = new MyHandler(this);
    private ArrayList<UserData> mUsers = new ArrayList<>();
    private IBioMiniDevice.CaptureOption mCaptureOptionDefault = new IBioMiniDevice.CaptureOption();
    /**
     *handle the capture result
     */
    private CaptureResponder mCaptureResponseDefault = new CaptureResponder() {
        @Override
        public boolean onCaptureEx(final Object context, final Bitmap capturedImage,
                                   final IBioMiniDevice.TemplateData capturedTemplate,
                                   final IBioMiniDevice.FingerState fingerState) {
            flag=1;
            log("onCapture : Capture successful!");
            printState(getResources().getText(R.string.capture_single_ok));
            byte[] raw = mCurrentDevice.getCaptureImageAsRAW_8();
            int quailty = mCurrentDevice.getFPQuality(raw,capturedImage.getWidth(),capturedImage.getHeight(),0);
            log("image NFIQ :"+quailty);
            log(((IBioMiniDevice) context).popPerformanceLog());
            runOnUiThread((Runnable) () -> {
                if (capturedImage != null) {
                    ImageView iv = (ImageView) findViewById(R.id.imagePreview);
                    if (iv != null) {
                        iv.setImageBitmap(capturedImage);
                    }
                }
            });

            return true;
        }

        @Override
        public void onCaptureError(Object contest, int errorCode, String error) {
            log("onCaptureError : " + error);
            if (errorCode != IBioMiniDevice.ErrorCode.OK.value())
                printState(getResources().getText(R.string.capture_single_fail));
        }
    };


    /**
     *
     *
     * @param str
     */
    synchronized public void printState(final CharSequence str) {
        log(str.toString());

    }


    synchronized public void log(final String msg) {
        Log.e(TAG, msg);
        runOnUiThread(() -> {
            if (mLogView != null) {
                //mLogView.getText().clear();
                mLogView.append(msg + "\n");
                if (mScrollLog != null) {
                    //mScrollLog.scrollTo(0, mScrollLog.getBottom());
                    mScrollLog.fullScroll(ScrollView.FOCUS_DOWN);
                }
            } else {
                Log.e("", msg);
            }
        });
    }

    synchronized public void printRev(final String msg) {
        log(msg);
    }


    /**
     *usb permission request
     */
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (device != null) {
                            if (mBioMiniFactory == null) return;
                            mBioMiniFactory.addDevice(device);
                            log(String.format(Locale.ENGLISH, "Initialized device count- BioMiniFactory (%d)", mBioMiniFactory.getDeviceCount()));
                        }
                    } else {
                        Log.e(TAG, "permission denied for device" + device);
                    }
                }
            }
        }
    };

    // check the finger print module
    public void checkDevice() {
        if (mUsbManager == null) return;
        HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();
        log("checkDevice..szie"+deviceList.size());
        Iterator<UsbDevice> deviceIter = deviceList.values().iterator();
        while (deviceIter.hasNext()) {
            UsbDevice _device = deviceIter.next();
            Log.e(TAG," device vid:"+_device.getVendorId()+" pid:"+_device.getProductId()+" name:"+_device.getDeviceName());
            if (_device.getVendorId() == 0x16d1) {
                //Suprema vendor ID
                Log.e(TAG,"find Fingerprint module bioslim2 : "+0x16d1);
                mUsbManager.requestPermission(_device, mPermissionIntent);
                if (mBioMiniFactory == null) return;
                mBioMiniFactory.addDevice(_device);
                mCurrentDevice=mBioMiniFactory.getDevice(0);
            } else {
            }
        }

    }

    /**
     * power on/off the fingerprint scanner
     * If use M8 need power on when oncreate
     *
     * @param isOpen
     */
    private void switchPower(boolean isOpen) {
        Misc.nativeUsbMode(isOpen ? 1 : 0);
        Misc.fingerEnable(isOpen);

    }

    @Override
    public void onClick(View view) {
        Log.e(TAG,"init button click method....");
        switch (view.getId()) {
            case R.id.buttonCaptureSingle:
                capture(view);
                break;
            case R.id.buttonEnroll:
                enroll(view);
                break;
            case R.id.buttonVerify:
                verify(view);
                break;
            case R.id.buttonDeleteAll:
                deleteAll(view);
                break;
            case R.id.buttonExportBmp:
                saveBMP(view);
                break;
            case R.id.buttonExportWsq:
                saveWSQ(view);
                break;
            case R.id.buttonTemplate:
                saveTemplate(view);
                break;

        }

    }

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        switchPower(true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biometric);
       // verifyStoragePermissions(Biomini2Activity.this);
        mScrollLog =findViewById(R.id.scrollLog);
        mLogView = findViewById(R.id.editLog);
        mainContext = BioMini2Activity.this;
        mCaptureOptionDefault.frameRate = IBioMiniDevice.FrameRate.SHIGH;
        findViewById(R.id.buttonCaptureSingle).setOnClickListener(this);
        findViewById(R.id.buttonDeleteAll).setOnClickListener(this);
        findViewById(R.id.buttonExportBmp).setOnClickListener(this);
        findViewById(R.id.buttonExportWsq).setOnClickListener(this);
        findViewById(R.id.buttonTemplate).setOnClickListener(this);
        findViewById(R.id.buttonEnroll).setOnClickListener(this);
        findViewById(R.id.buttonVerify).setOnClickListener(this);
        if (mBioMiniFactory != null) {
            mBioMiniFactory.close();
        }

        if (mbUsbExternalUSBManager) {
            mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
            mBioMiniFactory = new BioMiniFactory(mainContext, mUsbManager) {
                @Override
                public void onDeviceChange(DeviceChangeEvent event, Object dev) {

                    log("----------------------------------------");
                    log("onDeviceChange : " + event + " using external usb-manager");
                    log("----------------------------------------");
                    if (event == DeviceChangeEvent.DEVICE_ATTACHED && mCurrentDevice == null) {
                        new Thread(() -> {
                            int cnt = 0;
                            while (mBioMiniFactory == null && cnt < 20) {
                                SystemClock.sleep(1000);
                                cnt++;
                            }
                            if (mBioMiniFactory != null) {
                                mCurrentDevice = mBioMiniFactory.getDevice(0);
                                Log.e(TAG, "mCurrentDevice attached : " + mCurrentDevice);
                                if (mCurrentDevice != null) {
                                    log(" DeviceName : " + mCurrentDevice.getDeviceInfo().deviceName);
                                    log("         SN : " + mCurrentDevice.getDeviceInfo().deviceSN);
                                    log("SDK version : " + mCurrentDevice.getDeviceInfo().versionSDK);
                                }
                            }
                        }).start();
                    } else if (mCurrentDevice != null && event == DeviceChangeEvent.DEVICE_DETACHED && mCurrentDevice.isEqual(dev)) {
                        Log.e(TAG, "mCurrentDevice removed : " + mCurrentDevice);
                        mCurrentDevice = null;
                    }
                }
            };
            //
            mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
            IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
            registerReceiver(mUsbReceiver, filter);
            checkDevice();


        } else {
            mBioMiniFactory = new BioMiniFactory(mainContext) {
                @Override
                public void onDeviceChange(DeviceChangeEvent event, Object dev) {
                    log("----------------------------------------");
                    log("onDeviceChange : " + event);
                    log("----------------------------------------");
                    if (event == DeviceChangeEvent.DEVICE_ATTACHED && mCurrentDevice == null) {
                        new Thread(() -> {
                            int cnt = 0;
                            while (mBioMiniFactory == null && cnt < 20) {
                                SystemClock.sleep(1000);
                                cnt++;
                            }
                            if (mBioMiniFactory != null) {
                                mCurrentDevice = mBioMiniFactory.getDevice(0);
                                Log.e(TAG, "mCurrentDevice attached : " + mCurrentDevice);
                                if (mCurrentDevice != null) {
                                    log(" DeviceName : " + mCurrentDevice.getDeviceInfo().deviceName);
                                    log("         SN : " + mCurrentDevice.getDeviceInfo().deviceSN);
                                    log("SDK version : " + mCurrentDevice.getDeviceInfo().versionSDK);
                                }
                            }
                        }).start();
                    } else if (mCurrentDevice != null && event == DeviceChangeEvent.DEVICE_DETACHED && mCurrentDevice.isEqual(dev)) {
                        Log.e(TAG, "mCurrentDevice removed : " + mCurrentDevice);
                        mCurrentDevice = null;
                    }
                }
            };
        }
        mBioMiniFactory.setTransferMode(IBioMiniDevice.TransferMode.MODE2);
        printRev("" + mBioMiniFactory.getSDKInfo());
    }



    //android 6.0+ need  dynamic get storage read/write permission
    public static void verifyStoragePermissions(Activity activity) {

        try {
            //检测是否有写的权限
            int permission = ActivityCompat.checkSelfPermission(activity,
                    "android.permission.WRITE_EXTERNAL_STORAGE");
            if (permission != PackageManager.PERMISSION_GRANTED) {
                // 没有写的权限，去申请写的权限，会弹出对话框
                ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE,REQUEST_EXTERNAL_STORAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        switchPower(false); //power off the scanner
        if (mBioMiniFactory != null) {
            mBioMiniFactory.close();
            mBioMiniFactory = null;
        }
        super.onDestroy();
    }


    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            log("permission granted");
        }
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        requestPermission();
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        switchPower(false);
        if (mBioMiniFactory != null) {
            mBioMiniFactory.close();
            mBioMiniFactory = null;
        }
        super.onBackPressed();
    }



    //capture a finger and show the image
    public void capture(View view){
        Log.e(TAG,"start to capture");
        flag=0;
        ((ImageView) findViewById(R.id.imagePreview)).setImageBitmap(null);
        if (mCurrentDevice != null) {
            mBioMiniFactory.setTransferMode(IBioMiniDevice.TransferMode.MODE2);
            mCurrentDevice.setParameter(new IBioMiniDevice.Parameter(IBioMiniDevice.ParameterType.TIMEOUT, 15000));
            mCaptureOptionDefault.frameRate = IBioMiniDevice.FrameRate.MID;
            mCurrentDevice.captureSingle(
                    mCaptureOptionDefault,
                    mCaptureResponseDefault,
                    true);
            //mHandler.sendEmptyMessageDelayed(CAPT,3000);
        }
    }

    public void reCapture(){
        mCurrentDevice.abortCapturing();
        flag=0;
        if(mCurrentDevice.isCapturing()){
            Log.e(TAG,"device captuing busy");
            return;
        }
/*        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        ((ImageView) findViewById(R.id.imagePreview)).setImageBitmap(null);
        mBioMiniFactory.setTransferMode(IBioMiniDevice.TransferMode.MODE2);
        if(mCurrentDevice != null) {
            mCurrentDevice.setParameter(new IBioMiniDevice.Parameter(IBioMiniDevice.ParameterType.TIMEOUT, 15000));
            int outtime=  (int)mCurrentDevice.getParameter(IBioMiniDevice.ParameterType.TIMEOUT).value;
            log(" Recapture, time out value:"+outtime/1000);
            Log.e(TAG,"Recapture start to capture");
            //mCaptureOptionDefault.captureTimeout = (int)mCurrentDevice.getParameter(IBioMiniDevice.ParameterType.TIMEOUT).value;
            mCurrentDevice.captureSingle(mCaptureOptionDefault, mCaptureResponseDefault, true);
           // mHandler.sendEmptyMessageDelayed(CAPT,2000);
        }
    }

    public void abortCapture(){
        if(mCurrentDevice != null) {
            new Thread(() -> {
                mCurrentDevice.abortCapturing();
                int nRetryCount =0;
                while(mCurrentDevice != null && mCurrentDevice.isCapturing()){
                    SystemClock.sleep(10);
                    nRetryCount++;
                }
                Log.d("AbortCapturing" , String.format(Locale.ENGLISH , "IsCapturing return false.(Abort-lead time: %dms) " ,
                        nRetryCount* 10));
            }).start();
        }
    }


    //delete all the fingerprint in userlist
    public void deleteAll(View view){
        mUsers.clear();
        printState(getResources().getText(R.string.delete_user));
    }

    //enroll a finger print (capture and get a fingerprint template and store it into t ArrayList)
    public void enroll(View view){
        if (mCurrentDevice != null) {
            //mCurrentDevice.setParameter(new IBioMiniDevice.Parameter(IBioMiniDevice.ParameterType.TEMPLATE_TYPE, 2002));
            final String userName = ((EditText) findViewById(R.id.editUsername)).getText().toString();
            if (userName.equals("")) {
                log("<<ERROR>> There is no user name");
                return;
            }
            ((ImageView) findViewById(R.id.imagePreview)).setImageBitmap(null);
            IBioMiniDevice.CaptureOption option = new IBioMiniDevice.CaptureOption();
            mBioMiniFactory.setTransferMode(IBioMiniDevice.TransferMode.MODE2);
            mCurrentDevice.setParameter(new IBioMiniDevice.Parameter(IBioMiniDevice.ParameterType.TIMEOUT, 15000));
            option.frameRate = IBioMiniDevice.FrameRate.MID;
            option.captureTemplate = true;
            // capture fingerprint image
            mCurrentDevice.captureSingle(option,
                    new CaptureResponder() {
                        @Override
                        public boolean onCaptureEx(final Object context, final Bitmap capturedImage,
                                                   final IBioMiniDevice.TemplateData capturedTemplate,
                                                   final IBioMiniDevice.FingerState fingerState) {
                            runOnUiThread((Runnable) () -> {
                                if (capturedImage != null) {
                                    ImageView iv = (ImageView) findViewById(R.id.imagePreview);
                                    if (iv != null) {
                                        iv.setImageBitmap(capturedImage);
                                    }
                                }
                            });
                            if (capturedTemplate != null) {
                                mUsers.add(new UserData(userName, capturedTemplate.data, capturedTemplate.data.length));
                                log("User data added : " + userName);
                                printState(getResources().getText(R.string.enroll_ok));
                            } else {
                                log("<<ERROR>> Template is not extracted...");
                                printState(getResources().getText(R.string.enroll_fail));
                            }
                            log(((IBioMiniDevice) context).popPerformanceLog());

                            return true;
                        }

                        @Override
                        public void onCaptureError(Object context, int errorCode, String error) {
                            log("onCaptureError : " + error);
                            printState(getResources().getText(R.string.enroll_fail));
                        }
                    }, true);
        }
    }


    //convert a file to  byte[]
    public static byte[] toByteArray2(String filename) throws IOException {

        File f = new File(filename);
        if (!f.exists()) {
            throw new FileNotFoundException(filename);
        }
        FileChannel channel = null;
        FileInputStream fs = null;
        try {
            fs = new FileInputStream(f);
            channel = fs.getChannel();
            ByteBuffer byteBuffer = ByteBuffer.allocate((int) channel.size());
            while ((channel.read(byteBuffer)) > 0) {
                // do nothing
                // System.out.println("reading");
            }
            return byteBuffer.array();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            if(channel!=null){
                try {
                    channel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    fs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }



    //identify the Fingerprint you captured from the Fingerprint list
    public void verify(View view){
        if (mCurrentDevice != null) {
            if (mUsers.size() == 0) {
                log("There is no enrolled data");
                return;
            }
            // capture fingerprint image

            IBioMiniDevice.CaptureOption option = new IBioMiniDevice.CaptureOption();
            mBioMiniFactory.setTransferMode(IBioMiniDevice.TransferMode.MODE2);
            mCurrentDevice.setParameter(new IBioMiniDevice.Parameter(IBioMiniDevice.ParameterType.TIMEOUT, 15000));
            option.frameRate = IBioMiniDevice.FrameRate.MID;
            option.captureTemplate = true;
            // capture fingerprint image
            mCurrentDevice.captureSingle(option,
                    new CaptureResponder() {
                        @Override
                        public boolean onCaptureEx(final Object context, final Bitmap capturedImage,
                                                   final IBioMiniDevice.TemplateData capturedTemplate,
                                                   final IBioMiniDevice.FingerState fingerState) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (capturedImage != null) {
                                        ImageView iv = (ImageView) findViewById(R.id.imagePreview);
                                        if (iv != null) {
                                            iv.setImageBitmap(capturedImage);
                                        }
                                    }
                                }
                            });
                            if (capturedTemplate != null) {
                                boolean isMatched = false;
                                String matchedName = "";
                                long a=System.currentTimeMillis();
                                for (UserData ud : mUsers) {
                                    if (mCurrentDevice.verify(capturedTemplate.data, capturedTemplate.data.length, ud.template, ud.template.length)) {
                                        isMatched = true;
                                        matchedName = ud.name;
                                        //break;
                                    }
                                    Log.e(TAG,"user:"+ud.name+" :"+isMatched);
                                }
                                long b=System.currentTimeMillis()-a;
                                log("match cost time:"+b);
                                Log.e(TAG," use time"+b);
                                if (isMatched) {
                                    log("Match found : " + matchedName);
                                    printState(getResources().getText(R.string.verify_ok));
                                } else {
                                    log("No match found : ");
                                    printState(getResources().getText(R.string.verify_not_match));
                                }
                            } else {
                                log("<<ERROR>> Template is not extracted...");
                                printState(getResources().getText(R.string.verify_fail));
                            }
                            return true;
                        }

                        @Override
                        public void onCaptureError(Object context, int errorCode, String error) {
                            log("onCaptureError : " + error);
                            printState(getResources().getText(R.string.capture_fail));
                        }
                    }, true);
        }
    }
    //save the image buffer from scanner as bmp to the storage
    public void saveBMP(View view){
        if (mCurrentDevice != null) {
            final String userName = ((EditText) findViewById(R.id.editUsername)).getText().toString();
            if (userName.equals("")) {
                log("<<ERROR>> There is no user name");
                printState(getResources().getText(R.string.no_user_name));
                return;
            }
            byte[] bmp = mCurrentDevice.getCaptureImageAsBmp();
            if (bmp == null) {
                log("<<ERROR>> Cannot get BMP buffer");
                printState(getResources().getText(R.string.export_bmp_fail));
                return;
            }
            try {
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + userName + "_capturedImage.bmp");
                FileOutputStream fos = new FileOutputStream(file);
                fos.write(bmp);
                fos.close();

                printState(getResources().getText(R.string.export_bmp_ok));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // save the image buffer from scanner as a wsq to the storage
    public void saveWSQ(View view){
        if (mCurrentDevice != null) {
            final String userName = ((EditText) findViewById(R.id.editUsername)).getText().toString();
            if (userName.equals("")) {
                log("<<ERROR>> There is no user name");
                printState(getResources().getText(R.string.no_user_name));
                return;
            }

            byte[] wsq = mCurrentDevice.getCaptureImageAsWsq(-1, -1, 3.5f, 0);
            if (wsq == null) {
                log("<<ERROR>> Cannot get WSQ buffer");
                printState(getResources().getText(R.string.export_wsq_fail));
                return;
            }
            try {
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + userName + "_capturedImage.wsq");
                FileOutputStream fos = new FileOutputStream(file);
                fos.write(wsq);
                fos.close();
                printState(getResources().getText(R.string.export_wsq_ok));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    //extract the template from the scanner buffer(before need to use capture first.), and save it to storage
    public void saveTemplate(View view){
        if (mCurrentDevice != null) {
            final String userName = ((EditText) findViewById(R.id.editUsername)).getText().toString();
            if (userName.equals("")) {
                log("<<ERROR>> There is no user name");
                printState(getResources().getText(R.string.no_user_name));
                return;
            }

            int tmp_type_idx = (int) ((Spinner) findViewById(R.id.spinnerTemplateType)).getSelectedItemId();
            int tmp_type;
            IBioMiniDevice.TemplateType _type;
            switch (tmp_type_idx) {
                case 2:
                    tmp_type = IBioMiniDevice.TemplateType.ANSI378.value();
                    _type = IBioMiniDevice.TemplateType.ANSI378;
                    break;
                case 0:
                    tmp_type = IBioMiniDevice.TemplateType.SUPREMA.value();
                    _type = IBioMiniDevice.TemplateType.SUPREMA;
                    break;
                case 1:
                    tmp_type = IBioMiniDevice.TemplateType.ISO19794_2.value();
                    _type = IBioMiniDevice.TemplateType.ISO19794_2;
                    break;
                default:
                    tmp_type = IBioMiniDevice.TemplateType.SUPREMA.value();
                    _type = IBioMiniDevice.TemplateType.SUPREMA;
                    break;
            }
            mCurrentDevice.setParameter(new IBioMiniDevice.Parameter(IBioMiniDevice.ParameterType.TEMPLATE_TYPE, tmp_type));
            IBioMiniDevice.TemplateData tmp = mCurrentDevice.extractTemplate();
            if (tmp == null) {
                log("<<ERROR>> Cannot get Template buffer");
                printState(getResources().getText(R.string.export_template_fail));
                return;
            }
            if (tmp.data != null) {
                try {
                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + userName + "_(" + _type.toString() + ")_.tmp");
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(tmp.data);
                    fos.close();
                    printState(getResources().getText(R.string.export_template_ok));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
