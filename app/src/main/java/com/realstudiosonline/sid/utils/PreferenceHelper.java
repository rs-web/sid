package com.realstudiosonline.sid.utils;

import android.content.Context;
import android.preference.PreferenceManager;

@SuppressWarnings("unused")
public class PreferenceHelper {

    private static Context appContext;
    private static PreferenceHelper preferenceHelperInstance;

    public static PreferenceHelper getPreferenceHelperInstace()
    {
        if (preferenceHelperInstance == null) {
            preferenceHelperInstance = new PreferenceHelper();
        }
        return preferenceHelperInstance;
    }

    public boolean getBoolean(Context context, String key, Boolean value)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, value);
    }

    public int getInteger(Context context, String key, int value)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(key, value);
    }

    public float getString(Context context, String key, float value)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).getFloat(key, value);
    }

    public String getString(Context context, String key, String value)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(key, value);
    }

    public void setBoolean(Context context, String key, Boolean value)
    {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(key, value).apply();
    }

    public void setFloat(Context context, String key, float value)
    {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putFloat(key, value).apply();
    }

    public void setInteger(Context context, String key, int value)
    {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(key, value).apply();
    }

    public void setString(Context context, String key, String value)
    {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(key, value).apply();
    }
}

