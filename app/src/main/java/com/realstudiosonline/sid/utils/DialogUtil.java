package com.realstudiosonline.sid.utils;

import android.app.Activity;
import android.view.Window;

import androidx.appcompat.app.AlertDialog;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.realstudiosonline.sid.R;

public class DialogUtil {
    public static void showDialog(Activity activity, String msg){


        AlertDialog alertDialog;
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity,R.style.MaterialAlertDialog_rounded);
        builder.setView(R.layout.alert_dialog);
        alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();
        //if (alertDialog.getWindow()!=null)
        //alertDialog.getWindow().setLayout(650, 800);

        /*final Dialog dialog = new Dialog(activity,R.style.my_dialog);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.error_dialog);
        MaterialTextView text = (MaterialTextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(v -> dialog.dismiss());
        dialog.show();*/

    }
}
