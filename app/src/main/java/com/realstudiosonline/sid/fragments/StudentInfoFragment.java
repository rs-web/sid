package com.realstudiosonline.sid.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.adapter.StudentsDropDownAdapter;
import com.realstudiosonline.sid.models.DrawerSelectedEvent;
import com.realstudiosonline.sid.models.GeneratedSuccess;
import com.realstudiosonline.sid.models.MessageEvent;
import com.realstudiosonline.sid.models.School;
import com.realstudiosonline.sid.models.SessionLog;
import com.realstudiosonline.sid.models.StudentIDCard;
import com.realstudiosonline.sid.models.StudentInfo;
import com.realstudiosonline.sid.models.StudentInfoSelected;
import com.realstudiosonline.sid.models.UploadStatus;
import com.realstudiosonline.sid.room.StudentInfoViewModel;
import com.realstudiosonline.sid.utils.KeyBoard;
import com.realstudiosonline.sid.utils.PreferenceHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class StudentInfoFragment extends Fragment  {

    private static final String TAG = "StudentInfoFragment";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    String mParam1;
    String mParam2;
    String mDOB ="";
    ScrollView scrollView;
    StudentsDropDownAdapter studentsDropDownAdapter;
    List<StudentInfo> studentInfoList;
    StudentInfoViewModel mStudentViewModel;
    TextInputLayout inputLayoutAddStudent, inputLayoutFullName, inputLayoutGuardianContact,
            inputLayoutSpinnerGender, inputLayoutSpinnerYear, inputLayoutSpinnerMonth, inputLayoutSpinnerDay;
    View viewAddStudent;
    boolean isNewStudent = false;
    BottomSheetDialog bottomSheetDialog;
   // GenderDropDownAdapter genderDropDownAdapter;
    AppCompatImageView imageViewClose;
    AlertDialog alertDialogSession;
    private String genders[] = {
            "Male",
            "Female",
    };
   // ArrayList<Gender> genders = new ArrayList<>(List.of(new Gender("Male", 1), new Gender("Female", 2)));

    private String months[] = {
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "June",
            "July",
            "Aug",
            "Sept",
            "Oct",
            "Nov",
            "Dec"

    };
    private TextInputEditText edtGuardianContact;
    private String mFullName, mGuardianContact = " ";
    private String mGender;
    private AppCompatTextView mNextButton;
    private String mYear, mDay;
    private int mMonth;
    private static final long LIMIT = 10000000000L;
    private static long last = 0;
    StudentIDCard mStudentIDCard;
    StudentInfo mStudentInfo;
    AutoCompleteTextView autoCompleteTextViewYear, autoCompleteTextViewMonth, autoCompleteTextViewDay,
            autoCompleteTextViewGender, autoCompleteTextViewStudent;
    TextInputEditText editTextFullName;
   // AppCompatTextView tvGuardianName;
    private View view_student_info;

    public StudentInfoFragment() {
        // Required empty public constructor
    }

    public static StudentInfoFragment newInstance(String param1, String param2) {
        StudentInfoFragment fragment = new StudentInfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_student_info, container, false);

        view_student_info  = view.findViewById(R.id.view_student_info);

        inputLayoutGuardianContact = view.findViewById(R.id.input_guardian_contact_no);

        inputLayoutFullName = view.findViewById(R.id.input_full_name);

        inputLayoutSpinnerYear = view.findViewById(R.id.spinner_input_year);

        inputLayoutSpinnerMonth = view.findViewById(R.id.spinner_input_month);

        inputLayoutSpinnerDay = view.findViewById(R.id.spinner_input_day);

        inputLayoutSpinnerGender = view.findViewById(R.id.spinner_input_gender);

        editTextFullName = view.findViewById(R.id.edt_add_new_student);

        viewAddStudent = view.findViewById(R.id.view_add_new_student);

        inputLayoutAddStudent = view.findViewById(R.id.input_add_new_student);

        imageViewClose = view.findViewById(R.id.img_add_new_back_press);

        imageViewClose.setOnClickListener(v -> {
            imageViewClose.setVisibility(View.GONE);
            viewAddStudent.setVisibility(View.VISIBLE);
            inputLayoutAddStudent.setVisibility(View.GONE);
            inputLayoutFullName.setVisibility(View.VISIBLE);
        });

        //Add New Student
        viewAddStudent.setOnClickListener(v -> {

            isNewStudent = true;

            inputLayoutAddStudent.setVisibility(View.VISIBLE);

            inputLayoutFullName.setVisibility(View.GONE);

            viewAddStudent.setVisibility(View.GONE);

            imageViewClose.setVisibility(View.VISIBLE);

            StudentInfo studentInfo = new StudentInfo();


                Date now = new Date();
                int id = Integer.parseInt(new SimpleDateFormat("ddHHmmss",  Locale.US).format(now));

            Gson gson = new Gson();
            String selectedSchoolJson = PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(),"selectedSchool", "");
            School selectedSchool = gson.fromJson(selectedSchoolJson, School.class);

            studentInfo.setId(id);
            studentInfo.setSchool_code(selectedSchool.getSchool_code());
            studentInfo.setSchool(selectedSchool.getSchool_name());
            studentInfo.setRegion(selectedSchool.getRegion());

        });

       /* editTextFullName.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {


                    StudentInfo studentInfo = new StudentInfo();


                    Date now = new Date();
                    int id = Integer.parseInt(new SimpleDateFormat("ddHHmmss",  Locale.US).format(now));

                    Gson gson = new Gson();
                    String selectedSchoolJson = PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(),"selectedSchool", "");
                    School selectedSchool = gson.fromJson(selectedSchoolJson, School.class);


                    studentInfo.setId(id);
                    studentInfo.setSchool_code(selectedSchool.getSchool_code());
                    studentInfo.setSchool(selectedSchool.getSchool_name());
                    studentInfo.setRegion(selectedSchool.getRegion());

                    //do here your stuff f
                    return true;
                }
                return false;
            }
        });*/

        autoCompleteTextViewMonth =  view.findViewById(R.id.spinner_month);
        autoCompleteTextViewMonth.setKeyListener(null);

        autoCompleteTextViewYear =  view.findViewById(R.id.spinner_year);
        autoCompleteTextViewYear.setKeyListener(null);
       // tvGuardianName = view.findViewById(R.id.edt_guardian_contact_no);

        autoCompleteTextViewStudent = view.findViewById(R.id.edt_full_name);

        autoCompleteTextViewDay = view.findViewById(R.id.spinner_day);
        autoCompleteTextViewDay.setInputType(InputType.TYPE_NULL);
        autoCompleteTextViewDay.setKeyListener(null);
        scrollView = view.findViewById(R.id.student_info_layout);
        autoCompleteTextViewStudent.setEnabled(true);
        return  view;
    }


    public static String getID() {
        // 10 digits.
        long id = System.currentTimeMillis() % LIMIT;
        if ( id <= last ) {
            id = (last + 1) % LIMIT;
        }
        last = id;
        return Long.toString(last);

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEditStudentCard(StudentIDCard studentIDCard){
        studentIDCard.setLeftForeFinger(null);
        studentIDCard.setRightForeFinger(null);
        studentIDCard.setLeftThumb(null);
        studentIDCard.setRightThumb(null);
        for (StudentInfo studentInfo: studentInfoList) {

            if(studentInfo.getShs_number().equals(studentIDCard.getShs_number())){
                mStudentInfo = studentInfo;
            }
        }



       // Toast.makeText(getActivity(), "onEditStudentCard", Toast.LENGTH_SHORT).show();
        mStudentIDCard = studentIDCard;

        if(getActivity()!=null){
            if(studentIDCard!=null) {

                String dob = studentIDCard.getDob();

                autoCompleteTextViewStudent.setText(studentIDCard.getFullName(),false);

               // autoCompleteTextViewStudent.setListSelection(studentInfoList.indexOf());
                //autoCompleteTextViewStudent.setEnabled(false);
                DateTime mDOBDateTime = DateTime.parse(dob, DateTimeFormat.forPattern("dd/MM/yyyy"));
                String year = String.valueOf(mDOBDateTime.getYear());
                String day = String.valueOf(mDOBDateTime.getDayOfMonth());
                int monthIndex = mDOBDateTime.getMonthOfYear()-1;
                String month = months[monthIndex];
                edtGuardianContact.setText(studentIDCard.getGuardianContact());

                //edtFullName.setText(studentIDCard.getFullName());
                autoCompleteTextViewGender.setText(studentIDCard.getGender(), false);

                autoCompleteTextViewYear.setText(year, false);

                autoCompleteTextViewMonth.setText(month,false);

                autoCompleteTextViewDay.setText(day,false);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        mStudentViewModel = new ViewModelProvider(this).get(StudentInfoViewModel.class);

        studentInfoList = new ArrayList<>();

        if(getActivity()!=null)
        createSessionAlertDialog(getActivity());

        String selectedSchoolJSon = PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(),"selectedSchool", null);
        Gson gson = new Gson();

        School school = gson.fromJson(selectedSchoolJSon, School.class);

        Log.d("selectedSchool",school.getGender());

        edtGuardianContact = view.findViewById(R.id.edt_guardian_contact_no);

        edtGuardianContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mGuardianContact = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        mNextButton = view.findViewById(R.id.btn_submit);

        view.findViewById(R.id.drawer_menu).setOnClickListener(v ->{

            EventBus.getDefault().post(new DrawerSelectedEvent(1, true));
        });

        mNextButton.setOnClickListener(v -> {


            inputLayoutAddStudent.setErrorEnabled(false);
            inputLayoutFullName.setErrorEnabled(false);
            inputLayoutSpinnerYear.setErrorEnabled(false);
            inputLayoutSpinnerMonth.setErrorEnabled(false);
            inputLayoutSpinnerDay.setErrorEnabled(false);
            inputLayoutGuardianContact.setErrorEnabled(false);
            inputLayoutSpinnerGender.setErrorEnabled(false);



            DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");

            try {

                if(isNewStudent){
                    if(editTextFullName.getText()!=null)
                    mFullName = editTextFullName.getText().toString();
                    mStudentInfo = new StudentInfo();

                    Date now = new Date();
                    int id = Integer.parseInt(new SimpleDateFormat("ddHHmmss",  Locale.US).format(now));

                    String selectedSchoolJson = PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(),"selectedSchool", "");
                    School selectedSchool = gson.fromJson(selectedSchoolJson, School.class);
                    mStudentInfo.setId(id);
                    mStudentInfo.setSchool_code(selectedSchool.getSchool_code());
                    mStudentInfo.setSchool(selectedSchool.getSchool_name());
                    mStudentInfo.setRegion(selectedSchool.getRegion());

                }
                else {


                    if(mStudentInfo.getEdited_name()!=null){
                        if(mStudentInfo.getEdited_name().length()>0){
                            mFullName = mStudentInfo.getEdited_name();
                            Log.d("getEdited_name", mStudentInfo.getEdited_name());
                        }
                    }
                    else {
                        mFullName = mStudentInfo.getStudent_name();
                    }

                   /* mStudentViewModel.getStudentByID(school.getSchool_code(), mStudentInfo.getId())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<StudentInfo>() {
                        @Override
                        public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull StudentInfo studentInfo) {
                            if(studentInfo.getEdited_name()!=null){
                                if(studentInfo.getEdited_name().length()>0){
                                    mFullName = studentInfo.getEdited_name();
                                    Log.d("getEdited_name", mStudentInfo.getEdited_name());
                                }
                            }
                            else {
                                mFullName = mStudentInfo.getStudent_name();
                            }
                        }

                        @Override
                        public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

                        }
                    });*/



                }
               // mSecondAndOtherNames = edtSecondAndOtherNames.getText().toString();
                mGuardianContact = edtGuardianContact.getText().toString();
                mYear = autoCompleteTextViewYear.getText().toString();
                mGuardianContact = edtGuardianContact.getText().toString();


                for (int i =0; i<months.length; i++) {
                    String m = months[i];
                    if(m.equals(autoCompleteTextViewMonth.getText().toString())){
                        mMonth = i+1;
                    }
                }

                for (int i =0; i<genders.length; i++) {
                    String m = genders[i];
                    if(m.equals(autoCompleteTextViewGender.getText().toString())){
                        mGender = m;
                    }
                }

                mDay = autoCompleteTextViewDay.getText().toString();


            }
            catch (NullPointerException e){

              e.printStackTrace();
            }
            finally {
                  if(TextUtils.isEmpty(autoCompleteTextViewYear.getText())) {
                    inputLayoutSpinnerYear.setError("Select a year to proceed");
                    inputLayoutSpinnerYear.setErrorEnabled(true);
                }
                if(TextUtils.isEmpty(autoCompleteTextViewMonth.getText())){
                    inputLayoutSpinnerMonth.setError("Select month to proceed");
                    inputLayoutSpinnerMonth.setErrorEnabled(true);
                }

                if(TextUtils.isEmpty(autoCompleteTextViewDay.getText())){

                    inputLayoutSpinnerDay.setError("Select day to proceed");
                    inputLayoutSpinnerDay.setErrorEnabled(true);
                }

                if(TextUtils.isEmpty(edtGuardianContact.getText())){

                    inputLayoutGuardianContact.setError("Enter guardian contact");
                    inputLayoutGuardianContact.setErrorEnabled(true);
                }
                if(TextUtils.isEmpty(autoCompleteTextViewGender.getText())){
                    inputLayoutSpinnerGender.setError("Select gender to proceed");
                    inputLayoutSpinnerGender.setErrorEnabled(true);
                }

                if(editTextFullName.getVisibility() == View.VISIBLE){
                    if(TextUtils.isEmpty(editTextFullName.getText())){

                        inputLayoutAddStudent.setError("Enter student's Name");
                        inputLayoutAddStudent.setErrorEnabled(true);
                    }
                }

                if(TextUtils.isEmpty(autoCompleteTextViewStudent.getText())){
                    inputLayoutFullName.setError("Select student to proceed");
                    inputLayoutFullName.setErrorEnabled(true);
                }
            }

            if(mMonth!=0 && mDay!=null && mYear!=null){

                if(mFullName.trim().length()<=26) {
                    if (autoCompleteTextViewDay.getText().toString().length() > 0 && autoCompleteTextViewYear.getText().toString().length() > 0) {
                        String dob = mDay + "/" + mMonth + "/" + mYear;

                        //TODO INVALID DATE
                        DateTime mDOBdateTime = DateTime.parse(dob, DateTimeFormat.forPattern("dd/MM/yyyy"));

                        String mformattedDOB = mDOBdateTime.toString(timeFormatter);

                        Log.d(TAG, "date of birth is " + mformattedDOB);
                        //boolean isDateOfBirthValid = isDateValid(mformattedDOB);

                        DateTime expirationDate = new DateTime();

                        DateTime issueDate = new DateTime();

                        expirationDate = expirationDate.plusYears(4);

                        Date now = new Date();
                        int id = Integer.parseInt(new SimpleDateFormat("ddHHmmss", Locale.US).format(now));

                        String expirationDateTime = expirationDate.toString(timeFormatter);
                        String issueDateTime = issueDate.toString(timeFormatter);

                        Log.d(TAG, "expiration date is " + expirationDateTime);
                        if (mFullName != null && mGender != null) {

                            if (mStudentIDCard == null) {
                                mStudentIDCard = new StudentIDCard();
                                mStudentIDCard.setId(UUID.randomUUID().toString());
                            }
                            mStudentIDCard.setStatus(new UploadStatus(UploadStatus.READY).uploadStatusType);
                            mStudentIDCard.setClassID(school.getClass_id());
                            mStudentIDCard.setDob(dob);
                            mStudentIDCard.setSchoolCode(school.getSchool_code());
                            mStudentIDCard.setFullName(mFullName);
                            mStudentIDCard.setGuardianContact(mGuardianContact);
                            if (mStudentInfo != null) {
                                mStudentIDCard.setIndex_number_jhs(mStudentInfo.getIndex_number_jhs());
                                mStudentIDCard.setStudentID(mStudentInfo.getShs_number());
                                mStudentIDCard.setShs_number(mStudentInfo.getShs_number());
                            }
                            mStudentIDCard.setIssueDate(issueDateTime);
                            mStudentIDCard.setSchoolID(String.valueOf(school.getSchool_id()));
                            mStudentIDCard.setSchoolName(school.getSchool_name());

                            mStudentIDCard.setExpirationDate(expirationDateTime);
                            //  mStudentIDCard.setOtherNames(mSecondAndOtherNames);
                            mStudentIDCard.setGender(mGender);
                            String studentIDCardJson = gson.toJson(mStudentIDCard);

                            Log.d("mStudentIDCard", "STUDENT: " + studentIDCardJson);
                            PreferenceHelper.getPreferenceHelperInstace().setString(getActivity(), "student_id", studentIDCardJson);

                            // edtSecondAndOtherNames.setText("");
                            //edtFullName.setText("");
                            edtGuardianContact.setText("");
                            autoCompleteTextViewYear.setText("");
                            autoCompleteTextViewDay.setText("");
                            autoCompleteTextViewMonth.setText("");
                            autoCompleteTextViewGender.clearListSelection();
                            autoCompleteTextViewGender.setText("");
                            autoCompleteTextViewMonth.clearListSelection();
                            autoCompleteTextViewStudent.clearListSelection();
                            autoCompleteTextViewStudent.setText("");
                            autoCompleteTextViewDay.clearListSelection();
                            autoCompleteTextViewYear.clearListSelection();

                            isNewStudent = false;

                            imageViewClose.setVisibility(View.GONE);
                            viewAddStudent.setVisibility(View.VISIBLE);
                            inputLayoutAddStudent.setVisibility(View.GONE);
                            inputLayoutFullName.setVisibility(View.VISIBLE);

                            String currentSessionJson = PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(), "currentSession", null);

                            if (currentSessionJson != null) {
                                SessionLog sessionLog = new Gson().fromJson(currentSessionJson, SessionLog.class);
                                if (sessionLog.sessionType.equalsIgnoreCase(SessionLog.PAUSED)) {
                                    //TODO implement Session dialog

                                    if (alertDialogSession == null) {
                                        if (getActivity() != null)
                                            createSessionAlertDialog(getActivity());
                                    }
                                    showSessionAlertDialog();
                                } else {
                                    EventBus.getDefault().post(new MessageEvent(true, "Photo & Bio", 2));
                                }
                            } else {
                                EventBus.getDefault().post(new MessageEvent(true, "Photo & Bio", 2));
                            }

                        } else {
                            Toast.makeText(getActivity(), "Enter valid info to proceed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                else {
                    //TODO IMPLEMENT NAME TOO LONG DIALOG WITH OPTION TO EDIT NAME AND SAVE
                    Toast.makeText(getActivity(), "NAME TOO LONG", Toast.LENGTH_LONG).show();
                    if(getActivity()!=null) {
                        if(!isNewStudent)
                        showEditStudentDialog(getActivity());
                        else {
                            Toast.makeText(getActivity(), "Name too long. Edit student's name", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            else {
                if(TextUtils.isEmpty(autoCompleteTextViewYear.getText())) {
                    inputLayoutSpinnerYear.setError("Select a year to proceed");
                    inputLayoutSpinnerYear.setErrorEnabled(true);
                }
                if(TextUtils.isEmpty(autoCompleteTextViewMonth.getText())){

                    inputLayoutSpinnerMonth.setError("Select month to proceed");
                    inputLayoutSpinnerMonth.setErrorEnabled(true);
                }

                if(TextUtils.isEmpty(autoCompleteTextViewDay.getText())){

                    inputLayoutSpinnerDay.setError("Select day to proceed");
                    inputLayoutSpinnerDay.setErrorEnabled(true);
                }

                if(TextUtils.isEmpty(edtGuardianContact.getText())){
                    inputLayoutGuardianContact.setError("Enter guardian contact");
                    inputLayoutGuardianContact.setErrorEnabled(true);
                }
                if(TextUtils.isEmpty(autoCompleteTextViewGender.getText())){
                    inputLayoutSpinnerGender.setError("Select gender to proceed");
                    inputLayoutSpinnerGender.setErrorEnabled(true);
                }

                if(editTextFullName.getVisibility() == View.VISIBLE){
                    if(TextUtils.isEmpty(editTextFullName.getText())){
                       // Toast.makeText(getContext(), "enter student's name", Toast.LENGTH_SHORT).show();
                        inputLayoutAddStudent.setError("Enter student's Name");
                        inputLayoutAddStudent.setErrorEnabled(true);
                    }
                }

                if(inputLayoutFullName.getVisibility()== View.VISIBLE){
                     if(TextUtils.isEmpty(autoCompleteTextViewStudent.getText())){
                 //   Toast.makeText(getContext(), "select student ", Toast.LENGTH_SHORT).show();
                    inputLayoutFullName.setError("Select student to proceed");
                    inputLayoutFullName.setErrorEnabled(true);
                }
                }

            }
        });

        //gender
        if(getActivity()!=null) {



            autoCompleteTextViewGender = (AutoCompleteTextView) view.findViewById(R.id.my_spinner_gender);
            ArrayAdapter<String> genderDropDownAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, genders);


            autoCompleteTextViewGender.setTextColor(getResources().getColor(R.color.colorHintTextColor));
            autoCompleteTextViewGender.setAdapter(genderDropDownAdapter);
            autoCompleteTextViewGender.setThreshold(1);

            autoCompleteTextViewGender.setOnFocusChangeListener((v, hasFocus) -> {
                if (hasFocus) {
                    autoCompleteTextViewGender.showDropDown();
                    KeyBoard.hide(getActivity(),view_student_info);
                }
            });

            autoCompleteTextViewGender.setOnTouchListener((v, event) -> {
                autoCompleteTextViewGender.showDropDown();
                return true;
            });

            autoCompleteTextViewGender.setOnItemClickListener((parent, view12, position, id) -> {
                mGender = genderDropDownAdapter.getItem(position);
                if(getActivity()!=null ) {
                    autoCompleteTextViewGender.clearFocus();
                    edtGuardianContact.clearFocus();
                    KeyBoard.hide(getActivity(),view_student_info);


                }

            });


            new Handler(Looper.getMainLooper()).postDelayed(()->{
                if(school.getGender().equalsIgnoreCase("girls")){
                    mGender = genders[1];
                    autoCompleteTextViewGender.setText(autoCompleteTextViewGender.getAdapter().getItem(1).toString(), false);
                    autoCompleteTextViewGender.setDropDownHeight(0);
                    autoCompleteTextViewGender.setEnabled(false);

                }

                else if(school.getGender().equalsIgnoreCase("boys")){
                    mGender = genders[0];
                    autoCompleteTextViewGender.setText(autoCompleteTextViewGender.getAdapter().getItem(0).toString(), false);
                    autoCompleteTextViewGender.setDropDownHeight(0);
                    autoCompleteTextViewGender.setEnabled(false);
                }

            },300);

        /*    switch (school.getGender()){

                case "girls":
                    autoCompleteTextViewGender.setSelection(1);
                    autoCompleteTextViewGender.setEnabled(false);
                    break;

                case "boys":
                    autoCompleteTextViewGender.setSelection(0);
                    autoCompleteTextViewGender.setEnabled(false);
                    break;

                default:
                    autoCompleteTextViewGender.setEnabled(true);
                    break;
            }*/
/*
            actvGender.setOnFocusChangeListener((v, hasFocus) -> {
                if (hasFocus)
                    actvGender.showDropDown();

            });
            actvGender.setOnTouchListener((v, event) -> {
                actvGender.showDropDown();
                return false;
            });*/


        }



        //full name
        if(getActivity()!=null){

            studentsDropDownAdapter = new StudentsDropDownAdapter(getActivity(), studentInfoList);

            autoCompleteTextViewStudent.setOnFocusChangeListener((v, hasFocus) -> {
                if (hasFocus) {
                    autoCompleteTextViewStudent.showDropDown();
                     KeyBoard.hide(getActivity(),view_student_info);
                }
            });

            autoCompleteTextViewStudent.setOnTouchListener((v, event) -> {
                autoCompleteTextViewStudent.showDropDown();
                return false;
            });

            autoCompleteTextViewStudent.setTextColor(getResources().getColor(R.color.colorHintTextColor));
            autoCompleteTextViewStudent.setAdapter(studentsDropDownAdapter);

            autoCompleteTextViewStudent.setThreshold(1);

            autoCompleteTextViewStudent.setOnItemClickListener((parent, view12, position, id) ->{

                if(getActivity()!=null)
                    KeyBoard.hide(getActivity(),view_student_info);

                if(studentsDropDownAdapter.getItem(position)!=null){


                    //inputSpinnerSchoolName.setErrorEnabled(false);
                    mStudentInfo = studentsDropDownAdapter.getItem(position);

                  //  Toast.makeText(getActivity(), "Student SHS:" +mStudentInfo.getShs_number(), Toast.LENGTH_SHORT).show();
                    edtGuardianContact.setText(mStudentInfo.getGuardian_mobile());

                    if(mStudentInfo.getDate_of_birth().length()<=0){
                        setYears();
                        setMonths();
                        autoCompleteTextViewDay.setText("");
                    }
                    try {
                        //12:00:00 AM
                        DateTime dateTime = DateTime.parse(mStudentInfo.getDate_of_birth(), DateTimeFormat.forPattern("MM/dd/yyyy hh:mm:ss a"));
                        autoCompleteTextViewYear.setText(dateTime.getYear()+"");
                        autoCompleteTextViewMonth.setText(months[dateTime.getMonthOfYear()-1]);
                        autoCompleteTextViewDay.setText(dateTime.getDayOfMonth()+"");
                        Log.d("DateTime", ""+dateTime.toString());
                    }
                    catch (Exception e){
                        autoCompleteTextViewYear.setText("");
                        autoCompleteTextViewMonth.setText("");
                        autoCompleteTextViewDay.setText("");
                        e.printStackTrace();

                    }

                }
            } );

          /*  DateTime now  = DateTime.now();
            DateTime d2 = new DateTime(2010,1,1,0,0);
            Period period = new Period(now, d2, PeriodType.years());
            int differenceMinutes = period.getYears();*/

            if(school!=null){
                Log.d("getStudentBySchoolID", "School code:"+school.getSchool_code());
                mStudentViewModel.getStudentsBySchoolID(school.getSchool_code()).observe(getViewLifecycleOwner(), studentInfos -> {
                    Log.d("studentInfos", "size() :"+studentInfos.size());
                    studentInfoList.clear();
                    studentInfoList.addAll(studentInfos);
                    studentsDropDownAdapter.notifyDataSetChanged();
                });
            }
        }

        //year adapter
        setYears();


        //month adapter
        setMonths();

    }

    private void setYears(){
        if(getActivity()!=null) {

            int currentYear = Calendar.getInstance().get(Calendar.YEAR);

            int maxAge = 51;
            int minAge = 14;
            String[] years_array = new String[maxAge-minAge];

            for(int i=0; i < maxAge-minAge; i++)
                years_array[i] = ""+ (currentYear - minAge - i);


            ArrayAdapter<String> yearAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_item, years_array);
            autoCompleteTextViewYear.setAdapter(yearAdapter);
            autoCompleteTextViewYear.setThreshold(1);

            autoCompleteTextViewDay.setDropDownHeight(0);
            autoCompleteTextViewMonth.setDropDownHeight(0);

            yearAdapter.notifyDataSetChanged();


            autoCompleteTextViewYear.setOnFocusChangeListener((v, hasFocus) -> {
                if (hasFocus) {
                    autoCompleteTextViewYear.showDropDown();
                    KeyBoard.hide(getActivity(),view_student_info);
                }

            });
           /* autoCompleteTextViewYear.setOnTouchListener((v, event) -> {
                autoCompleteTextViewYear.showDropDown();
                KeyBoard.hide(getActivity());
                return true;
            });*/
            autoCompleteTextViewYear.setTextColor(getResources().getColor(R.color.colorHintTextColor));

            autoCompleteTextViewYear.setOnItemClickListener((parent, view1, position, id) -> {

                if(getActivity()!=null) {

                    //autoCompleteTextViewStudent.clearFocus();
                    //autoCompleteTextViewStudent.setCursorVisible(false);
                    edtGuardianContact.clearFocus();
                    KeyBoard.hide(getActivity(),view_student_info);
                    mYear = years_array[position];
                    autoCompleteTextViewDay.clearListSelection();
                    autoCompleteTextViewDay.setText("");
                    autoCompleteTextViewDay.clearFocus();
                    autoCompleteTextViewMonth.setDropDownHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                }
            });
        }
    }

    private void setMonths(){
        if(getActivity()!=null) {
            ArrayAdapter<String> monthAdapter = new ArrayAdapter<>
                    (getActivity(), android.R.layout.select_dialog_item, months);

            autoCompleteTextViewMonth.setAdapter(monthAdapter);
         //   autoCompleteTextViewMonth.setThreshold(1);

            autoCompleteTextViewMonth.setOnFocusChangeListener((v, hasFocus) -> {
                if (hasFocus) {
                    autoCompleteTextViewMonth.showDropDown();
                    KeyBoard.hide(getActivity(),view_student_info);
                }

            });
           /* autoCompleteTextViewMonth.setOnTouchListener((v, event) -> {
                autoCompleteTextViewMonth.showDropDown();
                return true;
            });*/
            autoCompleteTextViewMonth.setTextColor(getResources().getColor(R.color.colorHintTextColor));


            autoCompleteTextViewMonth.setOnItemClickListener((parent, view13, position, id) -> {

                if(getActivity()!=null){
                    // autoCompleteTextViewStudent.clearFocus();
                    edtGuardianContact.clearFocus();
                    //autoCompleteTextViewStudent.clearFocus();
                    // autoCompleteTextViewStudent.setCursorVisible(false);
                    edtGuardianContact.clearFocus();
                    // edtGuardianContact.setCursorVisible(false);
                    KeyBoard.hide(getActivity(),view_student_info);
                    autoCompleteTextViewDay.setDropDownHeight(ViewGroup.LayoutParams.WRAP_CONTENT);

                }
                mMonth = position+1;
                Log.d(TAG, "selectedMonth is "+ mMonth);
                Log.d(TAG, "mYear is "+ mYear);
                if(mYear!=null){
                    if(!mYear.isEmpty()){

                        setDays(mMonth, mYear);
                    }
                }
            });
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    public void setDays(int selectedMonth, String selectedYear) {
        autoCompleteTextViewDay.clearListSelection();
        autoCompleteTextViewDay.setText("");
        autoCompleteTextViewDay.clearFocus();
        int year = Integer.parseInt(selectedYear);

        Log.d(TAG, "selectedMonth is "+ selectedMonth);
        Log.d(TAG, "selectedYear is "+ selectedYear);

        Calendar mycal = new GregorianCalendar(year, selectedMonth-1, 1);

        // Get the number of days in that month
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);

        String[] days_array = new String[daysInMonth];

        for (int k = 0; k < daysInMonth; k++)
            days_array[k] = "" + (k + 1);

        if(getActivity()!=null){
            ArrayAdapter<String> daysAdapter = new ArrayAdapter<>
                    (getActivity(), android.R.layout.select_dialog_item, days_array);
            autoCompleteTextViewDay.setAdapter(daysAdapter);
            autoCompleteTextViewDay.setThreshold(1);
            autoCompleteTextViewDay.setOnFocusChangeListener((v, hasFocus) -> {
                if (hasFocus) {
                    autoCompleteTextViewDay.showDropDown();
                    KeyBoard.hide(getActivity(),view_student_info);
                }

            });
           /* autoCompleteTextViewDay.setOnTouchListener((v, event) -> {
                autoCompleteTextViewDay.showDropDown();
                return true;
            });*/
            autoCompleteTextViewDay.setTextColor(getResources().getColor(R.color.colorHintTextColor));

            autoCompleteTextViewDay.setOnItemClickListener((parent, view, position, id) -> {
                if(getActivity()!=null){

                    //autoCompleteTextViewStudent.clearFocus();
                    //autoCompleteTextViewStudent.setCursorVisible(false);
                    edtGuardianContact.clearFocus();
                    //autoCompleteTextViewStudent.setFocusable(false);
                   // autoCompleteTextViewStudent.clearFocus();
                   // autoCompleteTextViewStudent.setCursorVisible(false);
                    edtGuardianContact.clearFocus();
                    mDay = days_array[position];
                    KeyBoard.hide(getActivity(),view_student_info);
                    //scrollView.post(() -> scrollView.fullScroll(View.FOCUS_DOWN));

                }

            });

        }

    }

    public static boolean isDateValid(String date)
    {
        try {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (ParseException e) {
            Log.d(TAG, "ParseException: "+ e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void  GeneratedSuccess(GeneratedSuccess success){
        if(getActivity()!=null){
            mStudentIDCard = null;
            autoCompleteTextViewStudent.setEnabled(true);
            String selectedSchoolJSon = PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(),"selectedSchool", null);
            Gson gson = new Gson();

            School school = gson.fromJson(selectedSchoolJSon, School.class);


            new Handler(Looper.getMainLooper()).postDelayed(()->{
                if(school.getGender().equalsIgnoreCase("girls")){
                    mGender = genders[1];
                    autoCompleteTextViewGender.setText(autoCompleteTextViewGender.getAdapter().getItem(1).toString(), false);
                    autoCompleteTextViewGender.setDropDownHeight(0);
                    autoCompleteTextViewGender.setEnabled(false);

                }

                else if(school.getGender().equalsIgnoreCase("boys")){
                    mGender = genders[0];
                    autoCompleteTextViewGender.setText(autoCompleteTextViewGender.getAdapter().getItem(0).toString(), false);
                    autoCompleteTextViewGender.setDropDownHeight(0);
                    autoCompleteTextViewGender.setEnabled(false);
                }

            },300);

            Log.d("selectedSchool",school.getGender());
            mStudentViewModel.getStudentsBySchoolID(school.getSchool_code()).observe(getViewLifecycleOwner(), studentInfos -> {
                Log.d("studentInfos", "size() :"+studentInfos.size());
                studentInfoList.clear();
                studentInfoList.addAll(studentInfos);
                studentsDropDownAdapter.notifyDataSetChanged();
            });
        }

    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFragmentSelected(StudentInfoSelected event) {

        if(getActivity()!=null){
            autoCompleteTextViewStudent.setEnabled(true);
            String selectedSchoolJSon = PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(),"selectedSchool", null);
            Gson gson = new Gson();

            School school = gson.fromJson(selectedSchoolJSon, School.class);

            new Handler(Looper.getMainLooper()).postDelayed(()->{
                if(school.getGender().equalsIgnoreCase("girls")){
                    mGender = genders[1];
                    autoCompleteTextViewGender.setText(autoCompleteTextViewGender.getAdapter().getItem(1).toString(), false);
                    autoCompleteTextViewGender.setDropDownHeight(0);
                    autoCompleteTextViewGender.setEnabled(false);

                }

                else if(school.getGender().equalsIgnoreCase("boys")){
                    mGender = genders[0];
                    autoCompleteTextViewGender.setText(autoCompleteTextViewGender.getAdapter().getItem(0).toString(), false);
                    autoCompleteTextViewGender.setDropDownHeight(0);
                    autoCompleteTextViewGender.setEnabled(false);
                }

            },300);

            Log.d("selectedSchool",school.getGender());
            mStudentViewModel.getStudentsBySchoolID(school.getSchool_code()).observe(getViewLifecycleOwner(), studentInfos -> {
                Log.d("studentInfos", "size() :"+studentInfos.size());
                studentInfoList.clear();
                studentInfoList.addAll(studentInfos);
                studentsDropDownAdapter.notifyDataSetChanged();
            });
        }

        //Toast.makeText(getActivity(), "id is : "+event.getId(), Toast.LENGTH_SHORT).show();

    }



    void createSessionAlertDialog(Context context){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(context,
                R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.school_setup_dialog, null);
        MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);
        MaterialButton btnContinue  = alert_view.findViewById(R.id.btn_continue);
        if(btnContinue!=null) {
            btnContinue.setText("Okay");
            btnContinue.setOnClickListener(v -> {
                alertDialogSession.dismiss();
            });
        }
        tv_message.setText("Session is paused, unpause session from drawer menu to continue.");
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialogSession = (androidx.appcompat.app.AlertDialog) builder.create();
        alertDialogSession.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialogSession.getWindow()!=null)
            alertDialogSession.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    void showSessionAlertDialog(){
        if(alertDialogSession!=null)
        alertDialogSession.show();
    }



   private void createEditStudentDialog(Activity activity){
        bottomSheetDialog = new BottomSheetDialog(activity,R.style.MaterialAlertDialog_Alert);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.setDismissWithAnimation(true);
        bottomSheetDialog.getBehavior().setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
       if(bottomSheetDialog.getWindow()!=null)
           bottomSheetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
       bottomSheetDialog.setContentView(R.layout.edit_student_dialog);
       TextInputLayout inputLayoutEditStudent = bottomSheetDialog.findViewById(R.id.input_edit_student);
       TextInputEditText editStudent = bottomSheetDialog.findViewById(R.id.edt_edit_student);
       MaterialTextView  tvWordCount = bottomSheetDialog.findViewById(R.id.tv_word_count);
       AppCompatTextView btnCancel = bottomSheetDialog.findViewById(R.id.btn_cancel);
       AppCompatTextView btnConfirm = bottomSheetDialog.findViewById(R.id.btn_confirm);

       if(editStudent!=null){
           editStudent.addTextChangedListener(new TextWatcher() {
               @Override
               public void beforeTextChanged(CharSequence s, int start, int count, int after) {

               }

               @Override
               public void onTextChanged(CharSequence s, int start, int before, int count) {
                   if(s.toString().trim().length()<=26) {
                       if(inputLayoutEditStudent!=null){
                           inputLayoutEditStudent.setErrorEnabled(false);
                       }
                       String wordCount = s.length()+"/26";
                       if(tvWordCount!=null) {
                           tvWordCount.setTextColor(getResources().getColor(R.color.colorBlack));
                           tvWordCount.setText(wordCount);
                       }
                   }
                   else {
                       if(inputLayoutEditStudent!=null) {
                           inputLayoutEditStudent.setError("Name too long");
                           inputLayoutEditStudent.setErrorEnabled(true);

                           String wordCount = s.length()+"/26";
                           if(tvWordCount!=null) {
                               tvWordCount.setTextColor(getResources().getColor(R.color.colorRed));
                               tvWordCount.setText(wordCount);

                           }

                       }
                   }
               }

               @Override
               public void afterTextChanged(Editable s) {

               }
           });

           if(mStudentInfo!=null)
           editStudent.setText(mStudentInfo.getStudent_name());
       }

       if(btnConfirm!=null){
           btnConfirm.setOnClickListener(v -> {
               if(editStudent!=null) {
                   if (editStudent.getText() != null) {
                       mStudentViewModel.updateStudent(editStudent.getText().toString(), mStudentInfo.getId());
                       Toast.makeText(activity, "Name saved Successfully", Toast.LENGTH_LONG).show();
                       mFullName = editStudent.getText().toString();
                       mStudentInfo.setEdited_name(mFullName);
                       bottomSheetDialog.hide();
                   }
               }
           });
       }

       if(btnCancel!=null){
           btnCancel.setOnClickListener(v -> {
               if(editStudent!=null)
               editStudent.setText("");
               bottomSheetDialog.hide();
           });
       }

   }

   private void showEditStudentDialog(Activity activity){
        if(getActivity()!=null) {
            if(bottomSheetDialog==null)
                createEditStudentDialog(activity);
            bottomSheetDialog.show();
            if(bottomSheetDialog.getCurrentFocus()!=null)
            KeyBoard.hide(getActivity(),bottomSheetDialog.getCurrentFocus());
        }
   }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Override
    public void onDestroy() {
        if(alertDialogSession!=null)
            alertDialogSession.cancel();
        super.onDestroy();
    }
}