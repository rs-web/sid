package com.realstudiosonline.sid.fragments;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apollographql.apollo.api.Error;
import com.apollographql.apollo.api.Response;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.activities.IDCardViewActivity;
import com.realstudiosonline.sid.activities.ProjectOfficerSignInActivity;
import com.realstudiosonline.sid.activities.StudentInfoPhotoGenerateCardActivity;
import com.realstudiosonline.sid.graphqlserver.Server;
import com.realstudiosonline.sid.models.DrawerSelectedEvent;
import com.realstudiosonline.sid.models.MessageEvent;
import com.realstudiosonline.sid.models.ProjectOfficer;
import com.realstudiosonline.sid.models.ReviewCardSelected;
import com.realstudiosonline.sid.models.StudentIDCard;
import com.realstudiosonline.sid.room.StudentIDListAdapter;
import com.realstudiosonline.sid.room.StudentIDViewModel;
import com.realstudiosonline.sid.utils.PreferenceHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.observers.DisposableObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;

import static java.util.concurrent.Executors.newSingleThreadExecutor;


public class ReviewCardsFragment extends Fragment implements StudentIDListAdapter.setOnCardSelectedListener{

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    String mParam1;
    String mParam2;
    View drawerMenu;
    private RecyclerView recyclerView;
    private StudentIDViewModel mStudentViewModel;
    private View imgEmpty;
    private StudentIDListAdapter studentIDListAdapter;
    AppBarLayout appBarLayout;
    View submitLayout;
    View floatingActionButton;
    private final String TAG = "ReviewCardsFragment";
    List<StudentIDCard> studentIDCardList;
    private View view_review;
    private ProjectOfficer projectOfficer;
    //ProgressBar progressBar;
    public ReviewCardsFragment() {
        // Required empty public constructor
    }

    public static ReviewCardsFragment newInstance(String param1, String param2) {
        ReviewCardsFragment fragment = new ReviewCardsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_review_cards, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view_review = view.findViewById(R.id.view_review);

        //progressBar = view.findViewById(R.id.progressBar);

        CollapsingToolbarLayout collapsingToolbarLayout =view.findViewById(R.id.collapsing_toolbar);

        appBarLayout = view.findViewById(R.id.appbar);

        studentIDCardList = new ArrayList<>();

        floatingActionButton = view.findViewById(R.id.floating_action_submit);

        String projectOfficerJson = PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(), "projectOfficer", null);

        if(projectOfficerJson!=null){

            projectOfficer =  new Gson().fromJson(projectOfficerJson, ProjectOfficer.class);
        }

        floatingActionButton.setOnClickListener(v -> {

           /* submitLayout.startAnimation(AnimationUtils.loadAnimation(getActivity(),R.anim.left_to_right));

            submitLayout.postDelayed(()->{
                submitLayout.setVisibility(View.VISIBLE);
            },500);

            floatingActionButton.setVisibility(View.GONE);*/
            //revealSubmitButton();
        });

        submitLayout = view.findViewById(R.id.submit_layout);

        appBarLayout.setExpanded(true, true);

        view.findViewById(R.id.drawer_menu).setOnClickListener(v -> EventBus.getDefault().post(new DrawerSelectedEvent(1, true)));

        view.findViewById(R.id.review_sessions).setOnClickListener(v -> EventBus.getDefault().post(new MessageEvent(true, "Review Session",5)));

        view.findViewById(R.id.work_history).setOnClickListener(v -> EventBus.getDefault().post(new MessageEvent(true, "Work History",6)));

        drawerMenu = view.findViewById(R.id.title_bar);

        imgEmpty = view.findViewById(R.id.empty_view);

       // generatedCards = new ArrayList<>();

        recyclerView = view.findViewById(R.id.recycler_view_review_cards);
        //recyclerView.setHasFixedSize(true);
        studentIDListAdapter = new StudentIDListAdapter(new StudentIDListAdapter.StudentIDDiff(), this, getActivity());
       // ReviewCardsAdapter reviewCardsAdapter = new ReviewCardsAdapter(getGeneratedCards());

        if(getActivity()!=null){
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
          /*  ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(5,2);
            recyclerView.addItemDecoration(itemDecoration);*/
            recyclerView.setAdapter(studentIDListAdapter);
        }


        // Get a new or existing ViewModel from the ViewModelProvider.
        mStudentViewModel = new ViewModelProvider(this).get(StudentIDViewModel.class);

        // Add an observer on the LiveData returned by getAllStudentIDs.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        /* Update the cached copy of the student id card in the adapter. */

       /* new Handler().postDelayed(()-> mStudentViewModel.getAllStudentIDs().observe(getViewLifecycleOwner(), studentIDCards -> {
            Log.d("getAllStudentIDs", "size "+studentIDCards.size());
            if(studentIDCards.size() == 0){
                imgEmpty.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);

            }
            studentIDCardList.clear();
            studentIDCardList.addAll(studentIDCards);
            studentIDListAdapter.submitList(studentIDCards);
            progressBar.setVisibility(View.GONE);
        }),500);*/
    }





    private boolean internetConnectionAvailable(int timeOut) {
        InetAddress inetAddress = null;
        try {
            Future<InetAddress> future = newSingleThreadExecutor().submit(() -> {
                try {
                    return InetAddress.getByName("google.com");
                } catch (UnknownHostException e) {
                    return null;
                }
            });
            inetAddress = future.get(timeOut, TimeUnit.MILLISECONDS);
            future.cancel(true);
        }
        catch (InterruptedException | ExecutionException | TimeoutException e) {
        }
        return inetAddress!=null && !inetAddress.equals("");
    }

    public  boolean isNetworkConnected(Activity activity) {
        ConnectivityManager cm = (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
   public void onReviewCardSelected(ReviewCardSelected reviewCardSelected){

        if(getActivity()!=null){
           // Toast.makeText(getActivity(), "review cards", Toast.LENGTH_SHORT).show();
            if(appBarLayout!=null)
                appBarLayout.setExpanded(true, true);

            if(mStudentViewModel==null)
                mStudentViewModel = new ViewModelProvider(this).get(StudentIDViewModel.class);

            mStudentViewModel.getAllStudentIDs().observe(getViewLifecycleOwner(), studentIDCards -> {
             //   Toast.makeText(getActivity(), "student id "+ studentIDCards.size(), Toast.LENGTH_SHORT).show();
                if(studentIDCards.size() == 0){

                    imgEmpty.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);

                }
                else {
                    imgEmpty.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    studentIDListAdapter.submitList(studentIDCards);
                }
            });
        }

    }

    @Override
    public void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onCardSelected(StudentIDCard studentIDCard) {
        EventBus.getDefault().post(studentIDCard);
        EventBus.getDefault().post(new MessageEvent(false, "Student Info",1));
        //Toast.makeText(getActivity(), studentIDCard.getFullName(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCardLongPress(StudentIDCard studentIDCard) {
       // Toast.makeText(getActivity(), "onCardLongPress", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getActivity(), IDCardViewActivity.class);
        intent.putExtra("card_url", studentIDCard.getGeneratedCardURI());
        startActivity(intent);
    }


}
/*
*
    public void revealSubmitButton() {

        int cx = (floatingActionButton.getLeft() + floatingActionButton.getRight()) / 3;
        int cy = (floatingActionButton.getTop() + floatingActionButton.getBottom()) / 3;
        submitLayout.setBackgroundColor(Color.parseColor("#6FA6FF"));
        int finalRadius = Math.max(cy, submitLayout.getHeight() - cy);
        Animator anim = ViewAnimationUtils.createCircularReveal(submitLayout, cx, cy, 0, finalRadius);
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

                floatingActionButton.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        anim.setDuration(500);
        submitLayout.setVisibility(View.VISIBLE);
        anim.start();
    }

    public void revealFloatingButton() {

        int cx = (submitLayout.getLeft() + submitLayout.getRight()) / 3;
        int cy = (submitLayout.getTop() + submitLayout.getBottom()) / 3;
        floatingActionButton.setBackgroundColor(Color.parseColor("#6FA6FF"));
        int finalRadius = Math.max(cy, floatingActionButton.getHeight() - cy);
        Animator anim = ViewAnimationUtils.createCircularReveal(floatingActionButton, cx, cy, 0, finalRadius);
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

                submitLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        anim.setDuration(500);
        floatingActionButton.setVisibility(View.VISIBLE);
        anim.start();
    }*/