package com.realstudiosonline.sid.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.activities.StudentInfoPhotoGenerateCardActivity;
import com.realstudiosonline.sid.models.DrawerSelectedEvent;
import com.realstudiosonline.sid.models.GenerateCardSelected;
import com.realstudiosonline.sid.models.MessageEvent;
import com.realstudiosonline.sid.models.School;
import com.realstudiosonline.sid.models.SelectedFragment;
import com.realstudiosonline.sid.models.StudentIDCard;
import com.realstudiosonline.sid.room.StudentIDViewModel;
import com.realstudiosonline.sid.utils.PreferenceHelper;
import com.realstudiosonline.sid.utils.PxUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.EnumMap;
import java.util.Map;
import java.util.UUID;

import static com.realstudiosonline.sid.fragments.CameraXFragment.biometricFingerPrint;
import static com.realstudiosonline.sid.fragments.CameraXFragment.biometricFingerPrintBitmap;

public class GenerateCardFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private Bitmap mPassportSizedBitmap;
    String mParam1;
    String mParam2;
    private AppCompatImageView imgResult, imageViewSchoolCrest;
    private Bitmap qrImage;
    private Bitmap mBitmap;
    private final String TAG = "GenerateCardFragment";
    final int REQUEST_PERMISSION = 0xf0;
    private AppCompatImageView imgPassportSizedView; //mImgGeneratedCard;
    private  AppCompatTextView tvNameOfSchool,tvNameOfStudent,
            tvGuardianContact,tvExpirationDate, tvIssueDate, btnSubmit, tvStudentNo, tvGender;
    View mPrevious,mNextButton, mGenerateCard;
    private StudentIDViewModel mStudentViewModel;
    private StudentIDCard  studentIDCard;
    AlertDialog alertDialog;
    private boolean isSaved = false;
    BottomSheetDialog dialogLottie;
    LottieAnimationView lottieAnimationView;
    School school;
    public GenerateCardFragment() {
        // Required empty public constructor
    }


    public static GenerateCardFragment newInstance(String param1, String param2) {
        GenerateCardFragment fragment = new GenerateCardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_generate_card, container, false);
    }






    private Bitmap generateCard(View view){
        Bitmap b = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        view.draw(c);
        return b;
    }




    public  File saveCroppedImage(Context context, Bitmap image, String directoryName, String fileName){
        File imageFile =null;
        File storageDir = new File(context.getExternalFilesDir(
                Environment.DIRECTORY_PICTURES), directoryName);

        //   boolean isSaved = false;

        if (!storageDir.mkdirs()) {
            storageDir.mkdir();
        }

        FileOutputStream outStream = null;
        try {
            imageFile = new File(storageDir, fileName);
            outStream = new FileOutputStream(imageFile);
            image.compress(Bitmap.CompressFormat.PNG, 60, outStream);
            outStream.flush();
            outStream.close();
            isSaved = true;



            Log.d(TAG, "imageFile.getPath() is "+imageFile.getPath());
         /*   if(getActivity()!=null){
               // Toast.makeText(getActivity(), "Student ID Card generated successfully", Toast.LENGTH_LONG).show();

            }*/




        } catch (IOException e) {
            if(getActivity()!=null)
                Toast.makeText(getActivity(), "Error generating Student ID Card.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return imageFile;

    }



    public  boolean saveFileInAppDirectory(Context context, Bitmap image, String directoryName, String fileName, int position){
        File imageFile;
        File storageDir = new File(context.getExternalFilesDir(
                Environment.DIRECTORY_PICTURES), directoryName);

        //   boolean isSaved = false;

        if (!storageDir.mkdirs()) {
            storageDir.mkdir();
        }

        FileOutputStream outStream = null;
        try {
            imageFile = new File(storageDir, fileName);
            outStream = new FileOutputStream(imageFile);
            image.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
            isSaved = true;



            Log.d(TAG, "imageFile.getPath() is "+imageFile.getPath());
           /* if(getActivity()!=null){
                Toast.makeText(getActivity(), "Student ID Card generated successfully", Toast.LENGTH_LONG).show();

            }*/
            if(studentIDCard!=null){


                switch (position){
                    case 0:
                        studentIDCard.setRightThumbURl(imageFile.getPath());
                        break;

                    case 1:
                        studentIDCard.setRightForeFingerURl(imageFile.getPath());
                        break;

                    case 2:
                        studentIDCard.setLeftThumbURL(imageFile.getPath());
                        break;

                    case 3:
                        studentIDCard.setLeftForeFingerURL(imageFile.getPath());
                        break;
                }
            }


            new Handler(Looper.getMainLooper()).postDelayed(()->{
                if(isSaved){
                    if(getActivity()!=null){
                        isSaved = false;

                        String student = PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(),"student_id",null);
                        String url = "/storage/emulated/0/Android/data/com.realstudiosonline.studentidcard/files/Pictures/.StudentIDCards/IMG_1614905442801.png";
                        //  Uri uri = Uri.parse(url);
                        //  Log.d("IMAGEIRI","uri :"+getPath(uri, getActivity()));
                        // addStudent(studentIDCard);
                        if(getActivity()!=null) {
                            showBottomDialogAlert(getActivity());
                            //showDialog(getActivity());
                        }

                    }

                }
            },1000);
        } catch (IOException e) {
            if(getActivity()!=null)
                Toast.makeText(getActivity(), "Error generating Student ID Card.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return isSaved;

    }



    public  boolean saveFileInAppDirectory(Context context, Bitmap image, String directoryName, String fileName){
        File imageFile;
        File storageDir = new File(context.getExternalFilesDir(
                Environment.DIRECTORY_PICTURES), directoryName);

     //   boolean isSaved = false;

        if (!storageDir.mkdirs()) {
            storageDir.mkdir();
        }

        FileOutputStream outStream = null;
        try {
            imageFile = new File(storageDir, fileName);
            outStream = new FileOutputStream(imageFile);
            image.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
            isSaved = true;



            Log.d(TAG, "imageFile.getPath() is "+imageFile.getPath());
           /* if(getActivity()!=null){
                Toast.makeText(getActivity(), "Student ID Card generated successfully", Toast.LENGTH_LONG).show();

            }*/
            if(studentIDCard!=null){
                studentIDCard.setGeneratedCardURI(imageFile.getPath());


                for(int i=0; i<biometricFingerPrint.size(); i++){
                    byte [] bio = biometricFingerPrint.get(i);
                    switch (i){
                        case 0:
                            studentIDCard.setRightThumb(bio);
                            break;

                        case 1:
                            studentIDCard.setRightForeFinger(bio);
                            break;

                        case 2:
                            studentIDCard.setLeftThumb(bio);
                            break;

                            case 3:
                                studentIDCard.setLeftForeFinger(bio);
                            break;

                    }
                }
                biometricFingerPrint.clear();

            }


            new Handler(Looper.getMainLooper()).postDelayed(()->{
                if(isSaved){
                    if(getActivity()!=null){
                        isSaved = false;

                        String url = "/storage/emulated/0/Android/data/com.realstudiosonline.studentidcard/files/Pictures/.StudentIDCards/IMG_1614905442801.png";
                      //  Uri uri = Uri.parse(url);
                      //  Log.d("IMAGEIRI","uri :"+getPath(uri, getActivity()));
                      // addStudent(studentIDCard);
                        if(getActivity()!=null) {
                            showBottomDialogAlert(getActivity());
                           // showDialog(getActivity());
                        }

                    }

                }
            },1000);
        } catch (IOException e) {
            if(getActivity()!=null)
                Toast.makeText(getActivity(), "Error generating Student ID Card.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return isSaved;

    }


    public static boolean convertToPNG(Bitmap image) throws IOException {

        boolean isSaved = false;
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES+"/.StudentID");
        File imageFile = File.createTempFile(
                "IMG_"+System.currentTimeMillis(),  /* prefix */
                ".png",         /* suffix */
                storageDir      /* directory */
        );

        FileOutputStream outStream = null;
        try {
            outStream = new FileOutputStream(imageFile);
            image.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
              isSaved = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isSaved;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnSubmit = view.findViewById(R.id.btn_submit);

        imgResult = view.findViewById(R.id.img_qrcode);

        imageViewSchoolCrest = view.findViewById(R.id.img_school_crest);

        imgPassportSizedView = view.findViewById(R.id.img_passport_picture);

        tvGender = view.findViewById(R.id.tv_gender);

        mPrevious = view.findViewById(R.id.btn_back);



        if(getActivity()!=null){

            String schoolJson  = PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(),"selectedSchool", "");

            school = new Gson().fromJson(schoolJson, School.class);
        }

        if(getActivity()!=null) {
            createProgressDialog(getActivity());
            createBottomDialogAlert(getActivity());
        }
        mPrevious.setOnClickListener(v -> {

            EventBus.getDefault().post(new SelectedFragment(1, true));
            EventBus.getDefault().post(new MessageEvent(true, "Photo & Bio",2));
        });


       /* alertDialog = new SpotsDialog.Builder()
                .setContext(getActivity())
                .setMessage("Uploading Student ID Card....")
                .build();*/


       /* AppCompatImageView myImage = (AppCompatImageView)view.findViewById(R.id.img_generated_card);

        File imgFile = new  File(path);

        if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            myImage.setImageBitmap(myBitmap);

        }
        else Toast.makeText(getActivity(), "doest esi", Toast.LENGTH_SHORT).show();*/
       // loader = view.findViewById(R.id.loader);

        tvNameOfSchool = view.findViewById(R.id.tv_name_of_school);

        /*String nameOfSchool = "Name of School<br> <font color='#0C753D'><b>First boys senior high school</b></font>.";

        String nameOfStudent = "Name of Student<br> <font color='black'><b>KOFI APPIAH - KUMI</b></font>.";

        String expirationDate = "Expiration Date<br> <font color='black'><b>30/12/2024</b></font>.";

        String guardianContact = "Guardian's contact<br> <font color='black'><b>0201393112</b></font>.";

        String dateOfBirth = "Date of Birth<br> <font color='black'><b>30/04/2001</b></font>.";

*/
        tvNameOfStudent = view.findViewById(R.id.tv_name_of_student);


        //tvDateOfbirth = view.findViewById(R.id.tv_date_of_birth);

        tvStudentNo = view.findViewById(R.id.tv_id_no);


        tvGuardianContact = view.findViewById(R.id.tv_guardian_contact);


        tvIssueDate = view.findViewById(R.id.tv_issue_date);

        tvExpirationDate = view.findViewById(R.id.tv_expiration_date);

        mGenerateCard = view.findViewById(R.id.view_generate_card);

        view.findViewById(R.id.drawer_menu).setOnClickListener(v ->{

            EventBus.getDefault().post(new DrawerSelectedEvent(1, true));
        });



        btnSubmit.setOnClickListener(v -> {

            mBitmap = generateCard(mGenerateCard);
            //mImgGeneratedCard.setImageBitmap(mBitmap);
            String name = "IMG_"+System.currentTimeMillis()+".png";

            // isSaved = convertToPNG(mBitmap);
            if(getActivity()!=null) {

                for (int i = 0; i < biometricFingerPrintBitmap.size(); i++) {
                    Bitmap bitmap = biometricFingerPrintBitmap.get(i);
                    switch (i) {
                        case 0:
                            saveFileInAppDirectory(getActivity(), bitmap, ".Bio", "right_thumb_" + studentIDCard.getId()+".png", 0);
                            break;
                        case 1:
                            saveFileInAppDirectory(getActivity(), bitmap, ".Bio", "right_for_finger_" + studentIDCard.getId()+".png", 1);
                            break;
                        case 2:
                            saveFileInAppDirectory(getActivity(), bitmap, ".Bio", "left_thumb_" + studentIDCard.getId()+".png", 2);
                            break;
                        case 3:
                            saveFileInAppDirectory(getActivity(), bitmap, ".Bio", "left_for_finger_" + studentIDCard.getId()+".png", 3);
                            break;
                    }

                }
                biometricFingerPrintBitmap.clear();
                isSaved = saveFileInAppDirectory(getActivity(), mBitmap, ".StudentIDCards", name);


                if(studentIDCard!=null) {
                    mStudentViewModel.insert(studentIDCard);

                   /* mStudentViewModel.getStudent(studentIDCard.getSchoolID(), studentIDCard.getId())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<List<StudentIDCard>>() {
                        @Override
                        public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull List<StudentIDCard> studentIDCards) {
                            if(studentIDCards.size()==0){
                                mStudentViewModel.insert(studentIDCard);
                            }
                        }

                        @Override
                        public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                            mStudentViewModel.insert(studentIDCard);
                        }
                    });*/

                }
            }

        });

        ///mImgGeneratedCard = view.findViewById(R.id.img_generated_card);

        mNextButton = view.findViewById(R.id.btn_next);


        mNextButton.setOnClickListener(v -> {

        });



        // Get a new or existing ViewModel from the ViewModelProvider.
        mStudentViewModel = new ViewModelProvider(this).get(StudentIDViewModel.class);

        // Add an observer on the LiveData returned by getAllStudentIDs.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        mStudentViewModel.getAllStudentIDs().observe(getViewLifecycleOwner(), words -> {
            // Update the cached copy of the words in the adapter.
            //adapter.submitList(words);
        });
    }



    private void showLoadingVisible(boolean visible){



        if(getActivity()!=null){
            getActivity().runOnUiThread(() -> {
                if(visible){
                    showImage(null);
                }

          /*      loader.setVisibility(
                        (visible) ? View.VISIBLE : View.GONE
                );*/
            });
        }
    }



    void showBottomDialogAlert(Activity activity){
        if (dialogLottie == null) {
            createBottomDialogAlert(activity);
        }
        dialogLottie.show();
        lottieAnimationView = dialogLottie.findViewById(R.id.lottie_layer_name);
        if (lottieAnimationView != null) {
            lottieAnimationView.playAnimation();
        }

    }
    public void createBottomDialogAlert(Activity activity){
        dialogLottie = new BottomSheetDialog(activity);
        dialogLottie.setCancelable(true);
        dialogLottie.setContentView(R.layout.alert_dialog_success);
        dialogLottie.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog)dialog;
                FrameLayout frameLayout = (FrameLayout)d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                if(frameLayout!=null)
                BottomSheetBehavior.from(frameLayout).setState(BottomSheetBehavior.STATE_EXPANDED);


            }
        });

        AppCompatTextView btn_continue = dialogLottie.findViewById(R.id.btn_continue);
        if(btn_continue!=null){
            btn_continue.setOnClickListener(v -> {
                dialogLottie.cancel();
                EventBus.getDefault().post(new MessageEvent(false, "School setup",0));
                PreferenceHelper.getPreferenceHelperInstace().setString(getActivity(),"student_id","");
            });
        }

        AppCompatTextView btn_cancel = dialogLottie.findViewById(R.id.btn_cancel);
        if(btn_cancel!=null){
            btn_cancel.setOnClickListener(v -> {
                dialogLottie.cancel();
            });
        }

        MaterialTextView tv_message = dialogLottie.findViewById(R.id.tv_message);
        String text = "<b> Student ID Card generated successfully</b>";
        if(tv_message!=null)
        tv_message.setText(Html.fromHtml(text));

    }

   /* public void showDialog(Activity activity){


        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity,
                R.style.MyDialog);
        View alert_view = getLayoutInflater().inflate(R.layout.alert_dialog_success, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        AlertDialog dialog = (androidx.appcompat.app.AlertDialog) builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(dialog.getWindow()!=null) {
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
           // dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);


            WindowManager wm = (WindowManager)getActivity().getSystemService(Context.WINDOW_SERVICE);

            DisplayMetrics displaymetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int screenWidth = displaymetrics.widthPixels;
            int screenHeight = displaymetrics.heightPixels;

            WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
            params.width = WindowManager.LayoutParams.MATCH_PARENT;
            params.height = WindowManager.LayoutParams.WRAP_CONTENT;

            dialog.getWindow().setGravity(Gravity.BOTTOM);
        }

        MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);
        String text = "<b> Student ID Card generated successfully</b>";
        tv_message.setText(Html.fromHtml(text));


        LottieAnimationView lottieAnimationView = alert_view.findViewById(R.id.lottie_layer_name);


        lottieAnimationView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                animation.cancel();
                dialog.cancel();
                new Handler(Looper.getMainLooper()).postDelayed(()->{

                    EventBus.getDefault().post(new MessageEvent(false, "School setup",0));
                    PreferenceHelper.getPreferenceHelperInstace().setString(getActivity(),"student_id","");
                },500);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        lottieAnimationView.playAnimation();
        dialog.show();
    }
*/

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFragmentSelected(GenerateCardSelected event) {
      Toast.makeText(getActivity(), "onFragmentSelected", Toast.LENGTH_SHORT).show();


        btnSubmit.setEnabled(false);
        try {
            Gson gson = new Gson();
            String json = PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(), "student_id", "");
            studentIDCard = gson.fromJson(json, StudentIDCard.class);
            Log.d("studentIDCard", "studentIDCard: "+studentIDCard.toString());


            if(studentIDCard!=null && getActivity()!=null){
               // Toast.makeText(getActivity(), ""+studentIDCard.toString(), Toast.LENGTH_SHORT).show();

                String expiryDate = studentIDCard.getExpirationDate();


                DateTime mDOBdateTime = DateTime.parse(expiryDate, DateTimeFormat.forPattern("dd/MM/yyyy"));

                DateTime mIssueDate = DateTime.parse(studentIDCard.getIssueDate(), DateTimeFormat.forPattern("dd/MM/yyyy"));

                DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("MM/yyyy");


                String mExpiryDate = mDOBdateTime.toString(timeFormatter);

                String mIssueDateString = mIssueDate.toString(timeFormatter);

               // String nameOfSchool = "<font color='black'><b>ACCRA HIGH SCHOOL</b></font>";
                String nameOfSchool = studentIDCard.getSchoolName();

               // String nameOfStudent = "<font color='black'><b>" + studentIDCard.getFullName() +"</b></font>";
                String nameOfStudent = studentIDCard.getFullName();

                //String issueDate = "<font color='black'><b>" +mIssueDateString+ "</b></font>";
                String issueDate =  mIssueDateString;

               // String expirationDate = "<font color='black'><b>" +mExpiryDate+ "</b></font>";
                String expirationDate = mExpiryDate;


               // String guardianContact = "<font color='black'><b>0201393112</b></font>";
                String guardianContact = studentIDCard.getGuardianContact();


               // String dateOfBirth = "<font color='black'><b>" + studentIDCard.getDob()+"</b></font>";
                String dateOfBirth = studentIDCard.getDob();


                //String studentNo = "<font color='black'><b>" +" 0205481A002421"+"</b></font>";
                String studentNo = studentIDCard.getStudentID()==null ? "" : studentIDCard.getStudentID();

                tvNameOfSchool.setText(Html.fromHtml(nameOfSchool));


                tvNameOfStudent.setText(Html.fromHtml(nameOfStudent));

                Toast.makeText(getActivity(),"+gender:"+ studentIDCard.getGender(), Toast.LENGTH_SHORT).show();

                Log.d("studentIDCard", studentIDCard.toString());

                if(studentIDCard.getGender().equalsIgnoreCase("Male"))
                    tvGender.setText("M");

                else if(studentIDCard.getGender().equalsIgnoreCase("Female"))
                    tvGender.setText("F");


               // tvDateOfbirth.setText(Html.fromHtml(dateOfBirth));


                tvGuardianContact.setText(Html.fromHtml(guardianContact));

                tvStudentNo.setText(Html.fromHtml(studentNo));

               /* Gson gson = new Gson();
                String schoolJson = gson.toJson(selectedSchool);
                PreferenceHelper.getPreferenceHelperInstace().setString(
                        this,"selectedSchool", schoolJson);*/

                if(studentIDCard.getClassID().equalsIgnoreCase("1")){
                    tvExpirationDate.setText(Html.fromHtml(DateTime.now().plusYears(3).toString(timeFormatter)));
                }

                else if(studentIDCard.getClassID().equalsIgnoreCase("2")){
                    tvExpirationDate.setText(Html.fromHtml(DateTime.now().plusYears(2).toString(timeFormatter)));
                }


                else if(studentIDCard.getClassID().equalsIgnoreCase("3")){
                    tvExpirationDate.setText(Html.fromHtml(DateTime.now().plusYears(1).toString(timeFormatter)));
                }


                tvIssueDate.setText(Html.fromHtml(issueDate));

                final Handler handler = new Handler(Looper.getMainLooper());
                // Loads given image
                mPassportSizedBitmap = CameraXFragment.mPassportSizedBitmap;

               // File file = new File(Environment.getExternalStorageDirectory() + "/" + "."+studentIDCard.getId() + ".png");

               File croppedImage =  saveCroppedImage(getActivity(), mPassportSizedBitmap, ".Passport",   studentIDCard.getId()+".png");

                studentIDCard.setRawProfileImage(croppedImage.getPath());
                handler.postDelayed(() ->{
                   String crest =  PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(),"CREST","");
                    Glide.with(this).asBitmap().load("file://"+crest).override(100,100).into(imageViewSchoolCrest);

                    if(mPassportSizedBitmap!=null){
                        Glide.with(this).load(mPassportSizedBitmap).into(imgPassportSizedView);
                    }

                    /*if(mPassportSizedBitmap!=null){
                        Glide.with(this).load(mPassportSizedBitmap).into(imgPassportSizedView);
                    }
                    int schoolCrest = PxUtils.getDrawableByID(getActivity(), studentIDCard.getSchoolCode());
                    if(schoolCrest<=0){
                        Glide.with(this).load(R.drawable.ic_coat_of_arms).into(imageViewSchoolCrest);
                    }
                    else Glide.with(this).load(schoolCrest).into(imageViewSchoolCrest);
*/
                }, 500);

                String content = "School:"+studentIDCard.getSchoolName()+"\n"+"Name:"+studentIDCard.getFullName()+"\n"+"ID:"+studentIDCard.getShs_number();
                generateImage(content);
               /* new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {


                        try {

                            //JSONObject data = new JSONObject();
                            //data.put("Name of School", studentIDCard.getSchoolName());
                           // data.put("Name of Student", studentIDCard.getFullName());
                           // data.put("id", studentIDCard.getShs_number());
                           *//* data.put("Date of Birth", studentIDCard.getDob());
                            data.put("Guardian Contact","0201393112");
                            data.put("Expiration Date", studentIDCard.getExpirationDate());*//*



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                }, 1000);*/
            }




        }
        catch (Exception e){
            e.printStackTrace();
        }

        //Toast.makeText(getActivity(), "id is : "+event.getId(), Toast.LENGTH_SHORT).show();

    }



    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void reset(){
        showImage(null);
    }

    private void showImage(Bitmap bitmap) {


        if(getActivity()!=null){
            getActivity().runOnUiThread(() -> {
                if (bitmap == null) {
                    imgResult.setImageResource(android.R.color.transparent);
                    qrImage = null;

                } else {
                    imgResult.setImageBitmap(bitmap);
                }
            });
        }


    }

    private void generateImage(String text){

        showLoadingVisible(true);
        new Thread(() -> {
            int size = imgResult.getMeasuredWidth();
            Map<EncodeHintType, Object> hintMap = new EnumMap<>(EncodeHintType.class);
            hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            hintMap.put(EncodeHintType.MARGIN, 1);
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            try {
                BitMatrix byteMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, size,
                        size, hintMap);
                int height = byteMatrix.getHeight();
                int width = byteMatrix.getWidth();
                qrImage = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

                for (int x = 0; x < width; x++){
                    for (int y = 0; y < height; y++){
                        if(qrImage!=null)
                        qrImage.setPixel(x, y, byteMatrix.get(x,y) ? Color.BLACK : Color.WHITE);
                    }
                }

                if(getActivity()!=null)
                getActivity().runOnUiThread(() -> {
                    showImage(qrImage);
                    showLoadingVisible(false);
                    btnSubmit.setEnabled(true);

                });
            } catch (WriterException e) {
                btnSubmit.setEnabled(false);
                e.printStackTrace();
                Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                //alert(e.getMessage());
            }
        }).start();
    }

    private void saveImage() {
        if (qrImage == null) {
            Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
            // alert("Belum ada gambar.");
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION);
            return;
        }


        String fname = "qrcode-" + Calendar.getInstance().getTimeInMillis();
        boolean success = true;
        try {
            String result = MediaStore.Images.Media.insertImage(
                    getActivity().getContentResolver(),
                    qrImage,
                    fname,
                    "QRCode Image"
            );
            if (result == null) {
                success = false;
            } else {
                Log.e(TAG, result);
            }
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }

        if (!success) {
            Toast.makeText(getActivity(), "Failed to save image", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Image saved to gallery.", Toast.LENGTH_SHORT).show();
            //self.snackbar("Gambar tersimpan ke gallery.");
        }
    }


    private void showDialogProgressDialog(Activity activity){
        if(alertDialog==null)
            createProgressDialog(activity);

        alertDialog.show();
    }
    private void createProgressDialog(Activity activity){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity,
                R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.progress_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialog = (androidx.appcompat.app.AlertDialog) builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialog.getWindow()!=null)
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }

    public void cancelDialogProgressDialog(){
        if(alertDialog!=null){
            alertDialog.cancel();
        }
    }

}