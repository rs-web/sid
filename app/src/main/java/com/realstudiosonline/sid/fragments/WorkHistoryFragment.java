package com.realstudiosonline.sid.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.models.DrawerSelectedEvent;
import com.realstudiosonline.sid.models.LoginResponse;
import com.realstudiosonline.sid.models.MessageEvent;
import com.realstudiosonline.sid.models.ProjectOfficer;
import com.realstudiosonline.sid.utils.PreferenceHelper;

import org.greenrobot.eventbus.EventBus;

import at.grabner.circleprogress.CircleProgressView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WorkHistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class WorkHistoryFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    CircleProgressView mCircleView;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReviewSessionsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WorkHistoryFragment newInstance(String param1, String param2) {
        WorkHistoryFragment fragment = new WorkHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public WorkHistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_work_history, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        mCircleView = (CircleProgressView) view.findViewById(R.id.circleView);

        //custom typeface

        if(getActivity()!=null){
            Typeface typeface = ResourcesCompat.getFont(getActivity(), R.font.roboto_bold);
            mCircleView.setTextTypeface(typeface);
            mCircleView.setUnitTextTypeface(typeface);
        }

        MaterialTextView tv_officer_name = view.findViewById(R.id.tv_officer_name);
        MaterialTextView tv_officer_id = view.findViewById(R.id.tv_officer_id);
        Gson gson = new Gson();
        String name = "";
        String officerID = "";

        if(getActivity()!=null){
            String projectOfficerJson = PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(), "projectOfficer", null);

            String userString = PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(),"user", null);

            if(userString!=null){
                LoginResponse user  = gson.fromJson(userString, LoginResponse.class);
                name = user.getFirstname() + " "+ user.getLastname();
            }

            if(projectOfficerJson!=null){
                ProjectOfficer projectOfficer =gson.fromJson(projectOfficerJson,  ProjectOfficer.class);
                officerID = "ID No. "+projectOfficer.getProject_officer_id();

            }
        }

        tv_officer_name.setText(name);
        tv_officer_id.setText(officerID);

        AppCompatTextView tvComplete = view.findViewById(R.id.tv_complete);

        view.findViewById(R.id.drawer_menu).setOnClickListener(v ->{

            EventBus.getDefault().post(new DrawerSelectedEvent(1, true));
        });

        view.findViewById(R.id.review_sessions).setOnClickListener(v -> {
            EventBus.getDefault().post(new MessageEvent(true, "Review Sessions",5));
        });


        view.findViewById(R.id.review_cards).setOnClickListener(v -> {
            EventBus.getDefault().post(new MessageEvent(true, "Review Cards",4));
        });
    }
}