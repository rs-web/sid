package com.realstudiosonline.sid.fragments;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.devkit.api.Misc;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.Html;
import android.util.Log;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.camera.core.CameraX;
import androidx.camera.core.CameraX.LensFacing;
import androidx.camera.core.FlashMode;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageAnalysisConfig;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureConfig;
import androidx.camera.core.Preview;
import androidx.camera.core.PreviewConfig;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.LottieOnCompositionLoadedListener;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.models.DrawerSelectedEvent;
import com.realstudiosonline.sid.models.MessageEvent;
import com.realstudiosonline.sid.models.PhotoBioSelected;
import com.realstudiosonline.sid.models.School;
import com.realstudiosonline.sid.models.SelectedFragment;
import com.realstudiosonline.sid.models.StudentIDCard;
import com.realstudiosonline.sid.room.StudentIDViewModel;
import com.realstudiosonline.sid.utils.DemoAnalyzer;
import com.realstudiosonline.sid.utils.PreferenceHelper;
import com.suprema.BioMiniFactory;
import com.suprema.CaptureResponder;
import com.suprema.IBioMiniDevice;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class CameraXFragment extends Fragment {


    //biometric fingerprint
    public static final boolean mbUsbExternalUSBManager = false;
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private UsbManager mUsbManager = null;
    private PendingIntent mPermissionIntent = null;
    private BioMiniFactory mBioMiniFactory = null;
    public static final int REQUEST_WRITE_PERMISSION = 786;
    public IBioMiniDevice mCurrentDevice = null;
    private Context mainContext;
    public final static String TAG = "CameraXFragment";

    private int flag=0;

    private TextureView textureView;
    public static final int REQUEST_CODE_PERMISSION = 601;
    public static final String[] REQUIRED_PERMISSIONS = new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"};

    private AppCompatImageView imgResult;
    private LottieAnimationView imagePreviewBio;
    public static LensFacing lens = LensFacing.BACK;
    private AppCompatTextView ib, tvPhotoAppears, tvFingerTop, tvFingerBottom, tvBioAppears;
    public static Bitmap mPassportSizedBitmap;
    private View mViewNextPrevious;
    private LottieAnimationView btnRetake;

    private View cameraBox, parent_view, view_bio_metric, camera_frame;

    public static CameraXFragment newInstance() {
        return new CameraXFragment();
    }

    public static List<byte[]> biometricFingerPrint;

    public static List<Bitmap> biometricFingerPrintBitmap;

    private final ArrayList<StudentIDCard> mStudentIDCards = new ArrayList<>();
    private final IBioMiniDevice.CaptureOption mCaptureOptionDefault = new IBioMiniDevice.CaptureOption();
    private  boolean isSaved = false;
    StudentIDCard studentIDCard;
    private School selectedSchool;
    StudentIDViewModel mStudentViewModel;
    private AlertDialog alertDialogFingerPrint,alertDialogError;
    SoundPool soundPool;
    int failed, verified,please_try_again, unrecognized;
    AlertDialog alertDialog;


    @SuppressLint("SetTextI18n")
    private void showDialogConfirmation(byte[] capturedImage, Bitmap bioBitmap){
        if(getActivity()!=null){
            final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity(),R.style.MaterialAlertDialog_Alert);
            bottomSheetDialog.setCancelable(false);
            bottomSheetDialog.setDismissWithAnimation(true);
            bottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            if(bottomSheetDialog.getWindow()!=null)
            bottomSheetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            bottomSheetDialog.setContentView(R.layout.confirmation_dialog);
            MaterialTextView tv_message = bottomSheetDialog.findViewById(R.id.tv_message);
            LottieAnimationView lottieSuccessBio = bottomSheetDialog.findViewById(R.id.lottie_success_bio);
            if(lottieSuccessBio!=null)
            lottieSuccessBio.addAnimatorListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {

                    lottieSuccessBio.removeAllAnimatorListeners();
                    if(getActivity()!=null)
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            lottieSuccessBio.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                            Glide.with(getActivity()).load(bioBitmap).into(lottieSuccessBio);
                        }
                    });
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            String message ="";
            switch (biometricFingerPrint.size()){
                case 0:
                    message = "Confirm right thumb bio for <br>"+"<b>"+studentIDCard.getFullName()+"</b>";
                    break;
                case 1:
                    message = "Confirm right fore finger bio for <br>"+"<b>"+studentIDCard.getFullName()+"</b>";
                    break;
                case 2:
                    message = "Confirm left thumb bio for <br>"+"<b>"+studentIDCard.getFullName()+"</b>";
                    break;
                case 3:
                    message = "Confirm left fore finger bio for <br>"+"<b>"+studentIDCard.getFullName()+"</b>";
                    break;
            }
            if(tv_message!=null)
            tv_message.setText(Html.fromHtml(message));

            AppCompatTextView btnConfirm = bottomSheetDialog.findViewById(R.id.btn_confirm);

            if(btnConfirm!=null)
                btnConfirm.setOnClickListener(v -> {
                    if(biometricFingerPrint.size()<4){
                        biometricFingerPrint.add(capturedImage);
                        biometricFingerPrintBitmap.add(bioBitmap);
                    }
                    switch (biometricFingerPrint.size()){
                        case 0:
                            imagePreviewBio.setScaleType(ImageView.ScaleType.FIT_XY);
                            imagePreviewBio.setImageResource(R.drawable.left_thumb_finger_indicator);
                            tvFingerTop.setText(R.string.right_thumb);
                            tvFingerBottom.setText(R.string.right_thumb);
                            tvBioAppears.setText("Place your right thumb on the scanner");
                            break;
                        case 1:
                            imagePreviewBio.setScaleType(ImageView.ScaleType.FIT_XY);
                            imagePreviewBio.setImageResource(R.drawable.right_fore_finger_indicator);
                            tvFingerTop.setText(R.string.right_finger);
                            tvFingerBottom.setText(R.string.right_finger);
                            tvBioAppears.setText("Place your right fore finger on the scanner");
                            break;
                        case 2:
                            imagePreviewBio.setScaleType(ImageView.ScaleType.FIT_XY);
                            imagePreviewBio.setImageResource(R.drawable.left_thumb_finger_indicator);
                            tvFingerTop.setText(R.string.left_thumb);
                            tvFingerBottom.setText(R.string.left_thumb);
                            tvBioAppears.setText("Place your left thumb on the scanner");
                            break;

                        case 3:
                            imagePreviewBio.setScaleType(ImageView.ScaleType.FIT_XY);
                            imagePreviewBio.setImageResource(R.drawable.left_fore_finger_indicator);
                            tvFingerTop.setText(R.string.left_finger);
                            tvFingerBottom.setText(R.string.left_finger);
                            tvBioAppears.setText("Place your left fore finger on the scanner");

                            break;
                        case 4:

                            showDialogProgressDialog(getActivity());

                           /* switchPower(false);
                            if (mBioMiniFactory != null) {
                                mBioMiniFactory.close();
                                mBioMiniFactory.close();
                            }*/

                            new Handler(Looper.getMainLooper()).postDelayed(()->{
                                CameraX.unbindAll();
                                EventBus.getDefault().post(new MessageEvent(true, "Generate Card", 3));
                                alertDialog.dismiss();
                            },100);
                            break;

                        default:
                            break;
                    }

                    bottomSheetDialog.dismiss();
                });
            AppCompatTextView btnRetake = bottomSheetDialog.findViewById(R.id.btn_retake);

            if(btnRetake!=null)
                btnRetake.setOnClickListener(v -> {
                    reCapture();
                    bottomSheetDialog.dismiss();
                    Log.d("BottomDialogs", "Do something!");
                });


            bottomSheetDialog.getBehavior().setState(BottomSheetBehavior.STATE_EXPANDED);
            new Handler().postDelayed(()->{
                if(lottieSuccessBio!=null)
                    lottieSuccessBio.playAnimation();
            },50);
            bottomSheetDialog.show();

        }
    }


    @SuppressLint("SetTextI18n")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCameraFragmentSelected(PhotoBioSelected bioSelected){
        String student = PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(),"student_id",null);
        studentIDCard =  new Gson().fromJson(student, StudentIDCard.class);
        Log.d("onCameraFragment","student is "+ student);
        Log.d("onCameraFragment","studentIDCard is "+ studentIDCard.toString());
        imgResult.setVisibility(View.GONE);
        textureView.setVisibility(View.VISIBLE);
        cameraBox.setVisibility(View.VISIBLE);
        mViewNextPrevious.setVisibility(View.GONE);
        btnRetake.setVisibility(View.GONE);
        tvPhotoAppears.setVisibility(View.VISIBLE);
        ib.setVisibility(View.VISIBLE);
        if(biometricFingerPrint!=null){
            biometricFingerPrint.clear();
        }
        if(biometricFingerPrintBitmap!=null){
            biometricFingerPrintBitmap.clear();
        }
        imagePreviewBio.setImageResource(R.drawable.right_thumb_finger_indicator);
        view_bio_metric.setVisibility(View.GONE);
        parent_view.setVisibility(View.VISIBLE);
        tvFingerTop.setText(R.string.right_thumb);
        tvFingerBottom.setText(R.string.right_thumb);
        tvBioAppears.setText("Place your right thumb on the scanner");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        requestPermission();
        mainContext = getActivity();
        mCaptureOptionDefault.frameRate = IBioMiniDevice.FrameRate.SHIGH;
        mCaptureOptionDefault.extractParam.captureTemplate = true;
        return inflater.inflate(R.layout.fragment_photo_bio_updated, container, false);
    }



    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        }
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //TODO switchPower
        switchPower(true);
        mStudentViewModel = new ViewModelProvider(this).get(StudentIDViewModel.class);
        mViewNextPrevious = view.findViewById(R.id.view_next_previous);
        view_bio_metric = view.findViewById(R.id.view_bio_metric);
        textureView = view.findViewById(R.id.texture);
        btnRetake = view.findViewById(R.id.retake_picture);
        cameraBox = view.findViewById(R.id.view_cameara_box);
        camera_frame = view.findViewById(R.id.camera_frame);
        parent_view = view.findViewById(R.id.parent_view);
        tvPhotoAppears = view.findViewById(R.id.tv_photo_appears);
        tvBioAppears = view.findViewById(R.id.tv_bio_appears);
        tvFingerBottom = view.findViewById(R.id.tv_finger_bottom);
        tvFingerTop = view.findViewById(R.id.tv_finger_top);

        imagePreviewBio = view.findViewById(R.id.imagePreviewBio);
        textureView.setOpaque(false);
        createProgressDialog(getActivity());
        view.findViewById(R.id.tv_capture_bio).setOnClickListener(v -> {
            //TODO implement lottie
            imagePreviewBio.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imagePreviewBio.playAnimation();
            captureBio();
        });

        if(getActivity()!=null)
        createVerificationVoice(getActivity());
        view.findViewById(R.id.tv_student_info).setOnClickListener(v -> new Handler(Looper.getMainLooper()).postDelayed(()-> EventBus.getDefault().post(new MessageEvent(false, "School setup",1)),1000));

        if (mbUsbExternalUSBManager && getActivity()!=null) {

            mUsbManager = (UsbManager)getActivity().getSystemService(Context.USB_SERVICE);
            mBioMiniFactory = new BioMiniFactory(mainContext, mUsbManager) {
                @Override
                public void onDeviceChange(DeviceChangeEvent event, Object dev) {

                    log("----------------------------------------");
                    log("onDeviceChange : " + event + " using external usb-manager");
                    log("----------------------------------------");
                    if (event == DeviceChangeEvent.DEVICE_ATTACHED && mCurrentDevice == null) {
                        new Thread(() -> {
                            int cnt = 0;
                            while (mBioMiniFactory == null && cnt < 20) {
                                SystemClock.sleep(1000);
                                cnt++;
                            }
                            if (mBioMiniFactory != null) {
                                mCurrentDevice = mBioMiniFactory.getDevice(0);
                                Log.e(TAG, "mCurrentDevice attached : " + mCurrentDevice);
                                if (mCurrentDevice != null) {
                                    log(" DeviceName : " + mCurrentDevice.getDeviceInfo().deviceName);
                                    log("         SN : " + mCurrentDevice.getDeviceInfo().deviceSN);
                                    PreferenceHelper.getPreferenceHelperInstace().setString(getActivity(),"IMEI",mCurrentDevice.getDeviceInfo().deviceSN);
                                    log("SDK version : " + mCurrentDevice.getDeviceInfo().versionSDK);
                                }
                            }
                        }).start();
                    } else if (mCurrentDevice != null && event == DeviceChangeEvent.DEVICE_DETACHED && mCurrentDevice.isEqual(dev)) {
                        Log.e(TAG, "mCurrentDevice removed : " + mCurrentDevice);
                        mCurrentDevice = null;
                    }
                }
            };
            //
            mPermissionIntent = PendingIntent.getBroadcast(getActivity(), 0, new Intent(ACTION_USB_PERMISSION), 0);
            IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
            getActivity().registerReceiver(mUsbReceiver, filter);
            checkDevice();


        } else {
            mBioMiniFactory = new BioMiniFactory(mainContext) {
                @Override
                public void onDeviceChange(DeviceChangeEvent event, Object dev) {
                    log("----------------------------------------");
                    log("onDeviceChange : " + event);
                    log("----------------------------------------");
                    if (event == DeviceChangeEvent.DEVICE_ATTACHED && mCurrentDevice == null) {
                        new Thread(() -> {
                            int cnt = 0;
                            while (mBioMiniFactory == null && cnt < 20) {
                                SystemClock.sleep(1000);
                                cnt++;
                            }
                            if (mBioMiniFactory != null) {
                                //TODO fix Sample: BioMini SDK for Android 2.0.1.r2356 UsbManager: exception in UsbManager.openDevice  java.lang.IllegalArgumentException: device /dev/bus/usb/001/004 does not exist or is restricted
                                //2021-04-07 10:34:27.236 9382-9459/com.realstudiosonline.sid E/UsbManager: exception in UsbManager.openDevice
                                //    java.lang.IllegalArgumentException: device /dev/bus/usb/001/004 does not exist or is restricted
                                mCurrentDevice = mBioMiniFactory.getDevice(0);
                                Log.e(TAG, "mCurrentDevice attached : " + mCurrentDevice);
                                if (mCurrentDevice != null) {
                                    log(" DeviceName : " + mCurrentDevice.getDeviceInfo().deviceName);
                                    log("         SN : " + mCurrentDevice.getDeviceInfo().deviceSN);
                                    PreferenceHelper.getPreferenceHelperInstace().setString(getActivity(),"IMEI",mCurrentDevice.getDeviceInfo().deviceSN);

                                    log("SDK version : " + mCurrentDevice.getDeviceInfo().versionSDK);
                                }
                            }
                        }).start();
                    } else if (mCurrentDevice != null && event == DeviceChangeEvent.DEVICE_DETACHED && mCurrentDevice.isEqual(dev)) {
                        Log.e(TAG, "mCurrentDevice removed : " + mCurrentDevice);
                        mCurrentDevice = null;
                    }
                }
            };
        }
        mBioMiniFactory.setTransferMode(IBioMiniDevice.TransferMode.MODE1);


        view.findViewById(R.id.drawer_menu).setOnClickListener(v -> EventBus.getDefault().post(new DrawerSelectedEvent(1, true)));


        btnRetake.setOnClickListener(v1 -> {
            imgResult.setVisibility(View.GONE);
            textureView.setVisibility(View.VISIBLE);
            cameraBox.setVisibility(View.VISIBLE);
            mViewNextPrevious.setVisibility(View.GONE);
            btnRetake.setVisibility(View.GONE);
            //progressBar.setVisibility(View.GONE);
            tvPhotoAppears.setVisibility(View.VISIBLE);
            ib.setVisibility(View.VISIBLE);
/*
            Handler handler = new Handler(Looper.getMainLooper());
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            AudioManager manager = (AudioManager)getActivity().getSystemService(Context.AUDIO_SERVICE);
                            manager.setStreamMute(AudioManager.STREAM_SYSTEM,false);
                        }
                    });
                }
            },1000);*/
            if (allPermissionsGranted()) {
           /* Toast.makeText(getActivity(),
                    "Permissions granted.",
                    Toast.LENGTH_SHORT).show();*/
                textureView.post(this::startCamera);
                textureView.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> updateTransform());
            }
            else {
                if(getActivity()!=null && isAdded())
                    requestPermissions(REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSION);
            }
        });

        view.findViewById(R.id.layout_back).setOnClickListener(v -> EventBus.getDefault().post(new MessageEvent(true, "School setup",1)));

        view.findViewById(R.id.layout_next).setOnClickListener(v -> {
            biometricFingerPrint = new ArrayList<>();
            biometricFingerPrintBitmap = new ArrayList<>();
            imgResult.setVisibility(View.GONE);
            textureView.setVisibility(View.VISIBLE);
            cameraBox.setVisibility(View.VISIBLE);
            mViewNextPrevious.setVisibility(View.GONE);
            btnRetake.setVisibility(View.GONE);
            tvPhotoAppears.setVisibility(View.VISIBLE);
            ib.setVisibility(View.VISIBLE);
            imagePreviewBio.setImageResource(R.drawable.right_thumb_finger_indicator);
            view_bio_metric.setVisibility(View.VISIBLE);
            parent_view.setVisibility(View.GONE);


        });
        ib = view.findViewById(R.id.snap_picture);
        imgResult = view.findViewById(R.id.iv_preview);



    }

    @SuppressLint("RestrictedApi")
    private void startCamera() {
        initCamera();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setUserVisibleHint(SelectedFragment visibleHint){
       // Toast.makeText(getActivity(), "setUserVisibleHint", Toast.LENGTH_SHORT).show();
        Gson gson = new Gson();
        String selectedSchoolJson = PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(),"selectedSchool", "");
        selectedSchool = gson.fromJson(selectedSchoolJson, School.class);

        String studentID = PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(),"student_id",null);
        if(studentID!=null){
            studentIDCard =  new Gson().fromJson(studentID, StudentIDCard.class);
        }

        if(visibleHint.getId() == 1){
            if (allPermissionsGranted()) {
                textureView.post(this::startCamera);
            } else {
                if(getActivity()!=null && isAdded())
                    requestPermissions(REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSION);
            }
            textureView.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> updateTransform());
        }

    }


    @Override
    public void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();

    }

    @Override
    public void onStop() {
        isSaved = false;
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void switchPower(boolean isOpen) {
       Misc.nativeUsbMode(isOpen ? 1 : 0);
        Misc.fingerEnable(isOpen);

    }

    @Override
    public void onDestroy() {
        switchPower(false); //power off the scanner
        if (mBioMiniFactory != null) {
            mBioMiniFactory.close();
            mBioMiniFactory = null;
        }
        cancelDialogProgressDialog();
        cancelErrorDialog();
        super.onDestroy();
    }

    private void initCamera() {
        CameraX.unbindAll();
        PreviewConfig pc = new PreviewConfig
                .Builder()
                .setLensFacing(lens)
                .setTargetResolution(new Size(textureView.getWidth(), textureView.getHeight()))
                .build();

        Preview preview = new Preview(pc);
        preview.enableTorch(true);


        preview.setOnPreviewOutputUpdateListener(output -> {
            ViewGroup vg = (ViewGroup) textureView.getParent();
            vg.removeView(textureView);
            vg.addView(textureView, 0);
            textureView.setSurfaceTexture(output.getSurfaceTexture());
            updateTransform();
        });
        ImageCaptureConfig icc = new ImageCaptureConfig
                .Builder()
                .setCaptureMode(ImageCapture.CaptureMode.MAX_QUALITY)
                .setFlashMode(FlashMode.AUTO)
                .build();
        ImageCapture imgCap = new ImageCapture(icc);

        ib.setOnClickListener(v -> {

            // progressBar.setVisibility(View.VISIBLE);
            File file = new File(Environment.getExternalStorageDirectory() + "/" + "."+studentIDCard.getId() + ".png");

            ib.setVisibility(View.INVISIBLE);
            imgCap.takePicture(file, (Runnable::run), new ImageCapture.OnImageSavedListener() {

                @Override
                public void onImageSaved(@NonNull File file) {

                    Looper looper = Looper.getMainLooper();
                    if(looper!=null)
                    new Handler(looper).post((()->{
                        textureView.setVisibility(View.GONE);
                      //  textureView.setBackgroundResource(R.color.charcoal);
                        showDialogProgressDialog(getActivity());
                    }));


                    String msg = "Image is saved at: " + file.getAbsolutePath();
                    Log.d("IMAGE_T", msg);

                    String studentIDCardString =  PreferenceHelper.getPreferenceHelperInstace().getString(getActivity(),"student_id",null);
                    Gson gson = new Gson();

                    if(studentIDCardString!=null){
                        studentIDCard = gson.fromJson(studentIDCardString, StudentIDCard.class);
                        //studentIDCard.setRawProfileImage(file.getPath());
                    }
                    String studentIDJson = gson.toJson(studentIDCard);

                    PreferenceHelper.getPreferenceHelperInstace().setString(getActivity(),"student_id",studentIDJson);

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;

                    Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), options);

                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);

                    Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0,0,bitmap.getWidth(),bitmap.getHeight(), matrix, true);

                    //TODO CAMERA CROP IMAGE
                    Bitmap bitmap1 = cropImage(rotatedBitmap,  parent_view,camera_frame);

                    mPassportSizedBitmap = bitmap1;


                    new Handler(Looper.getMainLooper()).postDelayed(() ->{
                        CameraX.unbindAll();
                        if(getActivity()!=null){
                            Glide.with(getActivity()).load(mPassportSizedBitmap).into(imgResult);
                        }
                        cameraBox.setVisibility(View.GONE);
                        imgResult.setVisibility(View.VISIBLE);
                        textureView.setVisibility(View.GONE);
                        mViewNextPrevious.setVisibility(View.VISIBLE);
                        btnRetake.setVisibility(View.VISIBLE);
                        btnRetake.playAnimation();
                        tvPhotoAppears.setVisibility(View.GONE);
                        ib.setVisibility(View.GONE);
                        alertDialog.dismiss();
                    }, 100);

                    Log.i(TAG, msg);
                }

                @Override
                public void onError(@NonNull ImageCapture.ImageCaptureError imageCaptureError, @NonNull String message, Throwable cause) {
                    cause.printStackTrace();
                    Log.e(TAG, "An error occurred while saving:" + message);
                    // progressBar.setVisibility(View.GONE);
                    tvPhotoAppears.setVisibility(View.VISIBLE);
                    textureView.setVisibility(View.VISIBLE);
                }


            });
        });
        ImageAnalysisConfig iac = new ImageAnalysisConfig
                .Builder()
                .setLensFacing(lens)
                .setImageReaderMode(ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE)
                .build();

        ImageAnalysis imageAnalysis = new ImageAnalysis(iac);

        imageAnalysis.setAnalyzer(Runnable::run,
                new DemoAnalyzer());
        CameraX.bindToLifecycle(this, preview, imgCap, imageAnalysis);

      //  CameraControl cameraControl = CameraX.getCameraControl(LensFacing.BACK);

    }

    private void updateTransform() {
        Matrix mat = new Matrix();
        float centerX = textureView.getWidth() / 2.0f;
        float centerY = textureView.getHeight() / 2.0f;

        float rotationDegrees;
        switch (textureView.getDisplay().getRotation()) {
            case Surface.ROTATION_0:
                rotationDegrees = 0;
                break;
            case Surface.ROTATION_90:
                rotationDegrees = 90;
                break;
            case Surface.ROTATION_180:
                rotationDegrees = 180;
                break;
            case Surface.ROTATION_270:
                rotationDegrees = 270;
                break;
            default:
                return;
        }
        mat.postRotate(rotationDegrees, centerX, centerY);
        textureView.setTransform(mat);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {

            if (allPermissionsGranted()) {
               /* Toast.makeText(getActivity(),
                        "Permissions granted.",
                        Toast.LENGTH_SHORT).show();*/
                textureView.post(this::startCamera);
            } else {
                if(getActivity()!=null && isAdded()) {
                    Toast.makeText(getActivity(),
                            "Permissions not granted by the user.",
                            Toast.LENGTH_SHORT).show();

                    getActivity().finish();
                }
            }
        }
    }

    private boolean allPermissionsGranted() {
        for (String permission : REQUIRED_PERMISSIONS) {
            if(isAdded() && getActivity()!=null)
                if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
        }
        return true;
    }


    private Bitmap cropImage( Bitmap bitmap, View frame, View mCameraBox) {
        int heightOriginal = frame.getHeight();
        int widthOriginal = frame.getWidth();
        int heightFrame = mCameraBox.getHeight();
        int widthFrame = mCameraBox.getWidth();
        int leftFrame = mCameraBox.getLeft();
        int topFrame = mCameraBox.getTop();
        int heightReal = bitmap.getHeight();
        int widthReal = bitmap.getWidth();
        int widthFinal = widthFrame * widthReal / widthOriginal;
        int heightFinal = heightFrame * heightReal / heightOriginal;
        int leftFinal = leftFrame * widthReal / widthOriginal;
        int topFinal = topFrame * heightReal / heightOriginal;
        return Bitmap.createBitmap(bitmap, leftFinal, topFinal, widthFinal, heightFinal);//100 is the best quality possibe
    }




    //fingerprint bio
    //capture a finger and show the image
    public void captureBio(){
        Log.e(TAG,"start to capture");
        flag=0;

        //imagePreviewBio.setImageBitmap(null);
        if (mCurrentDevice != null) {
            mBioMiniFactory.setTransferMode(IBioMiniDevice.TransferMode.MODE1);
                mCurrentDevice.setParameter(new IBioMiniDevice.Parameter(IBioMiniDevice.ParameterType.TIMEOUT, 10000));
            mCurrentDevice.setParameter(new IBioMiniDevice.Parameter(IBioMiniDevice.ParameterType.AUTO_ROTATE, 1));
            //mCurrentDevice.setParameter(new IBioMiniDevice.Parameter(IBioMiniDevice.ParameterType.EXT_TRIGGER, 1));
           // mCurrentDevice.setParameter(new IBioMiniDevice.Parameter(IBioMiniDevice.ParameterType.ENABLE_AUTOSLEEP, 1));
            mCaptureOptionDefault.frameRate = IBioMiniDevice.FrameRate.HIGH;
            mCaptureOptionDefault.extractParam.captureTemplate = true;
            mCurrentDevice.captureSingle(
                    mCaptureOptionDefault,
                    mCaptureResponseDefault,
                    true);
            //mHandler.sendEmptyMessageDelayed(CAPT,3000);
        }
        else {
            if(getActivity()!=null)
            mUsbManager = (UsbManager)getActivity().getSystemService(Context.USB_SERVICE);
            mPermissionIntent = PendingIntent.getBroadcast(getActivity(), 0, new Intent(ACTION_USB_PERMISSION), 0);
            IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
            getActivity().registerReceiver(mUsbReceiver, filter);
            checkDevice();

        }
    }

    public void reCapture(){
       // mCurrentDevice.abortCapturing();
        flag=0;

        if(mCurrentDevice.isCapturing()){
            Log.e(TAG,"device captuing busy");
            if(mCurrentDevice!=null){
                mCurrentDevice.abortCapturing();
            }
        }
/*        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        //imagePreviewBio.setImageBitmap(null);
       // imagePreviewBio.setImageResource(R.drawable.left_fore_finger_indicator);
        mBioMiniFactory.setTransferMode(IBioMiniDevice.TransferMode.MODE1);
        if(mCurrentDevice != null) {
            mCurrentDevice.setParameter(new IBioMiniDevice.Parameter(IBioMiniDevice.ParameterType.TIMEOUT, 10000));
            int outTime=  (int)mCurrentDevice.getParameter(IBioMiniDevice.ParameterType.TIMEOUT).value;

            log(" Recapture, time out value:"+outTime/1000);
            Log.e(TAG,"Recapture start to capture");
            //mCaptureOptionDefault.captureTimeout = (int)mCurrentDevice.getParameter(IBioMiniDevice.ParameterType.TIMEOUT).value;
            mCurrentDevice.captureSingle(mCaptureOptionDefault, mCaptureResponseDefault, true);
            // mHandler.sendEmptyMessageDelayed(CAPT,2000);
        }
    }

    synchronized public void printState(final CharSequence str) {
        log(str.toString());

    }

    synchronized public void log(final String msg) {
        Log.e(TAG, msg);
        if(getActivity()!=null){
            getActivity().runOnUiThread(() -> Log.e("BIO_LOG", msg));
        }

    }

    void cancelFingerPrintDialog(){
        if(alertDialogFingerPrint!=null)
            alertDialogFingerPrint.cancel();
    }

    @SuppressLint("SetTextI18n")
    private void createFingerPrintDialog(Activity activity,String userName){

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity,R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.alert_dialog_warning, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialogFingerPrint = builder.create();

        MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);

        String text = "Fingerprint bio exist for <br><b>"+userName+ "</b>";
        tv_message.setText(Html.fromHtml(text));
        AppCompatTextView btnNo =  alert_view.findViewById(R.id.btn_no);
        btnNo.setVisibility(View.GONE);
       /* MaterialButton btnNo = (MaterialButton) alert_view.findViewById(R.id.btn_no);
        btnNo.setOnClickListener(v -> alertDialogFingerPrint.hide());*/

        AppCompatTextView btnYes =  alert_view.findViewById(R.id.btn_yes);
        btnYes.setText("Okay");
        btnYes.setOnClickListener(v -> {
            alertDialogFingerPrint.cancel();
            new Handler(Looper.getMainLooper()).postDelayed(()-> EventBus.getDefault().post(new MessageEvent(false, "School setup",1)),500);
        });

        alertDialogFingerPrint.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialogFingerPrint.getWindow()!=null)
            alertDialogFingerPrint.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        alertDialogFingerPrint.show();
    }


    @SuppressLint("SetTextI18n")
    private void createProgressDialog(Activity activity){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity,
                R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.progress_dialog, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        MaterialTextView tvMessage = alert_view.findViewById(R.id.tv_message);
        tvMessage.setText("Processing...");
        alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialog.getWindow()!=null)
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }

    private void showDialogProgressDialog(Activity activity){
        if(alertDialog==null)
            createProgressDialog(activity);
        alertDialog.show();

    }

    public void cancelDialogProgressDialog(){
        if(alertDialog!=null){
            alertDialog.cancel();
        }
    }



    private void playVoice(int voice){
        soundPool.play(voice, 1, 1, 0, 0, 1);
       // soundPool.autoPause();
    }

    private void createVerificationVoice(Activity activity){
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        soundPool= new SoundPool
                .Builder()
                .setMaxStreams(4)
                .setAudioAttributes(
                        audioAttributes)
                .build();

        verified = soundPool.load(activity, R.raw.verified, 1);
        failed = soundPool.load(activity, R.raw.failed, 1);
        please_try_again = soundPool.load(activity, R.raw.please_try_again, 1);
        unrecognized = soundPool.load(activity, R.raw.unrecognized, 1);
        //sound.release();
    }

    @SuppressLint("SetTextI18n")
    private void createErrorDialog(Activity activity){

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity,R.style.MaterialAlertDialog_rounded);
        View alert_view = getLayoutInflater().inflate(R.layout.alert_dialog_warning, null);
        builder.setCancelable(false);
        builder.setView(alert_view);
        alertDialogError =  builder.create();

        MaterialTextView tv_message = alert_view.findViewById(R.id.tv_message);

        tv_message.setText("Fingerprint capture failed");
        AppCompatTextView btnNo =  alert_view.findViewById(R.id.btn_no);
        btnNo.setVisibility(View.GONE);
       /* MaterialButton btnNo = (MaterialButton) alert_view.findViewById(R.id.btn_no);
        btnNo.setOnClickListener(v -> alertDialogFingerPrint.hide());*/

        AppCompatTextView btnYes =  alert_view.findViewById(R.id.btn_yes);
        btnYes.setText("Retry");
        btnYes.setOnClickListener(v -> {
            reCapture();
            if(alertDialogError!=null)
            alertDialogError.hide();
        });

        alertDialogError.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(alertDialogError.getWindow()!=null)
            alertDialogError.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    private void showErrorDialog(Activity activity){
        if(alertDialogError==null)
            createErrorDialog(activity);
        alertDialogError.show();
    }

    private void cancelErrorDialog(){
        if(alertDialogError!=null)
            alertDialogError.cancel();
    }

    private final CaptureResponder mCaptureResponseDefault = new CaptureResponder() {
        @Override
        public boolean onCaptureEx(final Object context, final Bitmap capturedImage,
                                   final IBioMiniDevice.TemplateData capturedTemplate,
                                   final IBioMiniDevice.FingerState fingerState) {
            flag=1;
            log("onCapture : Capture successful!");
            printState(getResources().getText(R.string.capture_single_ok));

            if(capturedTemplate!=null){
                Log.d("onCaptureEx" , "capturedTemplate.data.length : "+capturedTemplate.data.length);
            }

           // byte[] raw = mCurrentDevice.getCaptureImageAsRAW_8();
            log(((IBioMiniDevice) context).popPerformanceLog());

            if (getActivity() != null && isAdded()) {
                    getActivity().runOnUiThread(() -> {

                        if (capturedImage != null) {
                            //byte [] bioData = capturedTemplate.data;

                            boolean isMatched = false;
                            String matchedName = "";
                            long a = System.currentTimeMillis();

                            mStudentViewModel.getAllStudentIDs().observe(getViewLifecycleOwner(), studentIDCards -> {
                                if (studentIDCards != null) {
                                    mStudentIDCards.clear();
                                    mStudentIDCards.addAll(studentIDCards);
                                }

                            });

                            if (capturedTemplate != null) {
                                byte[] bioData = capturedTemplate.data;
                                Log.d("BYTE_SIZE", "size : " + bioData.length);

                                for (StudentIDCard studentIDCard : mStudentIDCards) {
                                    //TODO fix Java_com_suprema_android_BioMiniJni_Verify___3BI_3BI_3I+244) runtime.cc:523]   at com.suprema.android.BioMiniJni.Verify(Native method)

                                    if (mCurrentDevice.verify(
                                            bioData,
                                            bioData.length,
                                            studentIDCard.getRightThumb(),
                                            studentIDCard.getRightThumb().length)) {
                                        isMatched = true;
                                        matchedName = "" + studentIDCard.getFullName();
                                        //break;
                                    } else if (mCurrentDevice.verify(bioData, bioData.length, studentIDCard.getRightForeFinger(), studentIDCard.getRightForeFinger().length)) {
                                        isMatched = true;
                                        matchedName = "" + studentIDCard.getFullName();
                                    } else if (mCurrentDevice.verify(bioData, bioData.length, studentIDCard.getLeftThumb(), studentIDCard.getLeftThumb().length)) {
                                        isMatched = true;
                                        matchedName = "" + studentIDCard.getFullName();

                                    } else if (mCurrentDevice.verify(bioData, bioData.length, studentIDCard.getLeftForeFinger(), studentIDCard.getLeftForeFinger().length)) {
                                        isMatched = true;
                                        matchedName = "" + studentIDCard.getFullName();
                                    } else {
                                        isMatched = false;
                                        matchedName = "";
                                    }
                                    Log.e(TAG, "user:" + matchedName + " :" + isMatched);
                                }
                                long b = System.currentTimeMillis() - a;
                                log("match cost time: " + b);
                                Log.e(TAG, " use time: " + b);

                                if (isMatched) {
                                    log("Match found : " + matchedName);
                                    printState(getResources().getText(R.string.verify_ok));

                                    playVoice(verified);
                                    if (alertDialogFingerPrint != null) {

                                        cancelFingerPrintDialog();
                                    }
                                    createFingerPrintDialog(getActivity(), matchedName);

                                } else {
                                    //playVoice(unrecognized);
                                    //bioDataList.add(bioData);
                                    switch (biometricFingerPrint.size()) {
                                        case 0:
                                            //studentIDCard.setRightThumb(raw);
                                            showDialogConfirmation(bioData, capturedImage);
                                            Log.d("BioCapture", "Right thumb");
                                            break;
                                        case 1:
                                            //studentIDCard.setRightForeFinger(raw);
                                            showDialogConfirmation(bioData, capturedImage);
                                            Log.d("BioCapture", "Right fore finger");
                                            break;
                                        case 2:
                                            //studentIDCard.setLeftThumb(raw);
                                            showDialogConfirmation(bioData, capturedImage);
                                            Log.d("BioCapture", "Left thumb");
                                            break;
                                        case 3:
                                            //studentIDCard.setLeftForeFinger(raw);
                                            showDialogConfirmation(bioData, capturedImage);
                                            Log.d("BioCapture", "Left fore finger");
                                            break;
                                    }
                                    log("No match found : ");
                                    printState(getResources().getText(R.string.verify_not_match));
                                }

                                //showConfirmationDialog(capturedImage);
                                if (imagePreviewBio != null) {
                                    imagePreviewBio.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                                    Glide.with(getActivity()).load(capturedImage).into(imagePreviewBio);
                                }
                            }
                        }
                        else {
                            if(getActivity()!=null){
                                new Handler(Looper.getMainLooper()).post(() -> showErrorDialog(getActivity()));
                                //playVoice(failed);
                                playVoice(please_try_again);
                                if(mCurrentDevice!=null)
                                mCurrentDevice.abortCapturing();

                            }
                            log("<<ERROR>> Template is not extracted...");
                            printState(getResources().getText(R.string.verify_fail));
                        }
                    });
                }

            return true;
        }

        @Override
        public void onCaptureError(Object contest, int errorCode, String error) {
            log("onCaptureError : " + error);
            if(getActivity()!=null){
                new Handler(Looper.getMainLooper()).post(() -> showErrorDialog(getActivity()));
                //playVoice(failed);
                playVoice(please_try_again);
                if(mCurrentDevice!=null)
                    mCurrentDevice.abortCapturing();

            }
            if (errorCode != IBioMiniDevice.ErrorCode.OK.value())
                printState(getResources().getText(R.string.capture_single_fail));
        }
    };

    // check the finger print module
    public void checkDevice() {
        if (mUsbManager == null) return;
        HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();
        log("checkDevice..szie"+deviceList.size());
        for (UsbDevice _device : deviceList.values()) {
            Log.e(TAG, " device vid:" + _device.getVendorId() + " pid:" + _device.getProductId() + " name:" + _device.getDeviceName());
            if (_device.getVendorId() == 0x16d1) {
                //Suprema vendor ID
                Log.e(TAG, "find Fingerprint module bio slim2 : " + 0x16d1);
                mUsbManager.requestPermission(_device, mPermissionIntent);
                if (mBioMiniFactory == null) return;
                mBioMiniFactory.addDevice(_device);
                mCurrentDevice = mBioMiniFactory.getDevice(0);
            }
        }

    }

    /**
     *usb permission request
     */
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device =  intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (device != null) {
                            if (mBioMiniFactory == null) return;
                            mBioMiniFactory.addDevice(device);
                            log(String.format(Locale.ENGLISH, "Initialized device count- BioMiniFactory (%d)", mBioMiniFactory.getDeviceCount()));
                        }
                    } else {
                        Log.e(TAG, "permission denied for device" + device);
                    }
                }
            }
        }
    };


/*    synchronized public void printRev(final String msg) {
        log(msg);
    }*/


}