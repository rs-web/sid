package com.realstudiosonline.sid.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.realstudiosonline.sid.R;
import com.realstudiosonline.sid.adapter.WorkHistoryAdapter;
import com.realstudiosonline.sid.models.DrawerSelectedEvent;
import com.realstudiosonline.sid.models.MessageEvent;
import com.realstudiosonline.sid.models.SessionLogObject;
import com.realstudiosonline.sid.room.SchoolSessionViewModel;
import com.realstudiosonline.sid.room.SessionLogViewModel;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class ReviewSessionFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView recyclerView;
    private WorkHistoryAdapter workHistoryAdapter;
    private List<SessionLogObject> workHistoryList;

    SessionLogViewModel mSessionLogViewModel;

    SchoolSessionViewModel mSchoolSessionViewModel;


    public ReviewSessionFragment() {
        // Required empty public constructor
    }

    public static ReviewSessionFragment newInstance(String param1, String param2) {
        ReviewSessionFragment fragment = new ReviewSessionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_review_sessions, container, false);
    }





    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        recyclerView = view.findViewById(R.id.recycler_view_review_session);

        workHistoryList = new ArrayList<>();


        mSchoolSessionViewModel = new ViewModelProvider(this).get(SchoolSessionViewModel.class);

        mSessionLogViewModel = new ViewModelProvider(this).get(SessionLogViewModel.class);


      /*  workHistoryList.add(new WorkHistory(new SessionLog(SessionLog.PAUSE),
                new PressReport(PressReport.START),
                "-1",
                "22/1/2020","Accra High"));
        workHistoryList.add(new WorkHistory(new SessionLog(SessionLog.PAUSE),
                new PressReport(PressReport.START),
                UUID.randomUUID().toString(),
                "22/1/2020","Accra High"));

        workHistoryList.add(new WorkHistory(new SessionLog(SessionLog.CONTINUE),
                new PressReport(PressReport.NONE),
                UUID.randomUUID().toString(),
                "22/1/2020","Accra High"));

        workHistoryList.add(new WorkHistory(new SessionLog(SessionLog.PAUSE),
                new PressReport(PressReport.LOADING),
                UUID.randomUUID().toString(),
                "22/1/2020","Accra High"));


        workHistoryList.add(new WorkHistory(new SessionLog(SessionLog.END),
                new PressReport(PressReport.PRINTED),
                UUID.randomUUID().toString(),
                "22/1/2020","Accra High"));


        workHistoryList.add(new WorkHistory(new SessionLog(SessionLog.START),
                new PressReport(PressReport.NONE),
                UUID.randomUUID().toString(),
                "22/1/2020","Accra High"));


        workHistoryList.add(new WorkHistory(new SessionLog(SessionLog.END),
                new PressReport(PressReport.SUBMITTED),
                UUID.randomUUID().toString(),
                "22/1/2020","Accra High"));
*/






        if(getActivity()!=null){
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),
                    DividerItemDecoration.VERTICAL));
            workHistoryAdapter = new WorkHistoryAdapter(workHistoryList, getActivity());
            recyclerView.setAdapter(workHistoryAdapter);

        }
        view.findViewById(R.id.drawer_menu).setOnClickListener(v -> EventBus.getDefault().post(new DrawerSelectedEvent(1, true)));

        view.findViewById(R.id.work_history).setOnClickListener(v -> EventBus.getDefault().post(new MessageEvent(true, "Review Sessions",6)));


        view.findViewById(R.id.review_cards).setOnClickListener(v -> EventBus.getDefault().post(new MessageEvent(true, "Review Cards",4)));

        if(getActivity()!=null)
            getSessions();
    }

    private void getSessions(){
        mSchoolSessionViewModel.getAllSchoolSessions().observe(getViewLifecycleOwner(), schoolSessions -> {
            Log.d("mSchoolSessionViewModel", "schoolSessions: "+schoolSessions.toString());
        });

        mSessionLogViewModel.getAllSessions().observe(getViewLifecycleOwner(), sessionLogObjects -> {

            workHistoryList.clear();
            workHistoryList.addAll(sessionLogObjects);
            workHistoryAdapter.notifyDataSetChanged();
            Log.d("mSessionLogViewModel", "sessionLogObjects: "+sessionLogObjects.toString());
        });
    }

}